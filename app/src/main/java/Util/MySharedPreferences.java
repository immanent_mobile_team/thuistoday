package Util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.app.thuistoday.HomeActivity;
import com.app.thuistoday.RegistrationActivity;

import java.util.HashMap;

/**
 * Created by Owner on 3/29/2016.
 */


public class MySharedPreferences  {
    public static SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static String str_userId="",str_id="",str_address="",str_city="",str_email="",
                     str_name="",str_password="",str_phoneNumber="",str_status="",str_postCode="",str_picUrl="";
    public static boolean Ison=false;
    public static String str_english="",str_dutch="";
    public static String str_ideal="";

    public MySharedPreferences(Context context)
    {
        sharedPreferences=context.getSharedPreferences(Constants.MY_PREFS, Context.MODE_PRIVATE);
        getUserData();
    }
    public void getUserData()
    {
        if (!sharedPreferences.getString(Constants.KEY_USER_ID, Constants.NO_VALUE).equals(Constants.NO_VALUE))
        {
            str_userId =sharedPreferences.getString(Constants.KEY_USER_ID,Constants.NO_VALUE);
            str_id =sharedPreferences.getString(Constants.key_id, Constants.NO_VALUE);
            str_address = sharedPreferences.getString(Constants.key_address, Constants.NO_VALUE);
            str_city =sharedPreferences.getString(Constants.key_city, Constants.NO_VALUE);
            str_email =sharedPreferences.getString(Constants.key_email, Constants.NO_VALUE);
            str_name =sharedPreferences.getString(Constants.key_name, Constants.NO_VALUE);
            str_password =sharedPreferences.getString(Constants.key_password, Constants.NO_VALUE);
            str_phoneNumber =sharedPreferences.getString(Constants.key_phoneNumber, Constants.NO_VALUE);
            str_status =sharedPreferences.getString(Constants.key_status, Constants.NO_VALUE);
            str_postCode = sharedPreferences.getString(Constants.key_postCode, Constants.NO_VALUE);
            str_picUrl = sharedPreferences.getString(Constants.key_picture_url, Constants.NO_VALUE);
            Log.e("pref>",str_id);
            Log.e("pref>",str_userId);
            Log.e("pref>",str_name);
            Log.e("pref>",str_email);
            Log.e("pref>",str_phoneNumber);
            Log.e("pref>",str_postCode);
            Log.e("pref>",str_address);
            Log.e("Pref>",str_picUrl);


        }
    }
    public void saveUserDetail(String str_userId, String strId, String str_address, String str_city, String str_email, String str_name, String str_password, String str_phoneNumber, String strStatus, String str_postCode, String str_picUrl
    ) {
        editor = sharedPreferences.edit();
        editor.putString(Constants.KEY_USER_ID,str_userId);
        editor.putString(Constants.key_id,strId);
        editor.putString(Constants.key_address,str_address);
        editor.putString(Constants.key_city,str_city);
        editor.putString(Constants.key_email,str_email);
        editor.putString(Constants.key_name,str_name);
        editor.putString(Constants.key_password,str_password);
        editor.putString(Constants.key_phoneNumber,str_phoneNumber);
        editor.putString(Constants.key_status,strStatus);
        editor.putString(Constants.key_postCode,str_postCode);
        editor.putString(Constants.key_picture_url,str_picUrl);
        editor.commit();
        getUserData();
    }


    public void clearAllData(Context context) {
        sharedPreferences = context.getSharedPreferences(Constants.MY_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().apply();

        str_userId="";str_id="";str_address="";str_city="";str_email="";
        str_name="";str_password="";str_phoneNumber="";str_status="";str_postCode="";
        str_picUrl="";

    }
    public void saveToggleState(String str_english,String str_dutch){
        editor = sharedPreferences.edit();
//        editor.putBoolean(Constants.KEY_ISON,ison);
        editor.putString(Constants.KEY_ENGLISH,str_english);
        editor.putString(Constants.KEY_DUTCH,str_dutch);
        editor.commit();
        getToggleState();
    }
    public void getToggleState(){
        str_english =sharedPreferences.getString(Constants.KEY_ENGLISH, Constants.NO_VALUE);
        str_dutch= sharedPreferences.getString(Constants.KEY_DUTCH, Constants.NO_VALUE);
        Log.e("pref>>",str_english);
        Log.e("pref>>",str_dutch);
//        return sharedPreferences.getBoolean(Constants.KEY_ISON,false);
    }
    public void ClearDataToggleState(Context context){
        sharedPreferences=context.getSharedPreferences(Constants.MY_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear().apply();
        str_dutch="";
        str_english="";
    }
    public void savePaypayIdea(String str_paypal){
        editor=sharedPreferences.edit();
        editor.putString(Constants.KEY_IDEAL,str_paypal);
        editor.commit();
        getPypalIDeal();
    }
    public void getPypalIDeal(){
        str_ideal=sharedPreferences.getString(Constants.KEY_IDEAL,Constants.NO_VALUE);
    }
    public void clearDataPaypalIdeal(Context context){
        sharedPreferences=context.getSharedPreferences(Constants.MY_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear().apply();
        str_ideal="";


    }
}

