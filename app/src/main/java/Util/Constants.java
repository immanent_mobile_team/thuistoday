package Util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.app.thuistoday.HomeActivity;
import com.app.thuistoday.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import volley.AppController;

/**
 * Created by HOME on 1/18/2017.
 */

public class Constants {

    public static final String KEY_ITEM_NAME = "itemName";
    public static final String KEY_ITEM_DESCRIPTION = "itemDescription";
    public static final String KEY_ITEM_PRICE = "itemPrice";
    public static final String KEY_ITEM_QUANTITY = "itemQuantity";
    public static final String KEY_ITEM_WEIGHT_UNIT = "itemWeightUnit";
    public static final String MY_PREFS = "user_pref";
    public static final String KEY_OFFSET = "offset";
    public static final String KEY_ORDER_ID = "order_id";
    public static final String KEY_TOTAL_PAYMENT = "total_payment";
    public static final String KEY_DATE = "date";
    public static final String KEY_DELIVERY_DATE = "delivery_date";
    public static final String KEY_DELIVERY_TIME = "delivery_time";
    public static final String KEY_QUANTITY = "quantity";
    public static final String KEY_DELIVERY_CHARGE = "delivery_charge";
    public static final String KEY_PAYMENT_METHOD = "payment_method";
    public static final String KEY_SERVICE_CHARGE = "service_charge";
    public static final String KEY_CART = "cart";
    public static final String KEY_SHOPID = "shopid";
    public static final String KEY_TRANS_ID = "trans_id";
    public static final String KEY_DELIVERY_OPTION = "delivery_option";
    public static final String KEY_HASH_MAP = "hashmap";
    public static final String PREF_PRODUCT_DETAIL = "product_detail";
    public static final String KEY_ID = "id";
    public static final String KEY_RESTAURANT_NAME_AR = "restaurant_name_ar";
    public static final String KEY_PRICE = "price";
    public static final String SUBTOTAL = "subtotal";
    public static final String KEY_ORIGINAL_PRICE = "originalprice";
    public static final String KEY_PROFILE_PIC = "profile_pic";
    public static final CharSequence PLEASE_WAIT = "Please wait...";
    public static final String KEY_ROW_ID = "rowid";
    public static final String KEY_DELIVERY_CHARGE_OPENING_HOUR = "delivery_charges_opening_hour";
    public static final String KEY_OPEN_DATE = "opendate";
    public static final String VIA_FORGOT_PASS = "forgot_pass";

    public static final String KEY_FACEBOOK_PUBLIC_PROFILE ="public_profile";
    public static final String KEY_FACEBOOK_FIELDS="fields";
    public static final String KEY_FACEBOOK_PUT_ID_FNAME_LNAME="id,first_name,last_name,email,name";
    public static final String KEY_DEVICE_ID ="device_id" ;
    public static final String KEY_DEVICE_TYPE = "device_type";
    public static final String KEY_COUPON = "coupon";
    public static final String KEY_QTY = "qty";
    public static final String KEY_ISON ="ison" ;
    public static final String KEY_ENGLISH ="english" ;
    public static final String KEY_DUTCH ="dutch" ;
    public static final String KEY_NEW_PASSWORD ="new_password" ;
    public static final String KEY_OLD_PASSWORD="old_password";
    public static final String KEY_IDEAL = "Ideal";
    public static final String KEY_TOWN_NAME ="town_name" ;


    public static String key_picture_url = "pic_url";


    Context context;
    Snackbar snackbar;
    public Constants(Context context)
    {
        this.context= context;
    }

    /***
     *  ALL URL
     */
    public static final String IMAGE_BASE_URL="http://deliveryapp.demodemo.ga/uploads/";
    public static final String BASE_URL="http://deliveryapp.demodemo.ga/web/api/user/";
    public static final String REGISTRATION_URL=BASE_URL+"register";
    public static final String LOGIN_URL=BASE_URL+"login";
    public static final String SHOP_TYPE_URL=BASE_URL+"shopType";
    public static final String FETCH_PRODUCT_URL=BASE_URL+"fetchProduct";
    public static final String FETCH_PRODUCT_CATEGORY_URL=BASE_URL+"fetchProductCategory";
    public static final String FIND_SHOP_URL=BASE_URL+"findShop";
    public static final String FIND_STREET_CITY_URL=BASE_URL+"findStreetCity";
    public static final String ORDER_HISTORY_URL = BASE_URL + "orderHistory";
    public static final String ORDER_PLACE_ORDER = BASE_URL + "placeAnOrder";
    public static final String URL_UPLOAD_IMAGE = BASE_URL + "editUser_image";
    public static final String URL_UPDATE_PROFILE = BASE_URL + "edit_profile";
    public static final String GET_TIME_OF_SHOP = BASE_URL + "checkouttimingweekdays";
    public static final String URL_FORGOT_PASSWORD = BASE_URL + "forget_password";
    public static final String URL_IS_LOGIN = BASE_URL + "is_login";
    public static final String CHANGE_PASSWORD=BASE_URL +"new_password";



    /***
     * ALL KEYS
     */

    public static final String KEY_USER_ID ="user_id";
    public static final String NO_VALUE ="no values";
    public static final String KEY_NAME ="name";
    public static final String KEY_EMAIL ="email";
    public static final String KEY_PASSWORD ="password";
    public static final String KEY_PHONE ="phone";
    public static final String KEY_POST_CODE ="zipcode";
    public static final String KEY_ADDRESS ="address";
    public static final String KEY_CITY ="city";
    public static final String KEY_SHOP_ID="shop_id";
    public static final String KEY_CATEGORY_ID="category_id";
    public static final String KEY_CITY_NAME="cityname";
    public static final String KEY_ZIP_CODE="zipcode";
    public static final String KEY_CATEGORY="category";
    public static final String CATEGORY_ID ="category_id";
    public static final String CATEGORY_NAME ="category_name";
    public static final String IMAGE ="image";
    public static final String IMAGE_ID ="image_id";
    public static final String STATUS ="status";
    public static final String PLAATS_NAME ="plaats_name";
    public static final String STRAAT_NAME ="straat_name";
    public static final String STREET_ADDRESS ="street_address";
    public static final String DATE_ADDED_ON ="date_added_on";
    public static final String ADDRESS ="address";
    public static final String ADDRESS_AR ="address_ar";
    public static final String AREA_ID ="adrea_id";
    public static final String AVG_DELIVERY_TIME ="delivery_time";
    public static final String BANNER ="banner";
    public static final String CLOSE_TIME ="close_time";
    public static final String CONTACT_ID ="contact_id";
    public static final String COUNTRY_ID ="country_id";
    public static final String DELIVERY_FEE ="delivery_fee";
    public static final String ENTRANCE_FEE ="entrance_fee";
    public static final String SHOP_IMAGE ="shope_image";
    public static final String IS_ACTIVE ="is_active";
    public static final String IS_OPEN ="is_open";
    public static final String IS_VERIFIED ="is_verified";
    public static final String LATITUDE ="latitude";
    public static final String LOCATION ="location";
    public static final String LOGO ="logo";
    public static final String LONGITUDE ="longitude";
    public static final String OPEN_TIME ="open_time";
    public static final String PAYMENT_MODE ="payment_mode";
    public static final String PERMI_TIME ="permi_time";
    public static final String COMMISSION_PER_ODER ="commission_per_order";
    public static final String POST_CODE ="post_code";
    public static final String REFERENCE ="refernce";
    public static final String RESTAURANT_DESCRIPTION ="restaurant_description";
    public static final String RESTAURANT_DESCRIPTION_AR ="restaurant_description_ar";
    public static final String RESTAURANT_DISPLAY_ID ="restaurant_display_id";
    public static final String RESTAURANT_ID ="restaurant_id";
    public static final String RESTAURANT_NAME ="restaurant_name";
    public static final String RESTAURANT_NAME_AR ="restaurant_name";
    public static final String SAUCE_ON ="sauce_on";
    public static final String IS_ENABLED ="is_enabled";
    public static final String CRUST_ON ="crust_on";
    public static final String CATEGORY_NAME_AR ="category_name_ar";
    public static final String CUSTOM_PIZZA ="custom_pizza";
    public static final String MEAL_DESCRIPTION ="meal_description";
    public static final String MEAL_DESCRIPTION_AR ="meal_description_ar";
    public static final String MEAL_ID ="meal_id";
    public static final String MEAL_IMAGE ="meal_image";
    public static final String MEAL_NAME ="meal_name";
    public static final String MEAL_NAME_AR ="meal_name_ar";
    public static final String MEAL_PRICE ="meal_price";
    public static final String PIZZA_PRICE ="pizza_price";
    public static final String PRODUCT_UNIT ="product_unit";
    public static final String TYPE ="type";
    public static final String VAT ="vat";
    public static final String VAT_NAME ="vat_name";
    public static String key_id="id";
    public static String key_address="address";
    public static String key_city="city";
    public static String key_email="email";
    public static String key_name="name";
    public static String key_password="password";
    public static String key_phoneNumber="phone_number";
    public static String key_status="status";
    public static String key_postCode="post_code";
    public static String SelectedCategoryId="selected_category_id";
    public static String CityName="city_name";
    public static String PostCode="post_code";
    /**
     * @param string
     * @param linearLayout
     */
    public void showSnackBar(String string, LinearLayout linearLayout)
    {
        snackbar = Snackbar
                .make(linearLayout, string, Snackbar.LENGTH_INDEFINITE).
                        setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        });
        snackbar.show();
    }

    /***
     *
     * @param string
     * @param linearLayout
     */
    public void showSnackBar(String string, RelativeLayout linearLayout)
    {
        snackbar = Snackbar
                .make(linearLayout, string, Snackbar.LENGTH_INDEFINITE).
                        setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        });
        snackbar.show();
    }

    /***
     *
     * @param string
     * @param linearLayout
     */
    public void showSnackBar(String string, DrawerLayout linearLayout)
    {
        snackbar = Snackbar
                .make(linearLayout, string, Snackbar.LENGTH_INDEFINITE).
                        setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                snackbar.dismiss();
                            }
                        });
        snackbar.show();
    }
    public  static boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }
    public  static boolean isStringEmpty(String string) {
        if (string.trim().length()>0)
            return false;

        return true;
    }
    /***
     * METHOD TO GET JWT KEY
     * @return
     */
    public static String getBase64(){
        String str_secret_key="try@!tfgh5673";
        byte[]data;
        String base64 = null;
        try {

            data = str_secret_key.getBytes("UTF-8");

            base64 = Base64.encodeToString(data, Base64.DEFAULT);

            Log.i("Base 64 >>>", base64);

        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();

        }
        return base64.toString();
    }

    /***
     *
     * @param str_profilePic
     * @param iv_profile
     */
    public static void setImage(String str_profilePic, final ImageView iv_profile) {
        if (str_profilePic!=null&&str_profilePic.length()>0)
        {
            ImageLoader imageLoader = AppController.getInstance().getImageLoader();
            imageLoader.get(str_profilePic, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("jkd", "Image Load Error: " + error.getMessage());
                    iv_profile.setImageResource(R.drawable.no_image);
                }
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        Log.e("jkd", "" + response);
                        iv_profile.setImageBitmap(response.getBitmap());
                    }
                }
            });
        }
        else
        {
            iv_profile.setImageResource(R.drawable.zagroos_logo);
        }
    }

    public static void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/"
                , null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }

}
