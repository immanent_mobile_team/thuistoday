package Util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {

	
	  private Context _context;
	    boolean isConnected;
	    public ConnectionDetector(Context context){
	        this._context = context;
	    }
	 
	    /**
	     * Checking for all possible internet providers
	     * **/
	    public boolean isConnectingToInternet(){
	        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
	          if (connectivity != null)
	          {
	        	  
//	        	  NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
//	        	  isConnected = activeNetwork.isConnected();
	              NetworkInfo[] info = connectivity.getAllNetworkInfo();
	              if (info != null)
	                  for (int i = 0; i < info.length; i++)
	                      if (info[i].getState() == NetworkInfo.State.CONNECTED)
	                      {
	                          return true;
	                      }
	 
	          }
//	          Log.d("INSIDE ISCONNECTING TO INTERNET", "Boolean: "+isConnected);
	          return false;
	    }
}
