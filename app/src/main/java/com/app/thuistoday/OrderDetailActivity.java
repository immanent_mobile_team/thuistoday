package com.app.thuistoday;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import Util.Constants;
import Util.MySharedPreferences;
import adapter.CheckOutListAdapter;
import adapter.NavigationAdapter;
import adapter.OrderDetailAdapter;

/**
 * Created by HOME on 2/10/2017.
 */
public class OrderDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    TextView tv_toolBar,tv_orderNumberValue,tv_orderPlacedDate,tv_orderStatusValue,tv_deliveryAddressValue,tv_paymentMethodValue;
    LinearLayout ll_home, ll_myAccountLogin, ll_needHelp;
    ImageView iv_close,ivProfile;
    Button btn_signIN,btn_register;
    RecyclerView rv_navigation,rv_orderDetailList;
    Intent intent;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_order_detail);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            findIds();
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            rv_navigation.setLayoutManager(mLayoutManager);
            rv_navigation.setItemAnimator(new DefaultItemAnimator());
            rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),OrderDetailActivity.this));
            RecyclerView.LayoutManager mLayoutManagershopList = new LinearLayoutManager(getApplicationContext());
            rv_orderDetailList.setLayoutManager(mLayoutManagershopList);
            rv_orderDetailList.setItemAnimator(new DefaultItemAnimator());
            rv_orderDetailList.setAdapter(new OrderDetailAdapter(new ArrayList<HashMap<String, String>>()));
            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.setDrawerIndicatorEnabled(false);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.top_left_icon, this.getTheme());
            toggle.setHomeAsUpIndicator(drawable);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drawer.isDrawerVisible(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });
            toggle.syncState();
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View header = navigationView.getHeaderView(0);
        btn_signIN = (Button) header.findViewById(R.id.btn_signIN);
            btn_register = (Button) header.findViewById(R.id.btn_register);
        btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn=true;
                    intent=new Intent(OrderDetailActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister=true;
                    intent=new Intent(OrderDetailActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
        Constants.setImage(MySharedPreferences.str_picUrl,ivProfile);
//        Toast.makeText(this, "orderDetal", Toast.LENGTH_SHORT).show();
        }

        private void findIds() {
            ivProfile = (ImageView) findViewById(R.id.ivProfile);
            tv_toolBar = (TextView) findViewById(R.id.tv_toolBar);
            tv_orderNumberValue = (TextView) findViewById(R.id.tv_orderNumberValue);
            tv_orderPlacedDate = (TextView) findViewById(R.id.tv_orderPlacedDate);
            tv_orderStatusValue = (TextView) findViewById(R.id.tv_orderStatusValue);
            tv_deliveryAddressValue = (TextView) findViewById(R.id.tv_deliveryAddressValue);
            tv_paymentMethodValue = (TextView) findViewById(R.id.tv_paymentMethodValue);
            rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
            tv_toolBar.setText(getResources().getString(R.string.ZagroosStoreActivityTitle));
            ll_myAccountLogin = (LinearLayout) findViewById(R.id.ll_myAccountLogin);
            ll_home=(LinearLayout)findViewById(R.id.ll_home);
            ll_needHelp = (LinearLayout) findViewById(R.id.ll_needHelp);
            rv_orderDetailList=(RecyclerView)findViewById(R.id.rv_orderDetailList);
            ll_myAccountLogin.setOnClickListener(this);
            ll_needHelp.setOnClickListener(this);
            ll_home.setOnClickListener(this);
        }

        @Override
        public void onBackPressed() {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            int id = item.getItemId();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ll_myAccountLogin:
                    HomeActivity.isRegister=false;
                    intent = new Intent(this, RegistrationActivity.class);
                    startActivity(intent);
                    break;
                case R.id.ll_needHelp:
                    break;
                case R.id.ll_home:
                    intent = new Intent(this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
            }
        }
    }


