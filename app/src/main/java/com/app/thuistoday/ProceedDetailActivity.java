package com.app.thuistoday;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;

import static com.app.thuistoday.R.string.date;
import static com.app.thuistoday.R.string.payment_method;
import static com.app.thuistoday.R.string.quantity;

/**
 * Created by HOME on 2/13/2017.
 */
public class ProceedDetailActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    TextView tv_toolBar,tvCartItems,tvCheckOutPrice,tv_duringOpeningHrs,tv_during_weakdayWht,tv_pickUpWht;
    LinearLayout ll_home, ll_myAccountLogin, ll_needHelp;
    RelativeLayout rl_chooseShopType, rl_choose,rlDuringHours,rlDuringWeekly,rlPickUp,rlPaypal,rlIdeal,rlCash
            ,rlDatePick,rl_proceed,rl_revise_cart;
    Dialog dialog;
    ImageView ivProfile, ivCashOnDelivery,ivIdeal,ivPaypal;
    RecyclerView rv_chooseShopType, rv_navigation, rv_checkOutList;
    Intent intent;
    Button btn_signIN,btn_register;
    Layout layout;
    String strUserId="",strTotalPayment="",strDate="",strDeliveryDate="",strTime="",
            strQuantity="",strDeliveryCharge="",strPaymentMethod="cash on delivery",strServiceCharge="",strCart="",strShopId=""
            ,strTransId="",strDeliveryOption="",strAddress="",strName="",strPostCode="",strPhoneNumber=""
            ,strEmail="",strCity="",strOpenDate="",strDuringHoursCharges="",strDuringWeekDaysCharges="";
    private String strTimes[];
    private ProgressDialog pd;
    private EditText edtAddress,edtName,edtPostCode,edtPhoneNumber,edtCity,edtEmail,edtTime,edtDate,edtDatePick,edtNamePick,edtEmailPick,edtPhonePick;
    DrawerLayout drawer_layout;
    private boolean isDate,isTime,isHourlyWeeklySelected=true,isPickSelected;
    private RelativeLayout rlDate,rlTime;
    int hour=0,min=0,month=0,day=0,year=0,mCartItems=0;
    DatePicker datePicker;
    TimePicker timePicker;
    private float mDuringHoursWeeklyCost=0;
    private ListView listView;
    SharedPreferences sp,spProductDetail;
    double totalPrice=0;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed_detail);

        spProductDetail = getApplication().getSharedPreferences(Constants.PREF_PRODUCT_DETAIL,MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),ProceedDetailActivity.this));

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.top_left_icon, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN = (Button) header.findViewById(R.id.btn_signIN);
        btn_register = (Button) header.findViewById(R.id.btn_register);
//        btn_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn = true;
//                intent = new Intent(ProceedDetailActivity.this, RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister = true;
//                intent = new Intent(ProceedDetailActivity.this, RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {

                        intent=new Intent(ProceedDetailActivity.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });
        Log.e("RESPONSE_USER_ID",strUserId);
//        Toast.makeText(this, ""+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
        if (!MySharedPreferences.str_userId.equals("")){
            btn_register.setVisibility(View.GONE);
            btn_signIN.setVisibility(View.GONE);
            ViewGroup.MarginLayoutParams marginLayoutParams =
                    (ViewGroup.MarginLayoutParams) rv_navigation.getLayoutParams();
            marginLayoutParams.setMargins(0,(int)getResources().getDimension(R.dimen._120sp), 0, 0);
            rv_navigation.setLayoutParams(marginLayoutParams);
        }
        else
        {
            btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn = true;
                    intent = new Intent(ProceedDetailActivity.this, RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister = true;
                    intent = new Intent(ProceedDetailActivity.this, RegistrationActivity.class);
                    startActivity(intent);
                }
            });
        }
        strUserId = MySharedPreferences.str_userId;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        strDate = df.format(c.getTime());

        if (spProductDetail!=null)
        {
            strShopId = spProductDetail.getString(Constants.KEY_SHOP_ID,Constants.NO_VALUE);
        }
        if (spProductDetail!=null)
        {
            strDuringWeekDaysCharges =spProductDetail.getString(Constants.KEY_DELIVERY_CHARGE,Constants.NO_VALUE);
            strDuringHoursCharges = spProductDetail.getString(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR,Constants.NO_VALUE);
            Log.e("actualValue",strDuringHoursCharges);
            String str_duringHor = (strDuringHoursCharges.contains(","))
                    ? strDuringHoursCharges.replace(",",".")
                    : strDuringHoursCharges;
            mDuringHoursWeeklyCost = Float.parseFloat(str_duringHor);
//            mDuringHoursWeeklyCost=5f;
        }

    }

    private void findIds() {
        tv_toolBar = (TextView) findViewById(R.id.tv_toolBar);
        tvCheckOutPrice = (TextView) findViewById(R.id.tvCheckOutPrice);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        tv_toolBar.setText(getResources().getString(R.string.checkOutActivityTitle));
        ll_myAccountLogin = (LinearLayout) findViewById(R.id.ll_myAccountLogin);
        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        rl_proceed = (RelativeLayout) findViewById(R.id.rl_proceed);
        rl_revise_cart= (RelativeLayout) findViewById(R.id.rl_revise_cart);
        ll_needHelp = (LinearLayout) findViewById(R.id.ll_needHelp);

        edtDate = (EditText) findViewById(R.id.edt_date);
        edtDatePick = (EditText) findViewById(R.id.edt_datePick);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtTime = (EditText) findViewById(R.id.edt_time);
        edtPostCode = (EditText) findViewById(R.id.edt_postCode);
        edtPhoneNumber = (EditText) findViewById(R.id.edt_phoneNumber);
        edtCity = (EditText) findViewById(R.id.edt_city);
        edtEmail= (EditText) findViewById(R.id.edt_email);
        edtName = (EditText) findViewById(R.id.edt_name);

//        datePicker = (DatePicker) findViewById(R.id.datePicker);
//        timePicker = (TimePicker) findViewById(R.id.timePicker);
        tvCartItems = (TextView) findViewById(R.id.tvCartItems);
        tv_duringOpeningHrs=(TextView)findViewById(R.id.tv_duringOpeningHrs);
        tv_during_weakdayWht=(TextView)findViewById(R.id.tv_during_weakdayWht);
        tv_pickUpWht=(TextView)findViewById(R.id.tv_pickUpWht);

        ivCashOnDelivery = (ImageView) findViewById(R.id.ivCashOnDeliveryRadio);
        ivIdeal = (ImageView) findViewById(R.id.ivDealRadio);
        ivPaypal = (ImageView) findViewById(R.id.ivPaypalRadio);

        rlDate= (RelativeLayout) findViewById(R.id.rlDateA);
        rlTime= (RelativeLayout) findViewById(R.id.rlTime);

        rlDatePick = (RelativeLayout) findViewById(R.id.rlDatePick);
        edtNamePick = (EditText) findViewById(R.id.edt_namePick);
        edtPhonePick = (EditText) findViewById(R.id.edt_phoneNumberPick);
        edtEmailPick = (EditText) findViewById(R.id.edt_emailPick);


        rlCash = (RelativeLayout) findViewById(R.id.rlCash);
        rlIdeal = (RelativeLayout) findViewById(R.id.rlIdeal);
        rlPaypal = (RelativeLayout) findViewById(R.id.rlPaypal);
        rlDuringHours = (RelativeLayout) findViewById(R.id.rlDuringHours);
        rlDuringWeekly = (RelativeLayout) findViewById(R.id.rlDuringWeekly);
        rlPickUp = (RelativeLayout) findViewById(R.id.rlPickUp);

        rv_checkOutList = (RecyclerView) findViewById(R.id.rv_checkOutList);
        ll_myAccountLogin.setOnClickListener(this);
        ll_needHelp.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        rl_proceed.setOnClickListener(this);
        rl_revise_cart.setOnClickListener(this);

        rlDuringHours.setOnClickListener(this);
        rlDuringWeekly.setOnClickListener(this);
        rlPickUp.setOnClickListener(this);
        rlCash.setOnClickListener(this);
        rlIdeal.setOnClickListener(this);
        rlPaypal.setOnClickListener(this);

        ivIdeal.setOnClickListener(this);
        ivPaypal.setOnClickListener(this);
        ivCashOnDelivery.setOnClickListener(this);
        rlDate.setOnClickListener(this);
        rlTime.setOnClickListener(this);
        rlDatePick.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    static float roundOff(float x, int position)
    {
        float a = x;
        double temp = Math.pow(10.0, position);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_myAccountLogin:
                HomeActivity.isRegister = false;
                intent = new Intent(this, RegistrationActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_needHelp:
                break;
            case R.id.ll_home:
                intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.rlDuringHours:
                rlPickUp.setBackground(getResources().getDrawable(R.drawable.pick_up_wht));//new 01
                rlDuringHours.setBackground(getResources().getDrawable(R.drawable.during_weakday_org));//new
                rlDuringWeekly.setBackground(getResources().getDrawable(R.drawable.during_btn_wht));
                findViewById(R.id.rlParentPickUp).setVisibility(View.GONE);
                findViewById(R.id.rlParentWeeklyHourly).setVisibility(View.VISIBLE);
                tv_duringOpeningHrs.setTextColor(getResources().getColor(R.color.colorWhite));//1
                tv_during_weakdayWht.setTextColor(getResources().getColor(R.color.colorBlack));//2
                tv_pickUpWht.setTextColor(getResources().getColor(R.color.colorBlack));//3
                isHourlyWeeklySelected= true;
                isPickSelected = false;
                hour=0;min=0;month=0;day=0;year=0;
                strDeliveryCharge = spProductDetail.getString(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR ,Constants.NO_VALUE);
                mDuringHoursWeeklyCost = Float.parseFloat(strDeliveryCharge);
//                mDuringHoursWeeklyCost=5f;
                if (!strDeliveryCharge.equals(Constants.NO_VALUE))
                {
                    tvCheckOutPrice.setText("checkout(total: €"+String.valueOf(roundOff(Float.parseFloat(totalPrice+"")+Float.parseFloat(strDeliveryCharge.replace(",",".")),2))+")");
                }
                break;
            case R.id.rlDuringWeekly:
                rlPickUp.setBackground(getResources().getDrawable(R.drawable.pick_up_wht));//new  01
                rlDuringHours.setBackground(getResources().getDrawable(R.drawable.during_weakday_wht));
                rlDuringWeekly.setBackground(getResources().getDrawable(R.drawable.during_btn_org));//new
                findViewById(R.id.rlParentPickUp).setVisibility(View.GONE );
                findViewById(R.id.rlParentWeeklyHourly).setVisibility(View.VISIBLE);
                tv_duringOpeningHrs.setTextColor(getResources().getColor(R.color.colorBlack));//1
                tv_during_weakdayWht.setTextColor(getResources().getColor(R.color.colorWhite));//2
                tv_pickUpWht.setTextColor(getResources().getColor(R.color.colorBlack));
                isHourlyWeeklySelected= true;
                isPickSelected = false;
                hour=0;min=0;month=0;day=0;year=0;
                strDeliveryCharge = spProductDetail.getString(Constants.KEY_DELIVERY_CHARGE ,Constants.NO_VALUE);
                mDuringHoursWeeklyCost = Float.parseFloat(strDeliveryCharge);
//                mDuringHoursWeeklyCost=5f;
                if (!strDeliveryCharge.equals(Constants.NO_VALUE))
                {
                    tvCheckOutPrice.setText("checkout(total: €"+String.valueOf(roundOff(Float.parseFloat(totalPrice+"")+Float.parseFloat(strDeliveryCharge.replace(",",".")),2))+")");
                }
                break;
            case R.id.rlPickUp:
                rlPickUp.setBackground(getResources().getDrawable(R.drawable.pick_up_org));//new
                rlDuringHours.setBackground(getResources().getDrawable(R.drawable.during_weakday_wht));
                rlDuringWeekly.setBackground(getResources().getDrawable(R.drawable.during_btn_wht));//new 01
                findViewById(R.id.rlParentPickUp).setVisibility(View.VISIBLE);
                findViewById(R.id.rlParentWeeklyHourly).setVisibility(View.GONE);
                tv_duringOpeningHrs.setTextColor(getResources().getColor(R.color.colorBlack));
                tv_during_weakdayWht.setTextColor(getResources().getColor(R.color.colorBlack));
                tv_pickUpWht.setTextColor(getResources().getColor(R.color.colorWhite));
                isHourlyWeeklySelected= false;
                isPickSelected = true;
                hour=0;min=0;month=0;day=0;year=0;
                strDeliveryCharge = "";
                break;
            case R.id.rlCash:
                ivCashOnDelivery.setImageResource(R.drawable.radio_btn_act);
                ivIdeal.setImageResource(R.drawable.radio_btn);
                ivPaypal.setImageResource(R.drawable.radio_btn);
                strPaymentMethod = "cash on delivery";
                break;
            case R.id.rl_proceed:
                if (isHourlyWeeklySelected)
                {
                    strAddress = edtAddress.getText().toString();
                    strName = edtName.getText().toString();
                    strCity = edtCity.getText().toString();
                    strPostCode = edtPostCode.getText().toString();
                    strEmail = edtEmail.getText().toString();
                    strPhoneNumber = edtPhoneNumber.getText().toString();

                    if (strCity.length()>0&&strPostCode.length()>0&&strName.length()>0&&strEmail.length()>0
                            &&strAddress.length()>0&&strPhoneNumber.length()>0&&strDeliveryDate.length()>0&&strTime.length()>0)
                    {
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm");
                        Date date = null;
                        try {
                            date = sdf.parse(strDeliveryDate+" "+strTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Toast.makeText(ProceedDetailActivity.this, "Please try again." , Toast.LENGTH_SHORT).show();
                        }
                        if (System.currentTimeMillis() < date.getTime()) {
                            proceed();
//                            ded();
                        }
                        else
                            Toast.makeText(this, "Order place is too early.", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        if (strCity.length()==0)
                            AppController.constants.showSnackBar("Please enter your city.",drawer_layout);
                        else if (strAddress.length()==0)
                            AppController.constants.showSnackBar("Please enter your address.",drawer_layout);
                        else if (strName.length()==0)
                            AppController.constants.showSnackBar("Please enter your name.",drawer_layout);
                        else if (strPostCode.length()==0)
                            AppController.constants.showSnackBar("Please enter your postcode.",drawer_layout);
                        else if (strPhoneNumber.length()==0)
                            AppController.constants.showSnackBar("Please enter your phone number.",drawer_layout);
                        else if (strEmail.length()==0)
                            AppController.constants.showSnackBar("Please enter your email.",drawer_layout);
                        else if (strTime.length()==0)
                            AppController.constants.showSnackBar("Please select time.",drawer_layout);
                        else if (strDeliveryDate.length()==0)
                            AppController.constants.showSnackBar("Please select delivery date.",drawer_layout);
                    }
                }
                else if (isPickSelected)
                {
                    strName = edtNamePick.getText().toString();
                    strPhoneNumber = edtPhonePick.getText().toString();
                    strEmail = edtEmailPick.getText().toString();

                    if (strName.length()>0&&strEmail.length()>0
                            &&strPhoneNumber.length()>0&&strDeliveryDate.length()>0)
                    {

//                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
//                        Date date = null;
//                        try {
//                            date = sdf.parse(strTime);
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                            Toast.makeText(ProceedDetailActivity.this, "Please try again." , Toast.LENGTH_SHORT).show();
//                        }
//                        if (System.currentTimeMillis() < date.getTime()) {
                        proceed();
//                        }
//                        ded();

                    }
                    else
                    {
                        if (strName.length()==0)
                            AppController.constants.showSnackBar("Please enter your name.",drawer_layout);
                        else if (strDeliveryDate.length()==0)
                            AppController.constants.showSnackBar("Please  select delivery date.",drawer_layout);
                        else if (strPhoneNumber.length()==0)
                            AppController.constants.showSnackBar("Please enter your phone number.",drawer_layout);
                        else if (strEmail.length()==0)
                            AppController.constants.showSnackBar("Please enter your email.",drawer_layout);

                    }
                }



                break;
            case R.id.rlPaypal:
                ivCashOnDelivery.setImageResource(R.drawable.radio_btn);
                ivIdeal.setImageResource(R.drawable.radio_btn);
                ivPaypal.setImageResource(R.drawable.radio_btn_act);
                strPaymentMethod = "Paypal";
                strServiceCharge ="0.30";
                strTotalPayment = String.valueOf(roundOff(Float.parseFloat(String.valueOf(totalPrice + Double.parseDouble(strServiceCharge))),2));
                break;
            case R.id.rlIdeal:
                ivCashOnDelivery.setImageResource(R.drawable.radio_btn);
                ivIdeal.setImageResource(R.drawable.radio_btn_act);
                ivPaypal.setImageResource(R.drawable.radio_btn);
                strPaymentMethod =  "Ideal";
                strServiceCharge =  "0.25";
                strTotalPayment = String.valueOf(roundOff(Float.parseFloat(String.valueOf(totalPrice + Double.parseDouble(strServiceCharge))),2));
                break;
            case R.id.rl_revise_cart:
                finish();
                break;
            case R.id.rlTime:
                isDate = false;
                isTime = true;
                showTimes();
                break;
            case R.id.rlDateA:
//                Toast.makeText(this, "sdf", Toast.LENGTH_SHORT).show();
                isDate = true;
                isTime= false;
                showDialog();
                break;
            case R.id.rlDatePick:
                isDate = true;
                showDialog();
                break;
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        totalPrice = 0;
        Intent intent = getIntent();
        String action = intent.getAction();
        if (!MySharedPreferences.str_ideal.equals(""))
        {
            intent = new Intent(ProceedDetailActivity.this,MyAccount.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            AppController.mySharedPreferences.clearDataPaypalIdeal(ProceedDetailActivity.this);
            finish();

        }
//        Toast.makeText(this, ""+MySharedPreferences.str_ideal, Toast.LENGTH_SHORT).show();
        if (intent!=null&&Intent.ACTION_VIEW.equals(action)){
//            Toast.makeText(this, "Successfully", Toast.LENGTH_SHORT).show();

        }

        int  mCartItems=0;
        RealmController rel = new RealmController(ProceedDetailActivity.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");

        alCartItems = new ArrayList<>();


        ArrayList<HashMap<String,String>> myArray = new ArrayList<>();
        if (myRealms.size()!=0)
        {
            for (MyRealm myRealm: myRealms)
            {

                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(Constants.KEY_NAME,myRealm.getStrItemName());
                hashMap.put(Constants.KEY_ITEM_DESCRIPTION,myRealm.getStrDescription());
                hashMap.put(Constants.KEY_ITEM_PRICE,myRealm.getStrPrice());
                hashMap.put(Constants.KEY_QTY,myRealm.getStrNumberOfItems());
                hashMap.put(Constants.KEY_ITEM_WEIGHT_UNIT,myRealm.getStrWeightUnit());
                hashMap.put(Constants.VAT,myRealm.getStrVat());
                hashMap.put(Constants.KEY_ID,myRealm.getStrId());
                hashMap.put(Constants.TYPE,myRealm.getStrType());
                hashMap.put(Constants.KEY_ROW_ID,myRealm.getStrId());
                hashMap.put(Constants.KEY_COUPON,"");

                if (myRealm.getStrType().equals("Incl."))
                {
                    hashMap.put(Constants.KEY_PRICE,myRealm.getStrPrice());
                    hashMap.put(Constants.KEY_ORIGINAL_PRICE,myRealm.getStrPrice());
//                     = Double.parseDouble(myRealm.getStrPrice()) * Double.parseDouble(myRealm.getStrNumberOfItems());

                    double db1= Double.parseDouble(myRealm.getStrPrice().replace(",","."));
                    double db = db1 * Double.parseDouble(myRealm.getStrNumberOfItems());

                    hashMap.put(Constants.SUBTOTAL,String.valueOf(db).replace(".",","));
                }
                else
                {
                    String str = myRealm.getStrPrice().replace(",",".");
                    double dWithoutComma = Double.parseDouble(str);

                    double db1 = dWithoutComma + Double.parseDouble(myRealm.getStrVat());
                    double db = db1* Double.parseDouble(myRealm.getStrNumberOfItems());
                    hashMap.put(Constants.SUBTOTAL,String.valueOf(db).replace(".",","));
                    hashMap.put(Constants.KEY_PRICE,String.valueOf(db1).replace(".",","));
                    hashMap.put(Constants.KEY_ORIGINAL_PRICE,myRealm.getStrPrice());

                }
                myArray.add(hashMap);
//                JSONObject json= new JSONObject(hashMap);
//                HashMap<String,String> hashMap1 = new HashMap<>();



//                hashMap1.put(myRealm.getStrId(),json.toString());
//                strCart = json.toString();

//                Log.e("json>>",json.toString());

//                 alCartItems.add(json);
                String str = myRealm.getStrPrice().replace(",",".");
                double dbl= Double.parseDouble(str);
                totalPrice = totalPrice + dbl;
            }
//            HashMap<String,String > hashMapCart = new HashMap<>();
//            ArrayList<JSONObject> arrayList= new ArrayList<>();
//             for (int i=0; i<alCartItems.size();i++)
//            {
//                JSONObject object = alCartItems.get(i);
//                try {
//                    hashMapCart.put(object.getString(Constants.KEY_ID),object.toString());
//                    JSONObject jsonObject = new JSONObject(hashMapCart);
//                    arrayList.add(jsonObject);
//                } catch (JSONException e) {
//                    e.printStackTrace();
////                    Toast.makeText(this, "crsh", Toast.LENGTH_SHORT).show();
//                }
//            }
//            JSONObject js= null;
//            try {
//                js = new JSONObject(hashMapCart);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Toast.makeText(this, arrayList.size()+"", Toast.LENGTH_SHORT).show();
//            for (int i= 0; i<arrayList.size();i++)
//            {
//                JSONObject jsonObject = arrayList.get(i);
//                if (i==0)
//                 strCart = jsonObject.toString();
//                else
//                    strCart = strCart+","+jsonObject.toString();
//            }
////            strCart = js.toString();
//            try {
////                Log.e("cart>>< ob",new JSONObject(strCart).toString());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            Toast.makeText(this, strCart, Toast.LENGTH_SHORT).show();
            tvCartItems.setText(String.valueOf(myRealms.size()));
            strDeliveryCharge = spProductDetail.getString(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR ,Constants.NO_VALUE);
            Log.e("str_DELIVERY_CHARGE",strDeliveryCharge);
            String str_changeDeliveryCharge=(strDeliveryCharge.contains(","))
                    ? strDeliveryCharge.replace(",",".")
                    : strDeliveryCharge;
            mDuringHoursWeeklyCost = Float.parseFloat(str_changeDeliveryCharge);
            if (!strDeliveryCharge.equals(Constants.NO_VALUE))
            {
                tvCheckOutPrice.setText("checkout(total: €"+String.valueOf(roundOff(Float.parseFloat(totalPrice+"")+Float.parseFloat(strDeliveryCharge.replace(",",".")),2))+")");
            }

        }
        edtEmailPick.setText(MySharedPreferences.str_email);
        edtEmail.setText(MySharedPreferences.str_email);

        edtPhonePick.setText(MySharedPreferences.str_phoneNumber);
        edtPhoneNumber.setText(MySharedPreferences.str_phoneNumber);

        edtName.setText(MySharedPreferences.str_name);
        edtNamePick.setText(MySharedPreferences.str_name);

        edtAddress.setText(MySharedPreferences.str_address);
        edtPostCode.setText(MySharedPreferences.str_postCode);
        edtCity.setText(MySharedPreferences.str_city);

        strTotalPayment = totalPrice + "";
        strQuantity = myRealms.size() + "";


        HashMap<String,JSONObject> hashMap= new HashMap<>();
        for (int i=0; i<myArray.size();i++)
        {
            HashMap<String,String> object = myArray.get(i);
            try {
                hashMap.put(object.get(Constants.KEY_ID), new JSONObject(object));
//                JSONObject jsonObject = new JSONObject(hashMapCart);
//                arrayList.add(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
//                    Toast.makeText(this, "crsh", Toast.LENGTH_SHORT).show();
            }
        }
        Log.e("final cart", String.valueOf(new JSONObject(hashMap)));
        strCart = String.valueOf(new JSONObject(hashMap));
//        Toast.makeText(this, strTotalPayment, Toast.LENGTH_SHORT).show();

    }
    ArrayList<JSONObject> alCartItems;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void showDialog()
    {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProceedDetailActivity.this);
        LayoutInflater inflater = (LayoutInflater) ProceedDetailActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        dialog = alertDialogBuilder.create();
        dialog.show();
        datePicker = (DatePicker) view.findViewById(R.id.datePicker);
        timePicker = (TimePicker) view.findViewById(R.id.timePicker);
        Button btnOkay = (Button) view.findViewById(R.id.btnOkay);
        datePicker.setVisibility(View.GONE);
        timePicker.setVisibility(View.GONE);

        if (isDate)
            datePicker.setVisibility(View.VISIBLE);
        if (isTime)
            timePicker.setVisibility(View.VISIBLE);

        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isDate)
                {

                    day = datePicker.getDayOfMonth();
                    month = datePicker.getMonth();
                    year = datePicker.getYear();
                    month = month +1;
                    if (isHourlyWeeklySelected)
                    {
                        edtDate.setText(month+"/"+day+"/"+year);
                        strDeliveryDate = edtDate.getText().toString();
                    }
                    if (isPickSelected)
                    {
                        edtDatePick.setText(month+"/"+day+"/"+year);
                        strDeliveryDate = edtDatePick.getText().toString();
                    }
                    strOpenDate = strDeliveryDate;
                    if (isHourlyWeeklySelected)
                        getTimesOfShop();
                }
                if (isTime)
                {
                    hour = timePicker.getCurrentHour();
                    min = timePicker.getCurrentMinute();
                    if (isHourlyWeeklySelected)
                    {
                        edtTime.setText(hour+":"+min);
                        strTime = edtTime.getText().toString();
                    }

                }
                dialog.dismiss();
                isDate =false;
                isTime = false;
            }
        });

    }

    public void showTimes()
    {
        if (strDeliveryDate.equals("")||strTimes==null)
        {
            AppController.constants.showSnackBar(getResources().getString(R.string.please_select_date_first),drawer_layout);
            return;
        }

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ProceedDetailActivity.this);
        LayoutInflater inflater = (LayoutInflater) ProceedDetailActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dailog_times, null);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        dialog = alertDialogBuilder.create();
        dialog.show();
        listView = (ListView) view.findViewById(R.id.recyclerView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, strTimes);
        listView.setAdapter(adapter);
        Button btnOkay = (Button) view.findViewById(R.id.btnOkay);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                strTime = strTimes[position];
                edtTime.setText(strTime);
                dialog.dismiss();
            }});
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    public void ded()
    {
        Toast.makeText(this, "asd", Toast.LENGTH_SHORT).show();
        HashMap<String ,String> params=  new HashMap<>();
        params.put(Constants.KEY_USER_ID,strUserId);
        params.put(Constants.KEY_TOTAL_PAYMENT, strTotalPayment);
        params.put(Constants.KEY_DATE, strDate);
        params.put(Constants.KEY_DELIVERY_DATE, strDeliveryDate);
        params.put(Constants.KEY_DELIVERY_TIME, strTime);
        params.put(Constants.KEY_QUANTITY, strQuantity);
        params.put(Constants.KEY_DELIVERY_CHARGE, strDeliveryCharge);
        params.put(Constants.KEY_PAYMENT_METHOD, strPaymentMethod);
        params.put(Constants.KEY_SERVICE_CHARGE, strServiceCharge);
        params.put(Constants.KEY_CART, strCart);
        params.put(Constants.KEY_SHOPID, strShopId);
        params.put(Constants.KEY_TRANS_ID, strTransId);
        params.put(Constants.KEY_DELIVERY_OPTION, strDeliveryOption);

        Log.d("hash>",params.toString());

    }
    public void proceed()
    {
        strTotalPayment = String.valueOf(roundOff(Float.parseFloat(strTotalPayment)+mDuringHoursWeeklyCost,2));
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ProceedDetailActivity.this);
        pd.setMessage(getResources().getString(R.string.please_wait));
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.ORDER_PLACE_ORDER,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        String str_response = "";
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response = jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                RealmController rela = new RealmController(getApplication());
                                rela.clearAll();
                                RealmResults<MyRealm> myRealmsa = rela.getAllData();
                                if (myRealmsa.size()!=0)
                                {
                                    mCartItems = 0;
                                    for (MyRealm a : myRealmsa)
                                    {
                                        mCartItems = mCartItems+1;
                                    }
                                    tvCartItems.setText(String.valueOf(mCartItems));
                                }
                                else
                                    tvCartItems.setText("0");
//                                intent = new Intent(ProceedDetailActivity.this,HomeActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent);
//                                finish();

                                JSONObject jsonObject = object.getJSONObject("data");
                                String paymentMethod= jsonObject.getString("payment_method");
                                String str_ideal="";
                                if (paymentMethod.equals("Ideal"))
                                {
                                    AppController.mySharedPreferences.savePaypayIdea("Ideal");
                                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(jsonObject.getString("ideal_url")));
                                    startActivity(i);
                                    Toast.makeText(ProceedDetailActivity.this, ""+object.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                                if (paymentMethod.equals("Paypal"))
                                {
                                    AppController.mySharedPreferences.savePaypayIdea("Paypal");

                                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(jsonObject.getString("paypal_url")));
                                    startActivity(i);

                                }
                                if (paymentMethod.equals("cash on delivery"))
                                {
                                    Toast.makeText(ProceedDetailActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();

                                    intent = new Intent(ProceedDetailActivity.this,MyAccount.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                                    pd.dismiss();


//                                alOrders.clear();
//                                try {
//                                    JSONArray jsonArray=object.getJSONArray("data");
//                                    for (int i=0;i<jsonArray.length();i++) {
//                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                        str_categoryId= jsonObject.getString("category_id");
//                                        str_categoryName=jsonObject.getString("category_name");
//                                        str_image=jsonObject.getString("image");
//                                        str_imageId=jsonObject.getString("image_id");
//                                        strStatus = jsonObject.getString("status");
//                                        hashMap = new HashMap<String, String>();
//                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
//                                        hashMap.put(Constants.CATEGORY_NAME,str_categoryName);
//                                        hashMap.put(Constants.IMAGE,str_image);
//                                        hashMap.put(Constants.IMAGE_ID,str_imageId);
//                                        hashMap.put(Constants.STATUS,strStatus);
//                                        shopTypeList.add(hashMap);
//                                    }
//                                    adapter=new ShopTypeAdapter(context,shopTypeList);
//                                }
//                                catch (Exception e){
//                                    e.printStackTrace();
//                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
//
//                                }
//                                Snackbar snackbar = Snackbar
//                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_LONG)
//                                        .setAction("Ok", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Toast.makeText(HomeActivity.this, "Okay", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//
//                                snackbar.show();

//                                        Log.e("UserName>>",str_userName);
//                                        AppController.mySharedPreferences.saveUserDetail(str_userName);
//                                        editor= MySharedPreferences.sharedPreferences.edit();
//                                        editor.putString(Constants.key_userId,str_userId);
//                                        editor.putString(Constants.key_firstName,str_firstName);
//                                        editor.putString(Constants.key_lastName,str_last_name);
//                                        editor.putString(Constants.key_dob,str_dob);
//                                        editor.putString(Constants.key_email,strEmail);
//                                        editor.putString(Constants.key_userType,str_userType);
//                                        editor.putString(Constants.key_payPalId,str_payPalId);
//                                        editor.putString(Constants.key_profilePic,str_profilePic);
//                                        editor.putString(Constants.key_isVerified,str_isVerified);
//                                        editor.putString(Constants.key_FbProfile,str_picUrl);
//                                        editor.putString(Constants.key_deviceId,str_device_id);
//                                        editor.putString(Constants.key_userName,str_userName);
//                                        editor.commit();
//                                        intent=new Intent(RegistrationActivity.this,Search.class);
//                                        startActivity(intent);
//                                        finish();
//                                Toast.makeText(ProceedDetailActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
//                                finish();
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(getResources().getString(R.string.not_able_to_proceed),drawer_layout);

                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_USER_ID,strUserId);
                params.put(Constants.KEY_TOTAL_PAYMENT, strTotalPayment);
                params.put(Constants.KEY_DATE, strDate);
                params.put(Constants.KEY_DELIVERY_DATE, strDeliveryDate);
                params.put(Constants.KEY_DELIVERY_TIME, strTime);
                params.put(Constants.KEY_QUANTITY, strQuantity);
                params.put(Constants.KEY_DELIVERY_CHARGE, strDeliveryCharge);
                params.put(Constants.KEY_PAYMENT_METHOD, strPaymentMethod);

                params.put(Constants.KEY_SERVICE_CHARGE, strServiceCharge);
                params.put(Constants.KEY_CART, strCart);
                params.put(Constants.KEY_SHOPID, strShopId);
                params.put(Constants.KEY_TRANS_ID, strTransId);
                params.put(Constants.KEY_DELIVERY_OPTION, strDeliveryOption);

                Log.e("send> data>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
    public void getTimesOfShop()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ProceedDetailActivity.this);
        pd.setMessage(getResources().getString(R.string.please_wait));
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.GET_TIME_OF_SHOP,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        String str_response = "";
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response = jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                Log.e("time",object.getString("data"));
                                strTimes = object.getString("data").split(",");

                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_OPEN_DATE,strOpenDate);
                params.put(Constants.KEY_SHOPID, strShopId);

                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
}