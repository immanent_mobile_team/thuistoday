package com.app.thuistoday;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;

/**
 * Created by Admin on 2/23/2017.
 */

public class EditProfile extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener  {
    private EditText edtAddress,edtName,edtEmail,edtPostCode,edtCity,edtPassword,edtRepeatPassword,edtPhoneNumber;
    DrawerLayout drawer_layout,drawer;
    RecyclerView rv_navigation;
    boolean isDrawerFromHome,Ison;
    Intent intent;
    private ImageView ivProfile;
    private RelativeLayout rlCart;
    String strName="",strAddress="",strPostCode="",strCity="",strPhoneNumber="",strEmail="",
     strPassword="",strRepeatPassword="",strUserId="",strId="",str_picUrl="";
    boolean isSelect=false;
    private LinearLayout llParent;
    private ProgressDialog pd;
    private int mCartItems=0;
    private TextView tvCartItems,tvUpdate;
    private Button btn_signIN,   btn_register;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),EditProfile.this));
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(EditProfile.this);
        View header = navigationView.getHeaderView(0);
        btn_signIN=(Button) header.findViewById(R.id.btn_signIN);
        btn_register=(Button) header.findViewById(R.id.btn_register);
        btn_register.setVisibility(View.GONE);
        btn_signIN.setVisibility(View.GONE);
//        iv_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn=true;
//                intent=new Intent(MyAccount.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        iv_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister=true;
//                intent=new Intent(MyAccount.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        strUserId= MySharedPreferences.str_userId;
//        Toast.makeText(this, ""+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
        if (!MySharedPreferences.str_userId.equals("")){
            tvUpdate.setText(getResources().getString(R.string.update));

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MySharedPreferences.str_name.length()>0)
        {
            edtAddress.setText(MySharedPreferences.str_address);
            edtName.setText(MySharedPreferences.str_name);
            edtPostCode.setText(MySharedPreferences.str_postCode);
            edtPhoneNumber.setText(MySharedPreferences.str_phoneNumber);
            edtCity.setText(MySharedPreferences.str_city);
        }
        mCartItems=0;
        RealmController rel = new RealmController(EditProfile.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");
        Constants.setImage(MySharedPreferences.str_picUrl,ivProfile);
    }

    @Override
    public void onClick(View v) {
       switch (v.getId())
       {
           case R.id.tvUpdate:
               profileUpdate();
               break;
           case R.id.rl_cartLayout:
               intent = new Intent(EditProfile.this,CheckoutActivity.class);
               startActivity(intent);
               break;
       }
    }
    public void findIds()
    {
        tvCartItems = (TextView)findViewById(R.id.tvCartItems);
        ((TextView)findViewById(R.id.tv_toolBar)).setText(getResources().getString(R.string.update_profile));
        tvUpdate= (TextView) findViewById(R.id.tvUpdate);
        drawer_layout=(DrawerLayout)findViewById(R.id.drawer_layout);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        drawer_layout.setOnClickListener(this);
        edtName = (EditText) findViewById(R.id.edtName);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtCity = (EditText) findViewById(R.id.edt_city);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPostCode= (EditText) findViewById(R.id.edt_postCode);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtRepeatPassword = (EditText) findViewById(R.id.edt_repeatPassword);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtPhoneNumber = (EditText) findViewById(R.id.edt_phoneNumber);
        llParent = (LinearLayout) findViewById(R.id.llParent);

        rlCart = (RelativeLayout) findViewById(R.id.rl_cartLayout);
        rlCart.setOnClickListener(this);
        tvUpdate.setOnClickListener(this);
        tvCartItems.setOnClickListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return false;
    }
    public void profileUpdate()
    {
        strName = edtName.getText().toString();
        strAddress = edtAddress.getText().toString();
        strPostCode = edtPostCode.getText().toString();
        strCity = edtCity.getText().toString();
        strPhoneNumber = edtPhoneNumber.getText().toString();

        if (strName.trim().length()>0 && strAddress.trim().length()>0 && strPostCode.length()>0
                &&strCity.trim().length()>0&&strPhoneNumber.length()>0)
        {
//             if (strPassword.equals(strRepeatPassword))
                updateProfile();
//              else
//                 AppController.constants.showSnackBar(getResources().getString(R.string.please_enter_same_password),llParent);
        }
        else
        {
            if (strName.trim().length()==0 )
                AppController.constants.showSnackBar(getResources().getString(R.string.please_enter_name),llParent);
            else if ( strAddress.trim().length()==0 )
                AppController.constants.showSnackBar(getResources().getString(R.string.please_enter_address),llParent);
            else if (strPostCode.length()==0 )
                AppController.constants.showSnackBar(getResources().getString(R.string.please_enter_postcode),llParent);
            else if (strCity.trim().length()==0 )
                AppController.constants.showSnackBar(getResources().getString(R.string.please_enter_city),llParent);
            else if (strPhoneNumber.length()==0 )
                AppController.constants.showSnackBar(getResources().getString(R.string.please_enter_phone_number),llParent);

        }

    }

    private void updateProfile() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(EditProfile.this);
        pd.setMessage(getResources().getString(R.string.please_wait));
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL_UPDATE_PROFILE,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        String str_response = "";
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response = jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                JSONObject jsonObject= null;
                                try {
                                    jsonObject = object.getJSONObject("data");
                                    strUserId=jsonObject.getString("id");
                                    strAddress=jsonObject.getString("address");
                                    strCity=jsonObject.getString("city");
                                    strEmail=jsonObject.getString("email");
                                    strName=jsonObject.getString("name");
//                                    str_password=jsonObject.getString("password");
                                    strPhoneNumber=jsonObject.getString("phone");
                                    str_status=jsonObject.getString("status");
                                    strPostCode=jsonObject.getString("zipcode");
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                AppController.mySharedPreferences.saveUserDetail(strUserId,strId,strAddress,strAddress,strEmail,strName,strPassword,strPhoneNumber,str_status,strPostCode, str_picUrl);


//                                alOrders.clear();
//                                try {
//                                    JSONArray jsonArray=object.getJSONArray("data");
//                                    for (int i=0;i<jsonArray.length();i++) {
//                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                        str_categoryId= jsonObject.getString("category_id");
//                                        str_categoryName=jsonObject.getString("category_name");
//                                        str_image=jsonObject.getString("image");
//                                        str_imageId=jsonObject.getString("image_id");
//                                        strStatus = jsonObject.getString("status");
//                                        hashMap = new HashMap<String, String>();
//                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
//                                        hashMap.put(Constants.CATEGORY_NAME,str_categoryName);
//                                        hashMap.put(Constants.IMAGE,str_image);
//                                        hashMap.put(Constants.IMAGE_ID,str_imageId);
//                                        hashMap.put(Constants.STATUS,strStatus);
//                                        shopTypeList.add(hashMap);
//                                    }
//                                    adapter=new ShopTypeAdapter(context,shopTypeList);
//                                }
//                                catch (Exception e){
//                                    e.printStackTrace();
//                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
//
//                                }
//                                Snackbar snackbar = Snackbar
//                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_LONG)
//                                        .setAction("Ok", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Toast.makeText(HomeActivity.this, "Okay", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//
//                                snackbar.show();

//                                        Log.e("UserName>>",str_userName);
//                                        AppController.mySharedPreferences.saveUserDetail(str_userName);
//                                        editor= MySharedPreferences.sharedPreferences.edit();
//                                        editor.putString(Constants.key_userId,str_userId);
//                                        editor.putString(Constants.key_firstName,str_firstName);
//                                        editor.putString(Constants.key_lastName,str_last_name);
//                                        editor.putString(Constants.key_dob,str_dob);
//                                        editor.putString(Constants.key_email,strEmail);
//                                        editor.putString(Constants.key_userType,str_userType);
//                                        editor.putString(Constants.key_payPalId,str_payPalId);
//                                        editor.putString(Constants.key_profilePic,str_profilePic);
//                                        editor.putString(Constants.key_isVerified,str_isVerified);
//                                        editor.putString(Constants.key_FbProfile,str_picUrl);
//                                        editor.putString(Constants.key_deviceId,str_device_id);
//                                        editor.putString(Constants.key_userName,str_userName);
//                                        editor.commit();
//                                        intent=new Intent(RegistrationActivity.this,Search.class);
//                                        startActivity(intent);
//                                        finish();
                                Toast.makeText(EditProfile.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_NAME, strName);
                params.put(Constants.KEY_ADDRESS, strAddress);
                params.put(Constants.KEY_POST_CODE, strPostCode);
                params.put(Constants.KEY_PHONE, strPhoneNumber);
                params.put(Constants.KEY_PASSWORD, strPassword);
                params.put(Constants.KEY_CITY, strCity);
                params.put(Constants.KEY_EMAIL, strEmail);

                params.put(Constants.KEY_USER_ID,strUserId);

                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
}
