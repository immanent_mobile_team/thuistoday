package com.app.thuistoday;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import Util.Constants;
import Util.MySharedPreferences;
import adapter.CheckOutListAdapter;
import adapter.NavigationAdapter;
import adapter.ShopListAdapter;
import adapter.ShopTypeAdapter;
import io.realm.RealmResults;
import volley.AppController;

import static android.view.View.GONE;

/**
 * Created by HOME on 1/17/2017.
 */

public class CheckoutActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
        TextView tv_toolBar,tvCartItems,tv_checkMyAccountLogin;
        Button btn_checkOut,btn_clearAll,btn_signIN,btn_register;
        public static TextView tvTotalPrice;
        LinearLayout ll_home, ll_myAccountLogin, ll_needHelp;
        RelativeLayout rl_chooseShopType,rl_choose;
        Dialog dialog;
        ImageView iv_close,ivProfile;
        RecyclerView rv_chooseShopType, rv_navigation,rv_checkOutList;
        Intent intent;
        ArrayList<HashMap<String,String >> alCartItems;
        CheckOutListAdapter checkOutListAdapter;
        int mCartItems = 0;
         public static boolean isFromCheckout;
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_checkout);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            findIds();
            alCartItems = new ArrayList<>();


            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            rv_navigation.setLayoutManager(mLayoutManager);
            rv_navigation.setItemAnimator(new DefaultItemAnimator());
            rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),CheckoutActivity.this));

            final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.setDrawerIndicatorEnabled(false);
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.top_left_icon, this.getTheme());
            toggle.setHomeAsUpIndicator(drawable);
            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drawer.isDrawerVisible(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }
            });
            toggle.syncState();
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            View header = navigationView.getHeaderView(0);
            btn_signIN = (Button) header.findViewById(R.id.btn_signIN);
            btn_register = (Button) header.findViewById(R.id.btn_register);
//            btn_signIN.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    HomeActivity.isSignIn=true;
//                    intent=new Intent(CheckoutActivity.this,RegistrationActivity.class);
//                    startActivity(intent);
//                }
//            });
//            btn_register.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    HomeActivity.isRegister=true;
//                    intent=new Intent(CheckoutActivity.this,RegistrationActivity.class);
//                    startActivity(intent);
//                }
//            });
            ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
            ivProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                    if (sp!=null)
                    {
                        if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                        {

                            intent=new Intent(CheckoutActivity.this,MyAccount.class);
                            startActivity(intent);
                        }
                    }
                }
            });
//            Toast.makeText(this, ""+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
            if (!MySharedPreferences.str_userId.equals("")){
                tv_checkMyAccountLogin.setText(getResources().getString(R.string.my_account));
                btn_signIN.setVisibility(GONE);
                btn_register.setVisibility(GONE);
                
                ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i=new Intent(CheckoutActivity.this,MyAccount.class);
                        startActivity(i);

                    }
                });
                ViewGroup.MarginLayoutParams marginLayoutParams =
                        (ViewGroup.MarginLayoutParams) rv_navigation.getLayoutParams();
                marginLayoutParams.setMargins(0,(int)getResources().getDimension(R.dimen._120sp), 0, 0);
                rv_navigation.setLayoutParams(marginLayoutParams);
            }else{
                tv_checkMyAccountLogin.setText(getResources().getString(R.string.login));
                btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn=true;
                    intent=new Intent(CheckoutActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister=true;
                    intent=new Intent(CheckoutActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
                ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i=new Intent(CheckoutActivity.this,RegistrationActivity.class);
                        startActivity(i);
                    }
                });
            }

        }
        private void findIds() {
            tv_toolBar = (TextView) findViewById(R.id.tv_toolBar);
            tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
            tvCartItems = (TextView) findViewById(R.id.tvCartItems);
            tv_checkMyAccountLogin=(TextView)findViewById(R.id.tv_checkMyAccountLogin);
            rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
            tv_toolBar.setText(getResources().getString(R.string.ZagroosStoreActivityTitle));
            ll_myAccountLogin = (LinearLayout) findViewById(R.id.ll_myAccountLogin);
            ll_home=(LinearLayout)findViewById(R.id.ll_home);
            btn_checkOut=(Button) findViewById(R.id.btn_checkOut);
            btn_clearAll=(Button) findViewById(R.id.btn_clearAll);

            ll_needHelp = (LinearLayout) findViewById(R.id.ll_needHelp);
            rv_checkOutList=(RecyclerView)findViewById(R.id.rv_checkOutList);
            ll_myAccountLogin.setOnClickListener(this);
            ll_needHelp.setOnClickListener(this);
            ll_home.setOnClickListener(this);
            btn_checkOut.setOnClickListener(this);
            btn_clearAll.setOnClickListener(this);


        }

        @Override
        public void onBackPressed() {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
            else {
                super.onBackPressed();
            }
        }

    @Override
    protected void onResume() {
        super.onResume();
        if (alCartItems!=null && alCartItems.size()>0)
            alCartItems.clear();
        RealmController rel = new RealmController(CheckoutActivity.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        double totalPrice=0;
        if (myRealms.size()!=0)
        {
            for (MyRealm myRealm: myRealms)
            {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(Constants.KEY_ITEM_NAME,myRealm.getStrItemName());
                hashMap.put(Constants.KEY_ITEM_DESCRIPTION,myRealm.getStrDescription());
                hashMap.put(Constants.KEY_ITEM_PRICE,myRealm.getStrPrice());
                hashMap.put(Constants.KEY_ITEM_QUANTITY,myRealm.getStrNumberOfItems());
                hashMap.put(Constants.KEY_ITEM_WEIGHT_UNIT,myRealm.getStrWeightUnit());
                alCartItems.add(hashMap);
                String str = myRealm.getStrPrice().replace(",",".");
//                 String str = myRealm.getStrPrice().replace(".",",");
                double dbl= Double.parseDouble(str);

                totalPrice = totalPrice + dbl;
            }
            tvCartItems.setText(String.valueOf(myRealms.size()));
            Log.e("list><",alCartItems.toString());


        }
        RecyclerView.LayoutManager mLayoutManagershopList = new LinearLayoutManager(getApplicationContext());
        rv_checkOutList.setLayoutManager(mLayoutManagershopList);
        rv_checkOutList.setItemAnimator(new DefaultItemAnimator());
        checkOutListAdapter = new CheckOutListAdapter(alCartItems,tvCartItems,CheckoutActivity.this,btn_clearAll);
        rv_checkOutList.setAdapter(checkOutListAdapter);
        Log.e("totalPRice", String.valueOf(roundOff(Float.parseFloat(totalPrice+""),2)).replace(".",","));
        CheckoutActivity.tvTotalPrice.setText("Total price: €"+ String.valueOf(roundOff(Float.parseFloat(totalPrice+""),2)).replace(".",","));
        Constants.setImage(MySharedPreferences.str_picUrl,ivProfile);
    }
    static float roundOff(float x, int position)
    {
        float a = x;
        double temp = Math.pow(10.0, position);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }
    @SuppressWarnings("StatementWithEmptyBody")
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            // Handle navigation view item clicks here.
            int id = item.getItemId();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
//                case R.id.ll_myAccountLogin:
//                    HomeActivity.isRegister=false;
//                    intent = new Intent(this, RegistrationActivity.class);
//                    startActivity(intent);
//                    break;
                case R.id.ll_needHelp:
                    break;
                case R.id.ll_home:
                    intent = new Intent(this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                case R.id.btn_checkOut:
                        if (alCartItems!=null&&alCartItems.size()>0)
                        {
                           if (!MySharedPreferences.str_userId.equals(""))
                           {
                                    Log.e("Cart_VAlues",alCartItems.toString());
                               intent = new Intent(CheckoutActivity.this,ProceedDetailActivity.class);
                               startActivity(intent);

                           }
                            else
                           {
                               isFromCheckout =true;
                               intent = new Intent(CheckoutActivity.this,RegistrationActivity.class);
                               startActivity(intent);
                           }

                        }
                       else
                            Toast.makeText(this, getResources().getString(R.string.there_is_no_value_in_cart), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.btn_clearAll:
                    int size = this.alCartItems.size();
                    this.alCartItems.clear();
                    checkOutListAdapter.notifyItemRangeRemoved(0, size);


                    RealmController rela = new RealmController(getApplication());
                    rela.clearAll();
                    RealmResults<MyRealm> myRealmsa = rela.getAllData();
                    if (myRealmsa.size()!=0)
                    {
                        mCartItems = 0;
                        for (MyRealm a : myRealmsa)
                        {
                            mCartItems = mCartItems+1;
                        }
                        tvCartItems.setText(String.valueOf(mCartItems));
                    }
                    else
                        tvCartItems.setText("0");
                    tvTotalPrice.setText("Total price: €0,0");
                    break;
//                case R.id.rl_chooseShopType:
//                    dialog = new Dialog(com.app.thuistoday.CheckoutActivity.this, R.style.NewDialog);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
//                    lp.dimAmount = 0.8f;
//                    dialog.getWindow().setAttributes(lp);
//                    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//                    dialog.setCancelable(false);
//                    dialog.setContentView(R.layout.choose_shop_type_pop_up);
//                    iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
//                    rl_choose=(RelativeLayout)dialog.findViewById(R.id.rl_choose);
//                    rv_chooseShopType = (RecyclerView) dialog.findViewById(R.id.rv_chooseShopType);
//                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//                    rv_chooseShopType.setLayoutManager(mLayoutManager);
//                    rv_chooseShopType.setItemAnimator(new DefaultItemAnimator());
//                    rv_chooseShopType.setAdapter(new ShopTypeAdapter(new ArrayList<HashMap<String, String>>()));
//                    iv_close.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            dialog.dismiss();
//                        }
//                    });
//                    rl_choose.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
//                    dialog.show();
//                    break;
            }
        }
    }
