package com.app.thuistoday;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Util.MySharedPreferences;
import adapter.Setting_profileAdapter;
import volley.AppController;

import static com.app.thuistoday.R.id.iv_LoginIcon;

public class SettingProfile extends AppCompatActivity {
    public static final String TAG="mytag";
    String[] str_setting_TextNames;
    String[] str_language;
    int[] for_lt_icon={
            R.drawable.change_pass_setting,
            R.drawable.lang_icon_setting,
            R.drawable.logout_icon_setting
    };
    int[] for_rt_icons={
            R.drawable.arrow_icon_setting,
            R.drawable.arrow_icon_setting,
            R.drawable.arrow_icon_setting
    };
    int[] for_toggle_icons={
            R.drawable.push_btn_green,
            R.drawable.push_btn_green,
            R.drawable.push_btn_green
    };
    String[] str_english;
    String[] str_dutch;
    Context context;
    RecyclerView rv_setting;
    RecyclerView.Adapter rv_settingAdapter;
    RecyclerView.LayoutManager lm_recyclervw;
    ImageView ivLoginICon,iv_cartIcon;
    Toolbar tb_toolbarsetting;
    RelativeLayout rl_layoutSetting_Back;
    TextView tv_checkMyAccountLogin,tv_toolBar;
    RelativeLayout rl_cartLayout;
    boolean isSelect=true;
    String strName="";
    String strAddress="";
    String strPostCode="";
    String strPhoneNumber="";
    String strEmail="";
    String strPassword="";
    String strUserId="";
    String strId="";
    String str_picUrl="";
    String str_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_profile);
        context=getApplicationContext();
        findIds();


        lm_recyclervw=new LinearLayoutManager(context);
        rv_setting.setLayoutManager(lm_recyclervw);
        Log.e("onCreate:65", MySharedPreferences.str_userId);
        str_setting_TextNames=new String[]{
                context.getString(R.string.change_password),
                context.getString(R.string.language),
                context.getString(R.string.logout),
        };
        str_language=new String[]{
                context.getString(R.string.language)
        };
        str_english=new String[]{
                context.getString(R.string._english),
                context.getString(R.string._english),
                context.getString(R.string._english)
        };
        str_dutch=new String[]{
                context.getString(R.string._dutch),
                context.getString(R.string._dutch),
                context.getString(R.string._dutch)

        };

        if (!MySharedPreferences.str_userId.equals("")) {
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.my_account));

            rv_settingAdapter = new Setting_profileAdapter(context, for_lt_icon, str_setting_TextNames, str_english, for_toggle_icons, str_dutch, for_rt_icons);
        }
        else {
            rv_settingAdapter = new Setting_profileAdapter(context, for_lt_icon, str_language, str_english, for_toggle_icons, str_dutch, for_rt_icons);
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.login));
        }
        rv_setting.setAdapter(rv_settingAdapter);
        ivLoginICon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MySharedPreferences.str_userId.equals("")){
                    Intent i=new Intent(getApplicationContext(),RegistrationActivity.class);
                     startActivity(i);
                }
                else
                {
                    Intent i=new Intent(getApplicationContext(),MyAccount.class);
                    startActivity(i);
                }
            }
        });

        iv_cartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),HomeActivity.class);
                startActivity(i);
            }
        });
        rl_layoutSetting_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rl_cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SettingProfile.this,CheckoutActivity.class);
                startActivity(i);
            }
        });

    }

    private void findIds() {
        rv_setting=(RecyclerView)findViewById(R.id.rv_settings);
        ivLoginICon=(ImageView)findViewById(R.id.iv_LoginIcon);
        tb_toolbarsetting=(Toolbar)findViewById(R.id.toolbar);
        tv_toolBar=(TextView)findViewById(R.id.tv_toolBar);
        tv_toolBar.setText(getResources().getString(R.string.setting));
        tv_toolBar.setPadding(35,0,0,0);
        iv_cartIcon=(ImageView)findViewById(R.id.iv_cartIcon);
        rl_layoutSetting_Back=(RelativeLayout)findViewById(R.id.rl_layoutSetting_Back);
        rl_layoutSetting_Back.setVisibility(View.VISIBLE);
        tv_checkMyAccountLogin=(TextView)findViewById(R.id.tv_checkMyAccountLogin);
        rl_cartLayout=(RelativeLayout)findViewById(R.id.rl_cartLayout);
    }


}

