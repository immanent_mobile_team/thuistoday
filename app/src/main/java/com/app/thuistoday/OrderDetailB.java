package com.app.thuistoday;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import adapter.OrderDetailBAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;

/**
 * Created by Admin on 2/28/2017.
 */

public class OrderDetailB extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private TextView tvOrderNumber,tvOrderPlacedOn,tvOrderStatus,tvOrderDeliveryAddress,tvPaymentMethod,
                     tvDate, tvDownloadInvoice ,tvCartItems;
    private Button btn_signIN,btn_register;
    public static TextView tvTotalPrice;
    private RelativeLayout rlStatus;
    private RecyclerView recyclerView,rv_navigation;
    private ProgressDialog pd;
    private DrawerLayout drawer_layout;
    private String strOrderId;
    private HashMap<String ,String > hashMap;
    private Intent intent;
    private ImageView ivProfile;
    private boolean isDrawerFromHome;
    private int mCartItems=0;
    private Toolbar toolbar;
    private   ArrayList<JSONObject> arrayList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail_b);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();
        drawLayout();
       hashMap = (HashMap<String, String>) getIntent().getSerializableExtra(Constants.KEY_HASH_MAP);
        arrayList = new ArrayList<>();
        Log.e("hash>",hashMap.toString());
        initialize();
//        Toast.makeText(this, hashMap.get(Constants.KEY_CART), Toast.LENGTH_SHORT).show();
        JSONObject jsonObject= null;
        try {
            jsonObject = new JSONObject(hashMap.get(Constants.KEY_CART));
        } catch (JSONException e) {
            e.printStackTrace();
        }
         Log.e("Cart>><", jsonObject.toString() );

//        Iterator<String> iter = jsonObject.keys();
//        while (iter.hasNext()) {
//            String key = iter.next();
//            try {
//                Object value = jsonObject.get(key);
//                Toast.makeText(this,value.toString(), Toast.LENGTH_SHORT).show();
//            } catch (JSONException e) {
//                // Something went wrong!
//            }
//        }


        Iterator<String> keys = jsonObject.keys();
        // get some_name_i_wont_know in str_Name
        double d = 0;
        do
        {
            try {
                String value="";
            if (keys.hasNext())
            {
                String str_Name = keys.next();
                // get the value i care about
                 value = jsonObject.optString(str_Name);

            }
             else  {
                return;
            }


                JSONObject jsonObject1=  new JSONObject(value);


                double a=0;
                Log.e("cart>>",jsonObject1.toString());
                a= Double.parseDouble(jsonObject1.getString("subtotal").replace(",","."));
                d = d+a;

                arrayList.add(jsonObject1);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
            }
        }while (keys.hasNext());
        double x=d;
        DecimalFormat df = new DecimalFormat("#.##");
        String dx=df.format(x);
        x=Double.valueOf(dx);
//        tvTotalPrice.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
           tvTotalPrice.setText("TOTAL PRICE: €"+String.valueOf(x).replace(".",","));

        Log.e("list>>",arrayList.toString());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new OrderDetailBAdapter(arrayList));

    }


    @Override
    protected void onResume() {
        super.onResume();
        mCartItems=0;
        RealmController rel = new RealmController(OrderDetailB.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");
    }

    private void drawLayout() {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),OrderDetailB.this));
        drawer_layout= (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer_layout, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer_layout.isDrawerVisible(GravityCompat.START)) {
                    drawer_layout.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer_layout.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN=(Button) header.findViewById(R.id.btn_signIN);
        btn_register=(Button) header.findViewById(R.id.btn_register);
        btn_signIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isSignIn=true;
                intent=new Intent(OrderDetailB.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isRegister=true;
                intent=new Intent(OrderDetailB.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {
                        HomeActivity.isRegister=true;
                        intent=new Intent(OrderDetailB.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // Toast.makeText(this, ""+id, Toast.LENGTH_SHORT).show();
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void findIds() {
        tvTotalPrice = (TextView) findViewById(R.id.tvTotalPrice);
        ((TextView)findViewById(R.id.tv_toolBar)).setText("Order Detail");
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        tvPaymentMethod = (TextView) findViewById(R.id.tvPaymentMethod);
        tvCartItems = (TextView) findViewById(R.id.tvCartItems);
        tvDownloadInvoice = (TextView) findViewById(R.id.tvDownloadInvoice);
        tvOrderPlacedOn = (TextView) findViewById(R.id.tvOrderPlace);
        tvOrderDeliveryAddress = (TextView) findViewById(R.id.tvDeliveryAddress);
        tvOrderNumber= (TextView) findViewById(R.id.tvOrderNumber);
        tvOrderStatus = (TextView) findViewById(R.id.tv_orderStatus);
        tvDate = (TextView) findViewById(R.id.tvDate);
        rlStatus = (RelativeLayout) findViewById(R.id.rlOrderStatus);
        recyclerView = (RecyclerView) findViewById(R.id.rv_orderDetailList);
        tvDownloadInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(OrderDetailB.this, "under development", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void initialize()
    {
        tvOrderNumber.setText(hashMap.get(Constants.KEY_ID));
        tvDate.setText(hashMap.get(Constants.KEY_DATE));
        tvOrderPlacedOn.setText(hashMap.get(Constants.KEY_DATE));
        tvOrderDeliveryAddress.setText("on "+hashMap.get(Constants.KEY_DELIVERY_DATE));
        tvOrderDeliveryAddress.setText(MySharedPreferences.str_address);
        tvPaymentMethod.setText(hashMap.get(Constants.KEY_PAYMENT_METHOD));


    }
    public void orderdetail()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(OrderDetailB.this);
        pd.setMessage(getResources().getString(R.string.please_wait));
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST,
                Constants.ORDER_HISTORY_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        String str_response = "";
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response = jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
//                                alOrders.clear();
//                                try {
//                                    JSONArray jsonArray=object.getJSONArray("data");
//                                    for (int i=0;i<jsonArray.length();i++) {
//                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                                        str_categoryId= jsonObject.getString("category_id");
//                                        str_categoryName=jsonObject.getString("category_name");
//                                        str_image=jsonObject.getString("image");
//                                        str_imageId=jsonObject.getString("image_id");
//                                        strStatus = jsonObject.getString("status");
//                                        hashMap = new HashMap<String, String>();
//                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
//                                        hashMap.put(Constants.CATEGORY_NAME,str_categoryName);
//                                        hashMap.put(Constants.IMAGE,str_image);
//                                        hashMap.put(Constants.IMAGE_ID,str_imageId);
//                                        hashMap.put(Constants.STATUS,strStatus);
//                                        shopTypeList.add(hashMap);
//                                    }
//                                    adapter=new ShopTypeAdapter(context,shopTypeList);
//                                }
//                                catch (Exception e){
//                                    e.printStackTrace();
//                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
//
//                                }
//                                Snackbar snackbar = Snackbar
//                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_LONG)
//                                        .setAction("Ok", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Toast.makeText(HomeActivity.this, "Okay", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//
//                                snackbar.show();

//                                        Log.e("UserName>>",str_userName);
//                                        AppController.mySharedPreferences.saveUserDetail(str_userName);
//                                        editor= MySharedPreferences.sharedPreferences.edit();
//                                        editor.putString(Constants.key_userId,str_userId);
//                                        editor.putString(Constants.key_firstName,str_firstName);
//                                        editor.putString(Constants.key_lastName,str_last_name);
//                                        editor.putString(Constants.key_dob,str_dob);
//                                        editor.putString(Constants.key_email,strEmail);
//                                        editor.putString(Constants.key_userType,str_userType);
//                                        editor.putString(Constants.key_payPalId,str_payPalId);
//                                        editor.putString(Constants.key_profilePic,str_profilePic);
//                                        editor.putString(Constants.key_isVerified,str_isVerified);
//                                        editor.putString(Constants.key_FbProfile,str_picUrl);
//                                        editor.putString(Constants.key_deviceId,str_device_id);
//                                        editor.putString(Constants.key_userName,str_userName);
//                                        editor.commit();
//                                        intent=new Intent(RegistrationActivity.this,Search.class);
//                                        startActivity(intent);
//                                        finish();
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_ORDER_ID,strOrderId);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
}
