package com.app.thuistoday;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Util.ConnectionDetector;
import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import adapter.ShopTypeAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;


public class RegistrationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    TextView tv_toolBar,tv_forgotPassword,tvCartItems,tv_fbloginbtn,tv_loginBtn;
    LinearLayout ll_login,ll_registration;
    ImageView iv_signInAccount,iv_close;
    RecyclerView rv_navigation;
    EditText edt_name,edt_address,edt_postcode,edt_city,edt_phoneNumber,edt_email,edt_passwordRegistration,edt_repeatPassword,
            edt_username,edt_password,edt_email_forgotPass,edt_oldPassword,edt_newPassword,edt_confirm_password
            ,edt_otp;
    boolean isRegistration=true,Ison;
    String str_loginAccount;
    Intent intent;
    RelativeLayout rl_cartLayout,rl_send,rl_save,rl_sumit;
    ProgressDialog pd;
    String str_response="",str_name="",str_address="",str_postCode="",str_city="",str_phoneNumber="",str_email="",str_password="",strRepeat_password="",str_userName="",strId="",strStatus="",str_email_forgotPass="",
    str_oldPassword="",str_newPassword="",str_confirmPassword="",str_picUrl="",str_otp="",str_receivedOtp="";
    android.support.v7.app.AlertDialog alertDialog;
    DrawerLayout drawer_layout;
    ConnectionDetector cd;
    SharedPreferences.Editor editor;
    Snackbar snackbar;
    Dialog dialog;
    Button btn_registerBtn,btn_signIN_for_nav,btn_register_for_nav;
    int mCartItems;
    LoginButton loginBtnfb;
    CallbackManager callbackManagerfb;
    String str_fb_lName="",str_fb_fName="",str_fbId="",str_fb_email="",str_fb_name="";
    public static final String TAG="mytag";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());//initialize fb sdk
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),RegistrationActivity.this));
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        final Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN_for_nav=(Button) header.findViewById(R.id.btn_signIN);
        btn_register_for_nav=(Button) header.findViewById(R.id.btn_register);

        btn_signIN_for_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isSignIn=true;
                drawer.closeDrawer(GravityCompat.START);
                iv_signInAccount.setImageResource(R.drawable.push_btn_green);
                str_loginAccount="login";
                tv_toolBar.setText(getResources().getString(R.string.please_login));
                ll_registration.setVisibility(View.GONE);
                ll_login.setVisibility(View.VISIBLE);
                isRegistration=true;
                HomeActivity.isRegister=false;

            }
        });
        btn_register_for_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isSignIn=false;
                HomeActivity.isRegister=true;
                drawer.closeDrawer(GravityCompat.START);
                iv_signInAccount.setImageResource(R.drawable.push_btn_green1);
                str_loginAccount="registration";
                tv_toolBar.setText(getResources().getString(R.string.registerAnAccount));
                ll_registration.setVisibility(View.VISIBLE);
                ll_login.setVisibility(View.GONE);
                isRegistration=false;
                HomeActivity.isSignIn=false;
            }
        });
        if (HomeActivity.isSignIn==true)
        {
            iv_signInAccount.setImageResource(R.drawable.push_btn_green);
            str_loginAccount="login";
            tv_toolBar.setText(getResources().getString(R.string.please_login));
            ll_registration.setVisibility(View.GONE);
            ll_login.setVisibility(View.VISIBLE);
            isRegistration=true;
            HomeActivity.isRegister=false;
        }else {
            iv_signInAccount.setImageResource(R.drawable.push_btn_green1);
            str_loginAccount="registration";
            tv_toolBar.setText(getResources().getString(R.string.registerAnAccount));
            ll_registration.setVisibility(View.VISIBLE);
            ll_login.setVisibility(View.GONE);
            isRegistration=false;
            HomeActivity.isSignIn=false;

        }
        if (HomeActivity.isRegister==true){
            iv_signInAccount.setImageResource(R.drawable.push_btn_green1);
            str_loginAccount="registration";
            tv_toolBar.setText(getResources().getString(R.string.registerAnAccount));
            ll_registration.setVisibility(View.VISIBLE);
            ll_login.setVisibility(View.GONE);
            isRegistration=false;
            HomeActivity.isRegister=false;
        }else {
            iv_signInAccount.setImageResource(R.drawable.push_btn_green);
            str_loginAccount="login";
            tv_toolBar.setText(getResources().getString(R.string.please_login));
            ll_registration.setVisibility(View.GONE);
            ll_login.setVisibility(View.VISIBLE);
            isRegistration=true;
            HomeActivity.isSignIn =false;
        }

        loginBtnfb.setReadPermissions(Arrays.asList(Constants.KEY_FACEBOOK_PUBLIC_PROFILE,"email","basic_info"));
        callbackManagerfb=CallbackManager.Factory.create();
        loginBtnfb.registerCallback(callbackManagerfb, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest=GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                    Log.e("RESPONSE",response.toString());
                        try {
                            str_fbId=object.getString("id");
//                            str_fb_email=object.getString("email");
                            str_fb_fName=object.getString("first_name");
                            str_fb_lName=object.getString("last_name");
                            str_fb_name=object.getString("name");
                            AppController.mySharedPreferences.saveUserDetail(str_fbId,strId,str_address,str_city,str_email,str_fb_name,str_password,str_phoneNumber,strStatus,str_postCode,str_picUrl);

                            registerWithFb();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
;                    }


                });
                Bundle bundle=new Bundle();
                bundle.putString(Constants.KEY_FACEBOOK_FIELDS,Constants.KEY_FACEBOOK_PUT_ID_FNAME_LNAME);
                graphRequest.setParameters(bundle);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        tv_fbloginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginBtnfb.performClick();
            }
        });


   }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManagerfb.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCartItems=0;
        RealmController rel = new RealmController(RegistrationActivity.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
        {
            for (MyRealm realm:myRealms)
            {
                mCartItems = mCartItems+1;
            }
            tvCartItems.setText(String.valueOf(mCartItems));
        }
        else
            tvCartItems.setText(String.valueOf(mCartItems));
    }
    private void findIds() {
        tv_toolBar=(TextView)findViewById(R.id.tv_toolBar);
        tvCartItems = (TextView)findViewById(R.id.tvCartItems);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        tv_toolBar.setText(getResources().getString(R.string.please_login));
        ll_login=(LinearLayout)findViewById(R.id.ll_login);
        ll_registration=(LinearLayout)findViewById(R.id.ll_registration);
        iv_signInAccount=(ImageView)findViewById(R.id.iv_signInAccount);
        edt_name=(EditText)findViewById(R.id.edt_name);
        edt_address=(EditText)findViewById(R.id.edt_address);
        edt_postcode=(EditText)findViewById(R.id.edt_postcode);
        edt_city=(EditText)findViewById(R.id.edt_city);
        edt_phoneNumber=(EditText)findViewById(R.id.edt_phoneNumber);
        edt_email=(EditText)findViewById(R.id.edt_email);
        edt_passwordRegistration=(EditText)findViewById(R.id.edt_passwordRegistration);
        edt_repeatPassword=(EditText)findViewById(R.id.edt_repeatPassword);
        btn_registerBtn=(Button) findViewById(R.id.btn_registerBtn);
        edt_username=(EditText)findViewById(R.id.edt_username);
        edt_password=(EditText)findViewById(R.id.edt_password);
        tv_forgotPassword=(TextView) findViewById(R.id.tv_forgotPassword);
        tv_loginBtn=(TextView) findViewById(R.id.tv_loginBtn);
        rl_cartLayout=(RelativeLayout)findViewById(R.id.rl_cartLayout);
        drawer_layout=(DrawerLayout) findViewById(R.id.drawer_layout);
        iv_signInAccount.setOnClickListener(this);
        btn_registerBtn.setOnClickListener(this);
        tv_loginBtn.setOnClickListener(this);
        tv_forgotPassword.setOnClickListener(this);
        rl_cartLayout.setOnClickListener(this);
        loginBtnfb=(LoginButton)findViewById(R.id.loginbtn_fb);
        tv_fbloginbtn=(TextView)findViewById(R.id.iv_fbloginbtn);



//        if (isRegistration==true){
//            Toast.makeText(this, "rE", Toast.LENGTH_SHORT).show();
//            tv_toolBar.setText(getResources().getString(R.string.please_login));
//
//        }else {
//            Toast.makeText(this, "login", Toast.LENGTH_SHORT).show();
//            tv_toolBar.setText(getResources().getString(R.string.please_login));
//        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_signInAccount:
                if (isRegistration==true){
                    iv_signInAccount.setImageResource(R.drawable.push_btn_green1);
                    str_loginAccount="registration";
                    tv_toolBar.setText(getResources().getString(R.string.registerAnAccount));
                    ll_registration.setVisibility(v.VISIBLE);
                    ll_login.setVisibility(View.GONE);
                    isRegistration=false;
                }
                else {
                    iv_signInAccount.setImageResource(R.drawable.push_btn_green);
                    str_loginAccount="login";
                    tv_toolBar.setText(getResources().getString(R.string.please_login));
                    ll_registration.setVisibility(v.GONE);
                    ll_login.setVisibility(View.VISIBLE);
                    isRegistration=true;
                }
                break;
            case R.id.btn_registerBtn:
                str_name=edt_name.getText().toString().trim();
                str_address=edt_address.getText().toString().trim();
                str_postCode=edt_postcode.getText().toString().trim();
                str_city=edt_city.getText().toString().trim();
                str_phoneNumber=edt_phoneNumber.getText().toString().trim();
                str_email=edt_email.getText().toString().trim();
                str_password=edt_passwordRegistration.getText().toString().trim();
                strRepeat_password=edt_repeatPassword.getText().toString().trim();
                Log.e("PAssword>>",str_password);
                strRepeat_password=edt_repeatPassword.getText().toString();
                if (!Constants.isStringEmpty(str_name)&&!Constants.isStringEmpty(str_address)&&!Constants.isStringEmpty(str_postCode)&&!Constants.isStringEmpty(str_city)&&!Constants.isStringEmpty(str_phoneNumber)&&!Constants.isStringEmpty(str_email)&&isEmailValid(str_email)&&!Constants.isStringEmpty(str_password)&&!Constants.isStringEmpty(strRepeat_password)){
                    if (str_password.equals(strRepeat_password)){
                        cd = new ConnectionDetector(this);
                        if (cd.isConnectingToInternet()) {
                            RegistrationMethod();
                        }
                        else {
                            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
                        }
                    }else {
                        AppController.constants.showSnackBar(getResources().getString(R.string.passwordMismatch),drawer_layout);

                    }

                }
                else {
//                    if (str_name.length()==0){
//                        edt_name.requestFocus();
//                        AppController.constants.showSnackBar(getResources().getString(R.string.NameEmpty),drawer_layout);
//                    }
                    if (Constants.isStringEmpty(str_name)){
                        edt_name.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.NameEmpty),drawer_layout);

                    }
                    else if (Constants.isStringEmpty(str_address)){
                        edt_address.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.addressEmpty),drawer_layout);
                    }
                    else if (Constants.isStringEmpty(str_postCode)){
                        edt_postcode.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.postcodeEmpty),drawer_layout);
                    }
                    else if (Constants.isStringEmpty(str_city)){
                        edt_city.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.cityEmpty),drawer_layout);
                    } else if (Constants.isStringEmpty(str_phoneNumber)){
                        edt_phoneNumber.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.phoneNumberEmpty),drawer_layout);
                    }
                    else if (Constants.isStringEmpty(str_email)){
                        edt_email.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.emailEmpty),drawer_layout);
                    }
                    else if(!isEmailValid(str_email)){
                        edt_email.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.email_validation),drawer_layout);
                    }
                    else if (Constants.isStringEmpty(str_password)){
                        edt_passwordRegistration.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.passwordEmpty),drawer_layout);
                    }
                    else if (Constants.isStringEmpty(strRepeat_password)){
                        edt_repeatPassword.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.repeatPasswordEmpty),drawer_layout);
                    }

                }
//                if (HomeActivity.isFromHome==true) {
//                    //Toast.makeText(this, "if", Toast.LENGTH_SHORT).show();
//                    finish();
//                    HomeActivity.isFromHome=false;
//
//                }else {
//                   // Toast.makeText(this, "else", Toast.LENGTH_SHORT).show();
//                    intent = new Intent(this, HomeActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
//                }
                break;
            case R.id.tv_loginBtn:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                str_userName=edt_username.getText().toString().trim();
                str_password=edt_password.getText().toString().trim();

                if (!Constants.isStringEmpty(str_userName)&&isEmailValid(str_userName)&&!Constants.isStringEmpty(str_password)){
                        cd = new ConnectionDetector(this);
                        if (cd.isConnectingToInternet()) {
                            LoginMethod();
                        }
                        else {
                            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
                        }

                }
                else {
                    if (Constants.isStringEmpty(str_userName)){
                        edt_username.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.userNameEmpty),drawer_layout);
                    } else if (Constants.isStringEmpty(str_password)){
                            edt_password.requestFocus();
                            AppController.constants.showSnackBar(getResources().getString(R.string.passwordEmpty),drawer_layout);
                     }
                    else if(!isEmailValid(str_userName)){
                        edt_username.requestFocus();
                        AppController.constants.showSnackBar(getResources().getString(R.string.userName_validation),drawer_layout);
                    }
                    }
//                if (HomeActivity.isFromHome==true) {
//                    //Toast.makeText(this, "if", Toast.LENGTH_SHORT).show();
//                    finish();
//                    HomeActivity.isFromHome=false;
//                }else {
//                   // Toast.makeText(this, "else", Toast.LENGTH_SHORT).show();
//                    intent = new Intent(this, HomeActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
//                }
                break;
            case R.id.tv_forgotPassword:
                dialog = new Dialog(RegistrationActivity.this,R.style.NewDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                lp.dimAmount=0.9f;
                dialog.getWindow().setAttributes(lp);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.forgot_password_pop_up);
                iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
                rl_send=(RelativeLayout)dialog.findViewById(R.id.rl_send);
                edt_email_forgotPass=(EditText)dialog.findViewById(R.id.edt_email_forgotPass);
                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                rl_send.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        str_email_forgotPass =edt_email_forgotPass.getText().toString().trim();
                          forgotPassword();

                    }
                });
                dialog.show();
                break;
            case R.id.rl_cartLayout:
                intent=new Intent(this,CheckoutActivity.class);
                startActivity(intent);
                break;

        }
    }

    /***
     *  REGISTRATION METHOD
     */
    private void RegistrationMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(RegistrationActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.REGISTRATION_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                    snackbar = Snackbar
                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Ok", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                snackbar.dismiss();
//                                                JSONObject jsonObject= null;
//                                                try {
//                                                    jsonObject = object.getJSONObject("data");
//                                                    strId=jsonObject.getString("id");
//                                                    str_address=jsonObject.getString("address");
//                                                    str_city=jsonObject.getString("city");
//                                                    str_email=jsonObject.getString("email");
//                                                    str_name=jsonObject.getString("name");
//                                                    str_password=jsonObject.getString("password");
//                                                    str_phoneNumber=jsonObject.getString("phone");
//                                                    strStatus=jsonObject.getString("status");
//                                                    str_postCode=jsonObject.getString("zipcode");
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                                AppController.mySharedPreferences.saveUserDetail(strId);
//                                                editor= MySharedPreferences.sharedPreferences.edit();
//                                                editor.putString(Constants.key_id,strId);
//                                                editor.putString(Constants.key_address,str_address);
//                                                editor.putString(Constants.key_city,str_city);
//                                                editor.putString(Constants.key_email,str_email);
//                                                editor.putString(Constants.key_name,str_name);
//                                                editor.putString(Constants.key_password,str_password);
//                                                editor.putString(Constants.key_phoneNumber,str_phoneNumber);
//                                                editor.putString(Constants.key_status,strStatus);
//                                                editor.putString(Constants.key_postCode,str_postCode);
//                                                editor.commit();
                                                if (CheckoutActivity.isFromCheckout)
                                                {
                                                    iv_signInAccount.setImageResource(R.drawable.push_btn_green);
                                                    str_loginAccount="login";
                                                    tv_toolBar.setText(getResources().getString(R.string.please_login));
                                                    ll_registration.setVisibility(View.GONE);
                                                    ll_login.setVisibility(View.VISIBLE);
                                                }
                                               else if (HomeActivity.isFromHome==true) {
                                                    finish();
                                                    HomeActivity.isFromHome=false;
                                                }   else {
                                                    intent = new Intent(RegistrationActivity.this, HomeActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);
                                                }
                                            }
                                        });
                                    snackbar.show();
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                                edt_name.getText().clear();
                                edt_email.getText().clear();
                                edt_passwordRegistration.getText().clear();
                                edt_repeatPassword.getText().clear();
                                edt_phoneNumber.getText().clear();
                                edt_postcode.getText().clear();
                                edt_address.getText().clear();
                                edt_city.getText().clear();
                                edt_name.requestFocus();
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                            edt_name.getText().clear();
                            edt_email.getText().clear();
                            edt_passwordRegistration.getText().clear();
                            edt_repeatPassword.getText().clear();
                            edt_phoneNumber.getText().clear();
                            edt_postcode.getText().clear();
                            edt_address.getText().clear();
                            edt_city.getText().clear();
                            edt_name.requestFocus();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                        edt_name.getText().clear();
                        edt_email.getText().clear();
                        edt_passwordRegistration.getText().clear();
                        edt_repeatPassword.getText().clear();
                        edt_phoneNumber.getText().clear();
                        edt_postcode.getText().clear();
                        edt_address.getText().clear();
                        edt_city.getText().clear();
                        edt_name.requestFocus();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_NAME,str_name);
                params.put(Constants.KEY_EMAIL,str_email);
                params.put(Constants.KEY_PASSWORD,str_password);
                params.put(Constants.KEY_PHONE,str_phoneNumber);
                params.put(Constants.KEY_POST_CODE,str_postCode);
                params.put(Constants.KEY_ADDRESS,str_address);
                params.put(Constants.KEY_CITY,str_city);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
    /***
     * LOGIN METHOD
     */
    private void LoginMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(RegistrationActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.LOGIN_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                                JSONObject jsonObject= null;
                                                try {
                                                    jsonObject = object.getJSONObject("data");
                                                    strId=jsonObject.getString("id");
                                                    str_address=jsonObject.getString("address");
                                                    str_city=jsonObject.getString("city");
                                                    str_email=jsonObject.getString("email");
                                                    str_name=jsonObject.getString("name");
                                                    str_password=jsonObject.getString("password");
                                                    str_phoneNumber=jsonObject.getString("phone");
                                                    strStatus=jsonObject.getString("status");
                                                    str_postCode=jsonObject.getString("zipcode");
                                                    str_picUrl=jsonObject.getString("profile_pic");


                                                }
                                                catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                AppController.mySharedPreferences.saveUserDetail(strId,strId,str_address,str_city,str_email,str_name,str_password,str_phoneNumber,strStatus,str_postCode,str_picUrl);

//                                Toast.makeText(RegistrationActivity.this, ""+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
                                                if (HomeActivity.isFromHome==true) {
                                                    finish();
                                                    HomeActivity.isFromHome=false;
                                                }
                                                if (CheckoutActivity.isFromCheckout)
                                                {
                                                    finish();
                                                }
                                                else {
                                                intent = new Intent(RegistrationActivity.this, HomeActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                   }

//                                Toast.makeText(RegistrationActivity.this, ""+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();

                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);

                            }
                        } catch (Exception e)
                        {
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);

                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_EMAIL,str_userName);
                params.put(Constants.KEY_PASSWORD,str_password);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
    /** Email validations
     *
     * @param email
     * @return
     */

    public static boolean isEmailValid(String email) {
        boolean isValid = false;
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    public void showDialog(String str)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setMessage(str);
        builder.setCancelable(false);
        builder.setNegativeButton(getResources().getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 alertDialog.dismiss();
            }
        });
//         alertDialog = builder.create();
//        alertDialog.show();
    }


    /*
    * Forgot Password
    *
    */
   private void forgotPassword() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(RegistrationActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.URL_FORGOT_PASSWORD,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                 str_receivedOtp = object.getString("otp");
                                 showDialogEMail(object);


                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                                if (dialog!=null)
                                dialog.dismiss();
                            }
                        } catch (Exception e)
                        {
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);

                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_EMAIL,str_email_forgotPass);

                Log.e("send data>>",params.toString());
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strRequest.setRetryPolicy(policy);

        queue.add(strRequest);
    }

    private void showDialogEMail(JSONObject object) throws JSONException {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("Alert!");
        builder.setCancelable(false);
        builder.setMessage(object.getString("message"));
        builder.setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               alertDialog.dismiss();
                showOtpDialog();
            }
        });
        alertDialog  = builder.create();
        alertDialog.show();
    }


    private void showOtpDialog() {
        if (dialog!=null)
        dialog.dismiss();
        /***
         * ENTER OTP POP UP
         */
        dialog = new Dialog(RegistrationActivity.this,R.style.NewDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount=0.9f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_otp);
        iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
        rl_sumit =(RelativeLayout)dialog.findViewById(R.id.rl_submit);
        edt_otp =(EditText)dialog.findViewById(R.id.edt_otp);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        rl_sumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String  str_otp  = edt_otp.getText().toString().trim();
                if (str_otp.equals(str_receivedOtp))
                {
                    dialog.dismiss();
                    intent = new Intent(RegistrationActivity.this, ChangePassword.class);
                    intent.putExtra(Constants.VIA_FORGOT_PASS,"forgot");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    AppController.constants.showSnackBar(getString(R.string.otp_does_not_match),drawer_layout);
                }
            }
        });
        dialog.show();
    }
    private void registerWithFb() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(RegistrationActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST
                , Constants.URL_IS_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                JSONObject jsonObject=null;
                try {
                    jsonObject=new JSONObject(response);
                    String str_Response=jsonObject.getString("jwt");
                    Log.e("str_Response",str_Response.trim());

                    Jws<Claims> claims=Jwts.parser()
                            .setSigningKey(Constants.getBase64())
                            .parseClaimsJws(str_Response);
                    Log.e("Claim>>>",claims.toString());
                    Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                    JSONObject jsonObject_status=new JSONObject(claims.getBody());
                    Log.e("datafb",jsonObject.toString());
                    String str_Status=jsonObject_status.getString("status");
                    Log.e("str_status",str_Status.toString().trim());
                    if (str_Status.equals("1")){
//                        Toast.makeText(RegistrationActivity.this, "already registered go to home"+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
                        jsonObject = jsonObject_status.getJSONObject("data");
                        strId=jsonObject.getString("id");
                        str_address=jsonObject.getString("address");
                        str_city=jsonObject.getString("city");
                        str_email=jsonObject.getString("email");
                        str_name=jsonObject.getString("name");
                        str_password=jsonObject.getString("password");
                        str_phoneNumber=jsonObject.getString("phone");
                        strStatus=jsonObject.getString("status");
                        str_postCode=jsonObject.getString("zipcode");
                        str_picUrl=jsonObject.getString("profile_pic");

                        AppController.mySharedPreferences.saveUserDetail(strId,strId,str_address,str_city,str_email,str_name,str_password,str_phoneNumber,strStatus,str_postCode,str_picUrl);
                        if (HomeActivity.isFromHome==true) {
                            finish();
                            HomeActivity.isFromHome=false;
                        }
                        if (CheckoutActivity.isFromCheckout)
                        {
                            finish();
                        }
                        else {
                            intent = new Intent(RegistrationActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }

                    }if (str_Status.equals("2")){
                        Toast.makeText(RegistrationActivity.this, "proceed to verification", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(RegistrationActivity.this,ProceedWithFbRegistration.class);
                        intent.putExtra("id",str_fbId);
                        startActivity(intent);
//
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            @Override
            protected HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_EMAIL,str_fbId);//1
                params.put(Constants.KEY_DEVICE_ID,"d29tms9chd1324732");//2
                params.put(Constants.KEY_DEVICE_TYPE,"android");//3
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(stringRequest);
    }

}
