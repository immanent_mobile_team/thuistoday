package com.app.thuistoday;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import volley.AppController;

import static android.view.View.GONE;

public class ProceedWithFbRegistration extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{
    ProgressDialog pd;

    String str_Status,str_response="",str_name="",str_address="",str_postCode="",str_city="",str_phoneNumber="",str_email="",str_password="",strRepeat_password="",str_userName="",strId="",strStatus="",str_email_forgotPass="",
            str_oldPassword="",str_newPassword="",str_confirmPassword="",str_picUrl="",str_otp="",str_receivedOtp="",str_id_fb
            ;
    EditText edt_name,edt_address,edt_postCode,edt_City,edt_Phone_number;
    Button btn_proceed;
    TextView tv_toolBar;
    private static final int MY_LOCATION_PERMISSION_REQUEST_CODE = 1;
    RecyclerView rv_navigation;
    public static DrawerLayout drawer;
    public static boolean isDrawerFromHome=false,isSignIn=false,isRegister=false;
    Context context;
    Button btn_signIN,btn_register;
    Intent intent;
    ImageView iv_close,ivProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed_with_fb_registration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIDs();
        context = this;
         str_id_fb = getIntent().getStringExtra("id");//setting the id of fb
        str_name=MySharedPreferences.str_name;
        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_name=edt_name.getText().toString();

                str_address = edt_address.getText().toString();
                str_postCode = edt_postCode.getText().toString();
                str_city = edt_City.getText().toString();
                str_phoneNumber = edt_Phone_number.getText().toString();

                if (str_phoneNumber.length() > 0 &&
                        str_postCode.length() > 0 &&
                        str_address.length() > 0 &&
                        str_city.length() > 0 &&
                        str_name.length()>0) {
                    registerWithFbFirstTime();

                } else {
                    Toast.makeText(ProceedWithFbRegistration.this, "Can'nt leave the Field Blank", Toast.LENGTH_SHORT).show();

                }
            }
        });

        /*
        *
        *
        * */
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),ProceedWithFbRegistration.this));
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN=(Button) header.findViewById(R.id.btn_signIN);
        btn_register=(Button) header.findViewById(R.id.btn_register);
        btn_signIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isSignIn=true;
                intent=new Intent(ProceedWithFbRegistration.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isRegister=true;
                intent=new Intent(ProceedWithFbRegistration.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {
                        HomeActivity.isRegister=true;
                        intent=new Intent(ProceedWithFbRegistration.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission((android.Manifest.permission.CAMERA)) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{android.Manifest.permission.CAMERA,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_LOCATION_PERMISSION_REQUEST_CODE);
        }


    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_PERMISSION_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
        else {
            Toast.makeText(ProceedWithFbRegistration.this, "you have denied permissions.", Toast.LENGTH_SHORT).show();
        }

    }
        /*
        *
        *
        * */


    private void findIDs() {
        edt_name=(EditText)findViewById(R.id.edt_name);
        edt_address=(EditText)findViewById(R.id.edt_address);
        edt_postCode=(EditText)findViewById(R.id.edt_postCode);
        edt_City=(EditText)findViewById(R.id.edt_city);
        edt_Phone_number=(EditText)findViewById(R.id.edt_phoneNumber);

        btn_proceed=(Button) findViewById(R.id.btn_proceed);
        tv_toolBar =(TextView)findViewById(R.id.tv_toolBar);
        tv_toolBar.setText(getResources().getString(R.string.additioninfo));
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
    }

    private void registerWithFbFirstTime() {
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        pd=new ProgressDialog(this);
        pd.setMessage("please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                Constants.REGISTRATION_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                try {
                    JSONObject object=new JSONObject(response);
                    String str_Response=object.getString("jwt");
                    Log.e("str_response1stTime",str_Response.toString());
                    Jws<Claims> claims= Jwts.parser()
                            .setSigningKey(Constants.getBase64())
                            .parseClaimsJws(str_Response);
                    Log.e("Claim>>>",claims.toString());
                    Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                    JSONObject jsonObject_status=new JSONObject(claims.getBody());
                     str_Status=jsonObject_status.getString("status");
                    Log.e("status_fb_reg",str_Status.toString().trim());
                    if (str_Status.equals("1")){
                            Log.e("edtPhone",str_phoneNumber);


                        Intent intent=new Intent(ProceedWithFbRegistration.this,HomeActivity.class);
                        startActivity(intent);

                    }else{
                        Toast.makeText(ProceedWithFbRegistration.this, jsonObject_status.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                AppController.mySharedPreferences.saveUserDetail(strId,strId,str_address,str_city,str_email,str_name,str_password,str_phoneNumber,str_Status,str_postCode,str_picUrl);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected HashMap<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();


                params.put(Constants.KEY_NAME,str_name);//0
                params.put(Constants.KEY_EMAIL,str_id_fb);//1
                params.put(Constants.KEY_PHONE,str_phoneNumber);//2
                params.put(Constants.KEY_POST_CODE,str_postCode);//3
                params.put(Constants.KEY_ADDRESS,str_address);//4
                params.put(Constants.KEY_CITY,str_city);//5
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        requestQueue.add(stringRequest);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
