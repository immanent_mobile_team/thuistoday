package com.app.thuistoday;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Util.ConnectionDetector;
import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import adapter.ShopListAdapter;
import adapter.ShopTypeAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;
import volley.LruBitmapCache;

import static android.view.View.GONE;

/**
 * Created by HOME on 1/13/2017.
 */

public class ShopListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    TextView tv_toolBar,tv_checkMyAccountLogin;
    public static TextView tvCartItems;
    Button btn_choose,btn_signIN,btn_register;
    LinearLayout ll_home, ll_myAccountLogin, ll_needHelp;
    RelativeLayout rl_chooseShopType,rl_cartLayout;
    Dialog dialog;
    ImageView iv_close,ivProfile;
    RecyclerView rv_chooseShopType, rv_navigation,rv_shopList;
    Intent intent;
    ProgressDialog pd;
    ArrayList<HashMap<String,String>> shopTypeList = new ArrayList<>();
    ArrayList<HashMap<String,String>> findShopList = new ArrayList<>();
    HashMap<String, String> hashMap ;
    ShopTypeAdapter adapter;
    ShopListAdapter shopListAdapter;
    String str_response="",str_categoryId="",str_categoryName="",strStatus="",str_imageId="",str_image="",str_selectedCategoryId="",str_cityName="",str_post_code="",
            str_addedOn="",str_address="",str_address_ar="",str_areaId="",str_avrg_deliveryTime="",str_banner="",str_category="",str_category_name="",str_closeTime="",str_contctId="",str_countryId="",str_delivery_fee="",
            str_entrancefee="",str_image_shop="",str_isActive="",str_isOpen="",str_Isverified="",str_latitude="",str_longitude="",str_location="",str_logo="",str_openTime="",
            str_paymentMode="",str_permiTime="",str_per_order_commission="",str_postalCode="",str_reference="",str_restaurantDesciption="",str_restaurantDesciption_ar="",str_restaurantDisplayId="",
            str_restaurantId="",str_restaurantName="",str_restaurantName_ar="",str_delivery_charges="",
            str_deliveryChargesOpeningHour="",str_townNAmedisplay="",str_townNAme="";

    DrawerLayout drawer_layout;
    ConnectionDetector cd;
    Context context;
    int mCartItems=0;



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(R.layout.activity_shop_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();
        intent = getIntent();
        str_selectedCategoryId = intent.getStringExtra(Constants.SelectedCategoryId);
        str_cityName = intent.getStringExtra(Constants.CityName);
        str_post_code = intent.getStringExtra(Constants.PostCode);


        cd = new ConnectionDetector(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),ShopListActivity.this));
        RecyclerView.LayoutManager mLayoutManagershopList = new LinearLayoutManager(getApplicationContext());
        rv_shopList.setLayoutManager(mLayoutManagershopList);
        rv_shopList.setItemAnimator(new DefaultItemAnimator());
        //rv_shopList.setAdapter(new ShopListAdapter(new ArrayList<HashMap<String, String>>()));
        final DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.top_left_icon, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN = (Button) header.findViewById(R.id.btn_signIN);
        btn_register = (Button) header.findViewById(R.id.btn_register);
//        btn_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn=true;
//                intent=new Intent(ShopListActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister=true;
//                intent=new Intent(ShopListActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {

                        intent=new Intent(ShopListActivity.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });

//        Toast.makeText(context, ""+ MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
        if (!MySharedPreferences.str_userId.equals("")){
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.my_account));
            ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(ShopListActivity.this,MyAccount.class);
                    startActivity(i);
                }
            });

            btn_register.setVisibility(GONE);
            btn_signIN.setVisibility(GONE);

            ViewGroup.MarginLayoutParams marginLayoutParams =
                    (ViewGroup.MarginLayoutParams) rv_navigation.getLayoutParams();
            marginLayoutParams.setMargins(0,(int)getResources().getDimension(R.dimen._120sp), 0, 0);
            rv_navigation.setLayoutParams(marginLayoutParams);

        }
        else {
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.login));
            ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(ShopListActivity.this,RegistrationActivity.class);
                    startActivity(i);

                }
            });
            btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn=true;
                    intent=new Intent(ShopListActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister=true;
                    intent=new Intent(ShopListActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    private void findIds() {
        tv_toolBar = (TextView) findViewById(R.id.tv_toolBar);
        tvCartItems = (TextView) findViewById(R.id.tvCartItems);
        tv_checkMyAccountLogin=(TextView)findViewById(R.id.tv_checkMyAccountLogin);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
//        tv_toolBar.setText(getResources().getString(R.string.shopListActivityTitle));
        rl_chooseShopType = (RelativeLayout) findViewById(R.id.rl_chooseShopType);
        ll_myAccountLogin = (LinearLayout) findViewById(R.id.ll_myAccountLogin);
        ll_home=(LinearLayout)findViewById(R.id.ll_home);
        ll_needHelp = (LinearLayout) findViewById(R.id.ll_needHelp);
        rv_shopList=(RecyclerView)findViewById(R.id.rv_shopList);
        rl_cartLayout=(RelativeLayout)findViewById(R.id.rl_cartLayout);
        drawer_layout=(DrawerLayout)findViewById(R.id.drawer_layout);
        ll_myAccountLogin.setOnClickListener(this);
        ll_needHelp.setOnClickListener(this);
        rl_chooseShopType.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        rl_cartLayout.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (cd.isConnectingToInternet()) {
            ShopTypeMethod();
            FindShopMethod();
        }
        else {
            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
        }

        mCartItems=0;
        RealmController rel = new RealmController(ShopListActivity.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");



        lruBitmapCache = new LruBitmapCache();
        Constants.setImage(MySharedPreferences.str_picUrl, ivProfile);
        AppController.getInstance().getRequestQueue().getCache().remove(MySharedPreferences.str_picUrl);

    }
    LruBitmapCache lruBitmapCache;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.ll_myAccountLogin:
//                HomeActivity.isRegister=false;
//                intent = new Intent(this, RegistrationActivity.class);
//                startActivity(intent);
//                break;
            case R.id.ll_needHelp:
                break;
            case R.id.ll_home:
                finish();
                break;
            case R.id.rl_chooseShopType:
                dialog = new Dialog(ShopListActivity.this,R.style.NewDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                lp.dimAmount=0.9f;
                dialog.getWindow().setAttributes(lp);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.choose_shop_type_pop_up);
                iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
                btn_choose=(Button) dialog.findViewById(R.id.btn_choose);
                rv_chooseShopType= (RecyclerView)dialog.findViewById(R.id.rv_chooseShopType);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                rv_chooseShopType.setLayoutManager(mLayoutManager);
                rv_chooseShopType.setItemAnimator(new DefaultItemAnimator());
                rv_chooseShopType.setAdapter(adapter);
                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                btn_choose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!ShopTypeAdapter.arrayListCategoryId.isEmpty()){
                            str_selectedCategoryId = String.valueOf(ShopTypeAdapter.arrayListCategoryId);
                            str_selectedCategoryId = str_selectedCategoryId.substring(1, str_selectedCategoryId.length() - 1);
                            Log.e("selectedCategoryId1>>",str_selectedCategoryId);
                            if (str_selectedCategoryId.split(",").length==shopTypeList.size())
                                str_selectedCategoryId ="";


                        }
                        dialog.dismiss();
                        cd = new ConnectionDetector(context);
                        if (cd.isConnectingToInternet()) {
                            FindShopMethod();
                        }
                        else {
                            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
                        }
                    }
                });
                dialog.show();
                break;
            case R.id.rl_cartLayout:
                intent=new Intent(this,CheckoutActivity.class);
                startActivity(intent);
                break;

        }
    }
    /***
     * SHOP TYPE METHOD
     */
    private void ShopTypeMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ShopListActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        if (pd.isShowing()){
            pd.dismiss();
        }
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.SHOP_TYPE_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                shopTypeList.clear();
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        str_categoryId= jsonObject.getString("category_id");
                                        str_categoryName=jsonObject.getString("category_name");
                                        str_image=jsonObject.getString("image");
                                        str_imageId=jsonObject.getString("image_id");
                                        strStatus = jsonObject.getString("status");
                                        hashMap = new HashMap<String, String>();
                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
                                        hashMap.put(Constants.CATEGORY_NAME,str_categoryName);
                                        hashMap.put(Constants.IMAGE,str_image);
                                        hashMap.put(Constants.IMAGE_ID,str_imageId);
                                        hashMap.put(Constants.STATUS,strStatus);
                                        shopTypeList.add(hashMap);
                                    }
                                    adapter=new ShopTypeAdapter(context,shopTypeList);

                                }

                                catch (Exception e){
                                    e.printStackTrace();
                                    Toast.makeText(context, "shoperr", Toast.LENGTH_SHORT).show();
                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                                }
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }
                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }

    /***
     * FIND SHOP METHOD
     */
    private void FindShopMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ShopListActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.FIND_SHOP_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object11>>",object.toString());
                            String str_status=object.getString("status");
                            String str_town_name=object.getString("town_name");
                                tv_toolBar.setText(str_town_name);


                            if (str_status.equals("1")){
                                findShopList.clear();
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        str_addedOn= jsonObject.getString("added_on");
                                        str_address=jsonObject.getString("address");
                                        str_address_ar=jsonObject.getString("address_ar");
                                        str_areaId=jsonObject.getString("area_id");
                                        str_avrg_deliveryTime = jsonObject.getString("average_delivery_time");
                                        str_banner=jsonObject.getString("banner");
                                        str_category=jsonObject.getString("category");
                                        str_category_name = jsonObject.getString("category_name");
                                        str_closeTime=jsonObject.getString("close_time");
                                        str_contctId=jsonObject.getString("contact_email");
                                        str_countryId= jsonObject.getString("country_id");
                                        str_delivery_fee=jsonObject.getString("delivery_fees");
                                        str_delivery_charges=jsonObject.getString("delivery_charges");
                                        str_entrancefee=jsonObject.getString("entrancefee");
                                        str_image_shop = jsonObject.getString("image");
                                        str_isActive=jsonObject.getString("is_active");
                                        str_isOpen=jsonObject.getString("is_open");
                                        str_Isverified = jsonObject.getString("is_verified");
                                        str_latitude=jsonObject.getString("latitude");
                                        str_location = jsonObject.getString("location_id");
                                        str_logo=jsonObject.getString("logo");
                                        str_longitude=jsonObject.getString("longitude");
                                        str_openTime = jsonObject.getString("open_time");
                                        str_paymentMode=jsonObject.getString("payment_modes");
                                        str_permiTime=jsonObject.getString("permitme");
                                        str_per_order_commission = jsonObject.getString("per_order_commision");
                                        str_postalCode=jsonObject.getString("postal_code");
                                        str_reference = jsonObject.getString("reference");
                                        str_restaurantDesciption=jsonObject.getString("restaurant_description");
                                        str_restaurantDesciption_ar=jsonObject.getString("restaurant_description_ar");
                                        str_restaurantDisplayId = jsonObject.getString("restaurant_display_id");
                                        str_restaurantId=jsonObject.getString("restaurant_id");
                                        str_restaurantName=jsonObject.getString("restaurant_name");
                                        str_restaurantName_ar = jsonObject.getString("restaurant_name_ar");
                                        str_deliveryChargesOpeningHour = jsonObject.getString("delivery_charges_opening_hour");

//                                        tv_toolBar.setText(str_townNAme);

                                        hashMap = new HashMap<>();
                                        hashMap.put(Constants.DATE_ADDED_ON,str_addedOn);
                                        hashMap.put(Constants.ADDRESS,str_address);
                                        hashMap.put(Constants.ADDRESS_AR,str_address_ar);
                                        hashMap.put(Constants.AREA_ID,str_areaId);
                                        hashMap.put(Constants.AVG_DELIVERY_TIME,str_avrg_deliveryTime);
                                        hashMap.put(Constants.BANNER,str_banner);
                                        hashMap.put(Constants.CATEGORY_ID,str_category);
                                        hashMap.put(Constants.CATEGORY_NAME,str_category_name);
                                        hashMap.put(Constants.CLOSE_TIME,str_closeTime);
                                        hashMap.put(Constants.CONTACT_ID,str_contctId);
                                        hashMap.put(Constants.COUNTRY_ID,str_countryId);
                                        hashMap.put(Constants.DELIVERY_FEE,str_delivery_fee);
                                        hashMap.put(Constants.ENTRANCE_FEE,str_entrancefee);
                                        hashMap.put(Constants.SHOP_IMAGE,str_image_shop);
                                        hashMap.put(Constants.IS_ACTIVE,str_isActive);
                                        hashMap.put(Constants.IS_OPEN,str_isOpen);
                                        hashMap.put(Constants.IS_VERIFIED,str_Isverified);
                                        hashMap.put(Constants.LATITUDE,str_latitude);
                                        hashMap.put(Constants.LOCATION,str_location);
                                        hashMap.put(Constants.LOGO,str_logo);
                                        hashMap.put(Constants.LONGITUDE,str_longitude);
                                        hashMap.put(Constants.OPEN_TIME,str_openTime);
                                        hashMap.put(Constants.PAYMENT_MODE,str_paymentMode);
                                        hashMap.put(Constants.PERMI_TIME,str_permiTime);
                                        hashMap.put(Constants.COMMISSION_PER_ODER,str_per_order_commission);
                                        hashMap.put(Constants.POST_CODE,str_postalCode);
                                        hashMap.put(Constants.REFERENCE,str_reference);
                                        hashMap.put(Constants.RESTAURANT_DESCRIPTION,str_restaurantDesciption);
                                        hashMap.put(Constants.RESTAURANT_DESCRIPTION_AR,str_restaurantDesciption_ar);
                                        hashMap.put(Constants.RESTAURANT_DISPLAY_ID,str_restaurantDisplayId);
                                        hashMap.put(Constants.RESTAURANT_ID,str_restaurantId);
                                        hashMap.put(Constants.RESTAURANT_NAME,str_restaurantName);
                                        hashMap.put(Constants.RESTAURANT_NAME_AR,str_restaurantName_ar);
                                        hashMap.put(Constants.KEY_DELIVERY_CHARGE,str_delivery_charges);
                                        hashMap.put(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR,str_deliveryChargesOpeningHour);
                                        hashMap.put(Constants.KEY_TOWN_NAME,str_townNAme);
                                        findShopList.add(hashMap);
//                                        Toast.makeText(context, jsonObject.getString("delivery_charges"), Toast.LENGTH_SHORT).show();
                                    }
                                    Log.e("findShopLIst>>",findShopList.toString());
                                    //rv_shopList.setAdapter(new ShopListAdapter(ShopListActivity.this,findShopList));

                                    shopListAdapter=new ShopListAdapter(ShopListActivity.this,findShopList);
                                    rv_shopList.setAdapter(shopListAdapter);
                                    shopListAdapter.notifyDataSetChanged();
                                }
                                catch (Exception e){
                                    pd.dismiss();
                                    e.printStackTrace();

                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                                }
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }
                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            Toast.makeText(context, "er", Toast.LENGTH_SHORT).show();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.KEY_CITY_NAME,str_cityName);
                params.put(Constants.KEY_POST_CODE,str_post_code);
                params.put(Constants.KEY_CATEGORY,str_selectedCategoryId);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strRequest.setRetryPolicy(policy);

        queue.add(strRequest);
    }

}