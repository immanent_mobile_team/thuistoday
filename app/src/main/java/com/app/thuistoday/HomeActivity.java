package com.app.thuistoday;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import Util.ConnectionDetector;
import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import adapter.ShopListAdapter;
import adapter.ShopTypeAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;
import volley.LruBitmapCache;

import static android.view.View.GONE;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    TextView tv_toolBar,tvCartItems,tv_checkMyAccountLogin;
    EditText edt_cityNameOrZipCode;
    Button btn_choose,btn_signIN,btn_register;
    LinearLayout ll_findShop,ll_myAccountLogin,ll_needHelp,ll_parenthide,ll_hideTHis;
    RelativeLayout rl_chooseShopType,rl_cartLayout;
    Dialog dialog;
    ImageView iv_close,ivProfile;
    RecyclerView rv_chooseShopType,rv_navigation;
    Intent intent;
    public static boolean isFromHome=false;
    public static boolean isDrawerFromHome=false,isSignIn=false,isRegister=false;
    public static DrawerLayout drawer;
    DrawerLayout drawer_layout;
    ProgressDialog pd;
    ConnectionDetector cd;
    ArrayList<HashMap<String,String>> shopTypeList = new ArrayList<>();
    ArrayList<HashMap<String,String>> StreetOrCityNameList = new ArrayList<>();
    ArrayList<String> streetNameList=new ArrayList<>() ;
    HashMap<String, String> hashMap ;
    ShopTypeAdapter adapter;
    String regex = "[0-9]+";
    String str_response="",str_categoryId="",str_categoryName="",strStatus="",str_imageId="",str_image="",str_selectedCategoryId="",str_cityName="",str_post_code="",str_text="",str_plaatsName="",str_straatName="",str_address="",str_townname="";
    Context context;
    AutoCompleteTextView autoComplete_cityNameOrZipCode;
    int mCartItems=0;
    private static final int MY_LOCATION_PERMISSION_REQUEST_CODE = 1;
    ImageView iv_helpIcon;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findIds();

        context=this;
        /**
         * Enabling Search Filter
         * */
        Log.e("MYDP>>",MySharedPreferences.str_picUrl+"");
        autoComplete_cityNameOrZipCode.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text

                //  Search.this.adapter.getFilter().filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                str_text = autoComplete_cityNameOrZipCode.getText().toString().trim();
                if (str_text.length()>2){
                    if (str_text.matches(regex)){
                        str_post_code=str_text;
                    }
                    else {
                        str_cityName=str_text;
                        cd = new ConnectionDetector(context);
                        if (cd.isConnectingToInternet()) {
                            GetStreetOrCityMethod();
                        }
                        else {
                            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
                        }
                    }
                }
            }
        });
        cd = new ConnectionDetector(this);
        if (cd.isConnectingToInternet()) {
            ShopTypeMethod();
        }
        else {
            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),HomeActivity.this));
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN=(Button) header.findViewById(R.id.btn_signIN);
        btn_register=(Button) header.findViewById(R.id.btn_register);
//        btn_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn=true;
//                intent=new Intent(HomeActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister=true;
//                intent=new Intent(HomeActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {
                        HomeActivity.isRegister=true;
                        intent=new Intent(HomeActivity.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission((android.Manifest.permission.CAMERA)) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(
                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{android.Manifest.permission.CAMERA,android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_LOCATION_PERMISSION_REQUEST_CODE);
        }

//        Toast.makeText(context, ""+MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
        if (!MySharedPreferences.str_userId.equals("")){
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.my_account));
            btn_signIN.setVisibility(GONE);
            btn_register.setVisibility(GONE);

            ViewGroup.MarginLayoutParams marginLayoutParams =
                    (ViewGroup.MarginLayoutParams) rv_navigation.getLayoutParams();
            marginLayoutParams.setMargins(0,(int)getResources().getDimension(R.dimen._120sp), 0, 0);
            rv_navigation.setLayoutParams(marginLayoutParams);

            ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getApplicationContext(),MyAccount.class);
                    startActivity(i);

                }
            });
        }else {
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.login));
            btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn=true;
                    intent=new Intent(HomeActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister=true;
                    intent=new Intent(HomeActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getApplicationContext(),RegistrationActivity.class);
                    startActivity(i);

                }
            });
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_PERMISSION_REQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
        else {
            Toast.makeText(HomeActivity.this, "you have denied permissions.", Toast.LENGTH_SHORT).show();
        }

    }
    private void findIds() {
        tv_toolBar =(TextView)findViewById(R.id.tv_toolBar);
        tvCartItems =(TextView)findViewById(R.id.tvCartItems);
        tv_checkMyAccountLogin=(TextView)findViewById(R.id.tv_checkMyAccountLogin);

        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        tv_toolBar.setText(getResources().getString(R.string.welcome));
        rl_chooseShopType=(RelativeLayout)findViewById(R.id.rl_chooseShopType);
        ll_myAccountLogin=(LinearLayout)findViewById(R.id.ll_myAccountLogin);
        ll_findShop=(LinearLayout)findViewById(R.id.ll_findShop);
        ll_needHelp=(LinearLayout)findViewById(R.id.ll_needHelp);
//
        ll_parenthide=(LinearLayout) findViewById(R.id.ll_parenthide);
        ll_hideTHis=(LinearLayout)findViewById(R.id.ll_hideTHis);

//
        rl_cartLayout=(RelativeLayout)findViewById(R.id.rl_cartLayout);
        //edt_cityNameOrZipCode=(EditText)findViewById(R.id.edt_cityNameOrZipCode);
        autoComplete_cityNameOrZipCode=(AutoCompleteTextView)findViewById(R.id.autoComplete_cityNameOrZipCode);
        drawer_layout=(DrawerLayout)findViewById(R.id.drawer_layout);
        ll_findShop.setOnClickListener(this);
        ll_myAccountLogin.setOnClickListener(this);
        ll_needHelp.setOnClickListener(this);
        rl_chooseShopType.setOnClickListener(this);
        rl_cartLayout.setOnClickListener(this);
        drawer_layout.setOnClickListener(this);

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        autoComplete_cityNameOrZipCode.getText().clear();
        str_text="";
        str_selectedCategoryId="";
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // Toast.makeText(this, ""+id, Toast.LENGTH_SHORT).show();
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_findShop:
                str_text = autoComplete_cityNameOrZipCode.getText().toString().trim();
                if (str_text.length()>2) {
                    if (str_text.matches(regex)) {
                        str_post_code = str_text;
                        // Toast.makeText(context, "if", Toast.LENGTH_SHORT).show();
                    } else {
                        str_cityName = str_text;
                    }
                }
                if (!Constants.isStringEmpty(str_text)){
                    //|!Constants.isStringEmpty(str_selectedCategoryId)
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    intent = new Intent(this,ShopListActivity.class);
                    intent.putExtra(Constants.SelectedCategoryId,str_selectedCategoryId);
                    intent.putExtra(Constants.CityName,str_cityName);
                    intent.putExtra(Constants.PostCode,str_post_code);
//                    intent.putExtra(Constants.KEY_TOWN_NAME,str_townname);

                    startActivity(intent);

                }
                else {
                    if (Constants.isStringEmpty(str_text)){
                        AppController.constants.showSnackBar(getResources().getString(R.string.zipcodeOrCityNameEmpty),drawer_layout);
                    }
                }
                break;
//            case R.id.ll_myAccountLogin:
//                isRegister=false;
//                isFromHome=true;
//                intent=new Intent(this,RegistrationActivity.class);
//                startActivity(intent);
//                break;
            case R.id.ll_needHelp:
                isFromHome=true;
                break;
            case R.id.rl_chooseShopType:
                dialog = new Dialog(HomeActivity.this,R.style.NewDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
                lp.dimAmount=0.9f;
                dialog.getWindow().setAttributes(lp);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.choose_shop_type_pop_up);
                iv_close = (ImageView) dialog.findViewById(R.id.iv_close);
                btn_choose=(Button) dialog.findViewById(R.id.btn_choose);
                rv_chooseShopType= (RecyclerView)dialog.findViewById(R.id.rv_chooseShopType);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                rv_chooseShopType.setLayoutManager(mLayoutManager);
                rv_chooseShopType.setItemAnimator(new DefaultItemAnimator());
                rv_chooseShopType.setAdapter(adapter);
                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                btn_choose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!ShopTypeAdapter.arrayListCategoryId.isEmpty()){
                            str_selectedCategoryId= String.valueOf(ShopTypeAdapter.arrayListCategoryId);
                            str_selectedCategoryId = str_selectedCategoryId.substring(1, str_selectedCategoryId.length() - 1);
                            Log.e("selectedCategoryId>>",str_selectedCategoryId);

                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.rl_cartLayout:
                intent=new Intent(this,CheckoutActivity.class);
                startActivity(intent);
                break;
            case R.id.ivProfile:
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {

                        intent=new Intent(this,MyAccount.class);
                        startActivity(intent);
                    }
                }

                break;
        }
    }

    /***
     * SHOP TYPE METHOD
     */
    private void ShopTypeMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(HomeActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        if (pd.isShowing()){
            pd.dismiss();
        }
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.SHOP_TYPE_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>><",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                shopTypeList.clear();
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        str_categoryId= jsonObject.getString("category_id");
                                        str_categoryName=jsonObject.getString("category_name");
                                        str_image=jsonObject.getString("image");
                                        str_imageId=jsonObject.getString("image_id");
                                        strStatus = jsonObject.getString("status");
                                        hashMap = new HashMap<String, String>();
                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
                                        hashMap.put(Constants.CATEGORY_NAME,str_categoryName);
                                        hashMap.put(Constants.IMAGE,str_image);
                                        hashMap.put(Constants.IMAGE_ID,str_imageId);
                                        hashMap.put(Constants.STATUS,strStatus);
                                        shopTypeList.add(hashMap);
                                        Log.e("shoplist_Hashmap",hashMap.toString());
                                    }
                                    adapter=new ShopTypeAdapter(context,shopTypeList);
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                                }
//                                Snackbar snackbar = Snackbar
//                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_LONG)
//                                        .setAction("Ok", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Toast.makeText(HomeActivity.this, "Okay", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//
//                                snackbar.show();

//                                        Log.e("UserName>>",str_userName);
//                                        AppController.mySharedPreferences.saveUserDetail(str_userName);
//                                        editor= MySharedPreferences.sharedPreferences.edit();
//                                        editor.putString(Constants.key_userId,str_userId);
//                                        editor.putString(Constants.key_firstName,str_firstName);
//                                        editor.putString(Constants.key_lastName,str_last_name);
//                                        editor.putString(Constants.key_dob,str_dob);
//                                        editor.putString(Constants.key_email,strEmail);
//                                        editor.putString(Constants.key_userType,str_userType);
//                                        editor.putString(Constants.key_payPalId,str_payPalId);
//                                        editor.putString(Constants.key_profilePic,str_profilePic);
//                                        editor.putString(Constants.key_isVerified,str_isVerified);
//                                        editor.putString(Constants.key_FbProfile,str_picUrl);
//                                        editor.putString(Constants.key_deviceId,str_device_id);
//                                        editor.putString(Constants.key_userName,str_userName);
//                                        editor.commit();
//                                        intent=new Intent(RegistrationActivity.this,Search.class);
//                                        startActivity(intent);
//                                        finish();
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                Log.e("send data shopMethod",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
    /***
     * METHOD TO GET STREET OR CITY NAME
     */
    private void GetStreetOrCityMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(HomeActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        if (pd.isShowing()){
            pd.dismiss();
        }
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.FIND_STREET_CITY_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        pd.dismiss();
                        Log.e("Response>>",response);

                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object626>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                StreetOrCityNameList.clear();
                                Log.e("getStatus",str_status);
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        str_plaatsName= jsonObject.getString("plaatsnaam_utf8_nen");
                                        str_straatName=jsonObject.getString("straatnaam_utf8_nen");
                                        str_address=str_straatName+","+str_plaatsName;
                                        hashMap = new HashMap<String, String>();
//                                        hashMap.put(Constants.PLAATS_NAME,str_plaatsName);
//                                        hashMap.put(Constants.STRAAT_NAME,str_straatName);
                                        hashMap.put(Constants.STREET_ADDRESS,str_address);
                                        StreetOrCityNameList.add(hashMap);
                                            Log.e("HashMapOFTown",hashMap.toString());
                                    }
                                    Log.e("CityName>>",StreetOrCityNameList.toString());
                                    for (int i=0;i<StreetOrCityNameList.size();i++){
                                        String str_streetName;
                                        hashMap=StreetOrCityNameList.get(i);
                                        str_streetName=hashMap.get(Constants.STREET_ADDRESS);
                                        streetNameList.add(str_streetName);
                                    }
                                    ArrayAdapter adapter = new ArrayAdapter(context,android.R.layout.simple_list_item_1,streetNameList);

                                    autoComplete_cityNameOrZipCode.setAdapter(adapter);
                                    autoComplete_cityNameOrZipCode.setThreshold(1);
                                    adapter.notifyDataSetChanged();
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                                }
//                                Snackbar snackbar = Snackbar
//                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_LONG)
//                                        .setAction("Ok", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Toast.makeText(HomeActivity.this, "Okay", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//
//                                snackbar.show();

//                                        Log.e("UserName>>",str_userName);
//                                        AppController.mySharedPreferences.saveUserDetail(str_userName);
//                                        editor= MySharedPreferences.sharedPreferences.edit();
//                                        editor.putString(Constants.key_userId,str_userId);
//                                        editor.putString(Constants.key_firstName,str_firstName);
//                                        editor.putString(Constants.key_lastName,str_last_name);
//                                        editor.putString(Constants.key_dob,str_dob);
//                                        editor.putString(Constants.key_email,strEmail);
//                                        editor.putString(Constants.key_userType,str_userType);
//                                        editor.putString(Constants.key_payPalId,str_payPalId);
//                                        editor.putString(Constants.key_profilePic,str_profilePic);
//                                        editor.putString(Constants.key_isVerified,str_isVerified);
//                                        editor.putString(Constants.key_FbProfile,str_picUrl);
//                                        editor.putString(Constants.key_deviceId,str_device_id);
//                                        editor.putString(Constants.key_userName,str_userName);
//                                        editor.commit();
//                                        intent=new Intent(RegistrationActivity.this,Search.class);
//                                        startActivity(intent);
//                                        finish();
                            }
                            else {
                                View view = getCurrentFocus();
                                if (view != null) {
                                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                }
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            View view = getCurrentFocus();
                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_HIDDEN);
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_CITY_NAME,str_cityName);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);

    }

    @Override
    protected void onResume() {
        autoComplete_cityNameOrZipCode.getText().clear();
        str_text="";
        str_selectedCategoryId="";
        str_cityName ="";
        str_post_code ="";
        super.onResume();
        mCartItems=0;
        RealmController rel = new RealmController(HomeActivity.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");
        lruBitmapCache = new LruBitmapCache();

//        if (MySharedPreferences.str_picUrl!=null) {
//            Constants.setImage(MySharedPreferences.str_picUrl, ivProfile);
//        }else
//            {
//        }
//        Constants.setImage("http://deliveryapp.demodemo.ga/web/uploads/IMG_20170209_1314394.jpg",ivProfile);
    }
    LruBitmapCache lruBitmapCache;

}

