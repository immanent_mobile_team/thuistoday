package com.app.thuistoday;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Admin on 2/16/2017.
 */

public class MyRealm extends RealmObject {
    @PrimaryKey
    private   String strItemName;
    private String strPrice;
    private String strNumberOfItems;
    private String strWeightUnit;
    private String strVat;

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    private String strType;

    public String getStrId() {
        return strId;
    }

    public void setStrId(String strId) {
        this.strId = strId;
    }

    private String strId;

    public String getStrVat() {
        return strVat;
    }

    public void setStrVat(String strVat) {
        this.strVat = strVat;
    }



    public String getStrCategoryId() {
        return strCategoryId;
    }

    public void setStrCategoryId(String strCategoryId) {
        this.strCategoryId = strCategoryId;
    }

    private String strCategoryId;

    public String getStrDescription() {
        return strDescription;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }

    private String strDescription;
    public String getStrItemName() {
        return strItemName;
    }

    public void setStrItemName(String strItemName) {
        this.strItemName = strItemName;
    }

    public String getStrPrice() {
        return strPrice;
    }

    public void setStrPrice(String strPrice) {
        this.strPrice = strPrice;
    }

    public String getStrNumberOfItems() {
        return strNumberOfItems;
    }

    public void setStrNumberOfItems(String strNumberOfItems) {
        this.strNumberOfItems = strNumberOfItems;
    }

    public String getStrWeightUnit() {
        return strWeightUnit;
    }

    public void setStrWeightUnit(String strWeightUnit) {
        this.strWeightUnit = strWeightUnit;
    }



    public MyRealm()
    {}
}
