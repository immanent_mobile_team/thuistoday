package com.app.thuistoday;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Util.ConnectionDetector;
import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;

import static android.view.View.GONE;
import static com.app.thuistoday.R.id.drawer_layout;

/**
 * Created by Admin on 3/22/2017.
 */

public class ChangePassword extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
     private String strOldPassword="",strPassword="",strConfirmPassword="", stringExtra="",str_userid="",str_newPassword=""
             ,str_old_password="",str_response="",str_status="",str_confirmpassword="";
     private EditText edtOldPassword,edtPassword,edtConfirmPassword,edt_newPassword;
     private RelativeLayout rlSave;
     private TextView tvCartItems,tv_toolBar;
     private int mCartItems=0;
     private RelativeLayout rlOldPass;
     private ImageView ivProfile;
     private DrawerLayout drawerLayout,drawer;
     private boolean isDrawerFromHome;
     private Intent intent;
     private RecyclerView  rv_navigation;
     private Toolbar toolbar;
     ProgressDialog pd;
     private Button btn_signIN,btn_register;
    ConnectionDetector cd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();
         stringExtra = getIntent().getStringExtra(Constants.VIA_FORGOT_PASS);
        if (stringExtra!=null&&stringExtra.equals("forgot"))
        {
          edtOldPassword.setVisibility(View.GONE);
            rlOldPass.setVisibility(View.GONE);
        }

    rlSave.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            strOldPassword=edtOldPassword.getText().toString();
            str_newPassword=edt_newPassword.getText().toString();
            str_confirmpassword=edtConfirmPassword.getText().toString();
            if (str_newPassword.equals(str_confirmpassword)){
                cd = new ConnectionDetector(ChangePassword.this);
                if (cd.isConnectingToInternet()) {
                    changePassword();
                }else{
                    AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer);
                }

            }
                else {
                    AppController.constants.showSnackBar(getResources().getString(R.string.passwordMismatch),drawer);

                }

            Log.e("getuserDta",MySharedPreferences.str_userId);


        }
    });
    }

    private void changePassword() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ChangePassword.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest stringRequest=new StringRequest(Request.Method.POST,
                Constants.CHANGE_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                JSONObject jsonObject= null;

                try {
                    jsonObject = new JSONObject(response);
                    Log.e("Response",response);
                    str_response=jsonObject.getString("jwt");
                    Jws<Claims> claims= Jwts.parser().setSigningKey(Constants.getBase64()).parseClaimsJws(str_response);
                    final JSONObject object = new JSONObject(claims.getBody());
                    str_status=object.getString("status");
                    Log.e("change_password",str_status);

                    if (str_status.equals("1"))
                    {
                        Intent n=new Intent(ChangePassword.this,HomeActivity.class);
                        startActivity(n);
                        edtOldPassword.setText("");
                        edt_newPassword.setText("");
                        edtConfirmPassword.setText("");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_USER_ID,MySharedPreferences.str_userId);
                params.put(Constants.KEY_NEW_PASSWORD,str_newPassword);
                params.put(Constants.KEY_OLD_PASSWORD,str_old_password);

                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void findIds() {
        tv_toolBar =(TextView)findViewById(R.id.tv_toolBar);
        rlSave = (RelativeLayout) findViewById(R.id.rl_save);
        rlOldPass = (RelativeLayout) findViewById(R.id.rlOldPass);
        tv_toolBar.setText(getResources().getString(R.string.change_password));
        edtConfirmPassword = (EditText) findViewById(R.id.edt_confirm_password);
        edt_newPassword=(EditText)findViewById(R.id.edt_newPassword);
        edtOldPassword = (EditText) findViewById(R.id.edt_oldPassword);
//        edtPassword = (EditText) findViewById(R.id.edt_password);/
        tvCartItems = (TextView) findViewById(R.id.tvCartItems);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCartItems=0;
        RealmController rel = new RealmController(ChangePassword.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),ChangePassword.this));
        drawer= (DrawerLayout) findViewById(drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(ChangePassword.this);
        View header = navigationView.getHeaderView(0);
        btn_signIN=(Button) header.findViewById(R.id.btn_signIN);
        btn_register=(Button) header.findViewById(R.id.btn_register);
//        btn_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn=true;
//                intent=new Intent(ChangePassword.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister=true;
//                intent=new Intent(ChangePassword.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        if (!MySharedPreferences.str_userId.equals("")){
            btn_signIN.setVisibility(GONE);
            btn_register.setVisibility(GONE);

        }else {
            btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn=true;
                    intent=new Intent(ChangePassword.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister=true;
                    intent=new Intent(ChangePassword.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });

        }
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {
                        HomeActivity.isRegister=true;
                        intent=new Intent(ChangePassword.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public void onClick(View v) {
    }
}
