package com.app.thuistoday;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Util.ConnectionDetector;
import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import adapter.OrderHistoryAdapter;
import adapter.ShopTypeAdapter;
import id.zelory.compressor.Compressor;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;

import static android.view.View.GONE;
import static com.app.thuistoday.R.layout.dialog;

/**
 * Created by Admin on 2/23/2017.
 */

public class MyAccount extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener{
    private TextView tvName,tvPostCode,tvAddress,tvPhoneNumber,tvEmail,tvCity,tvCartItems;
    private ImageView ivEditProfile,ivProfile,ivEditProfilePic,ivProfilePic;
    RecyclerView rv_navigation,rvOrderHistory;
    DrawerLayout drawer;
    boolean isDrawerFromHome;
    Intent intent;
    DrawerLayout drawer_layout;
    RelativeLayout rlCart;
    private ProgressDialog pd;
    ArrayList<HashMap<String,String>> alOrders;
    String strUserId="",strId="",strTransId="",strTotalPayment="",strDate="",strDeliveryOption="",strDeliveryDate=""
            ,strDeliveryTime="",strQuentity="",strDeliveryCharge="",strPaymentMethod="",strServiceCharge="",strCart=""
            ,strStatus="",strShopId="",strRestaurantName="";
    private int mOffset = 0,REQUEST_CAMERA=174,SELECT_FILE = 651,mCartItems=0;
    OrderHistoryAdapter orderHistoryAdapter;
    private File fileAttach;
    private AlertDialog dialog, dialog1;
    private Button btn_signIN,btn_register;
    private SwipeRefreshLayout srl_myAccountRefresh;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager mLayoutManagera = new LinearLayoutManager(getApplicationContext());
        rvOrderHistory.setLayoutManager(mLayoutManagera);
        rvOrderHistory.setItemAnimator(new DefaultItemAnimator());

        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),MyAccount.this));
        drawer= (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(),   R.drawable.top_left_icon,this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(MyAccount.this);
        View header = navigationView.getHeaderView(0);
        btn_signIN=(Button) header.findViewById(R.id.btn_signIN);
        btn_register=(Button) header.findViewById(R.id.btn_register);
//        btn_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn=true;
//                intent=new Intent(MyAccount.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister=true;
//                intent=new Intent(MyAccount.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                    isDrawerFromHome=false;
                } else {
                    isDrawerFromHome=true;
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        alOrders = new ArrayList<>();
         strUserId = MySharedPreferences.str_userId;
        final ConnectionDetector cd = new ConnectionDetector(MyAccount.this);
        if(cd.isConnectingToInternet())
           oderHistory();
        else
            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
    Log.e("MY_USER_ID",MySharedPreferences.str_userId);
    if (!MySharedPreferences.str_userId.equals("")){
        btn_register.setVisibility(GONE);
        btn_signIN.setVisibility(GONE);
        ViewGroup.MarginLayoutParams marginLayoutParams =
                (ViewGroup.MarginLayoutParams) rv_navigation.getLayoutParams();
        marginLayoutParams.setMargins(0,(int)getResources().getDimension(R.dimen._120sp), 0, 0);
        rv_navigation.setLayoutParams(marginLayoutParams);
    }else
    {
        btn_signIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isSignIn=true;
                intent=new Intent(MyAccount.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity.isRegister=true;
                intent=new Intent(MyAccount.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });

    }
        srl_myAccountRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (cd.isConnectingToInternet()) {
                    srl_myAccountRefresh.setRefreshing(true);
                    mOffset=mOffset+25;
                    oderHistory();
                }
            }
        });

        Constants.setImage(MySharedPreferences.str_picUrl,ivProfile);
        Constants.setImage(MySharedPreferences.str_picUrl,ivProfilePic);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvAddress.setText(MySharedPreferences.str_address);
        tvCity.setText(MySharedPreferences.str_city);
        tvPhoneNumber.setText(MySharedPreferences.str_phoneNumber);
        tvEmail.setText(MySharedPreferences.str_email);
        tvPostCode.setText(MySharedPreferences.str_postCode);
        tvName.setText(MySharedPreferences.str_name);


        mCartItems=0;
        RealmController rel = new RealmController(MyAccount.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
            tvCartItems.setText(String.valueOf(myRealms.size()));
        else
            tvCartItems.setText("0");


    }


    private void findIds() {
        drawer_layout=(DrawerLayout)findViewById(R.id.drawer_layout);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        rvOrderHistory = (RecyclerView) findViewById(R.id.rv_oderHistory);
        ivEditProfile = (ImageView) findViewById(R.id.ivEditProfile);
        ivEditProfilePic = (ImageView) findViewById(R.id.ivEditProfilePic);
        ivProfilePic= (ImageView) findViewById(R.id.ivProfilePic);
        drawer_layout.setOnClickListener(this);
        tvCartItems = (TextView)findViewById(R.id.tvCartItems);
        ((TextView)findViewById(R.id.tv_toolBar)).setText(getResources().getString(R.string.profile));
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvPostCode = (TextView) findViewById(R.id.tvPostCode);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
        tvCity = (TextView) findViewById(R.id.tv_city);
        ivEditProfile = (ImageView) findViewById(R.id.ivEditProfile);
        rlCart = (RelativeLayout) findViewById(R.id.rl_cartLayout);
        rlCart.setOnClickListener(this);
        ivEditProfile.setOnClickListener(this);
        ivEditProfilePic.setOnClickListener(this);
        srl_myAccountRefresh=(SwipeRefreshLayout)findViewById(R.id.srl_myAccountRefresh);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        return false;
    }

    @Override
    public void onClick(View v) {
       switch (v.getId())
       {
           case R.id.rl_cartLayout:
               intent = new Intent(MyAccount.this,CheckoutActivity.class);
               startActivity(intent);
               break;
           case R.id.ivEditProfile:
               intent = new Intent(MyAccount.this,EditProfile.class);
               startActivity(intent);
               break;
           case R.id.ivEditProfilePic:
               showDialog();
               break;
       }
    }
    public void oderHistory()
    {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(MyAccount.this);
        pd.setMessage(getResources().getString(R.string.please_wait));
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.ORDER_HISTORY_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        String str_response = "";
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                             str_response = jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                srl_myAccountRefresh.setRefreshing(false);
                                alOrders.clear();
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        strId= jsonObject.getString("id");
                                        strTransId = jsonObject.getString("trans_id");
                                        strUserId =jsonObject.getString("user_id");
                                        strTotalPayment =jsonObject.getString("total_payment");
                                        strDate = jsonObject.getString("date");

                                        strDeliveryOption= jsonObject.getString("delivery_option");
                                        strDeliveryDate=jsonObject.getString("delivery_date");
                                        strDeliveryTime=jsonObject.getString("delivery_time");
                                        strQuentity=jsonObject.getString("quantity");
                                        strDeliveryCharge = jsonObject.getString("delivery_charge");

                                        strPaymentMethod= jsonObject.getString("payment_method");
                                        strServiceCharge=jsonObject.getString("service_charge");
                                        strCart=jsonObject.getString("cart");
                                        strStatus=jsonObject.getString("status");
                                        strShopId = jsonObject.getString("shopid");
                                        strRestaurantName = jsonObject.getString("restaurant_name_ar");


                                       HashMap<String,String >  hashMap = new HashMap<String, String>();
                                         hashMap.put(Constants.KEY_ID,strId);
                                        hashMap.put(Constants.KEY_TRANS_ID,strTransId);
                                        hashMap.put(Constants.KEY_USER_ID,strUserId);
                                        hashMap.put(Constants.KEY_TOTAL_PAYMENT,strTotalPayment);
                                        hashMap.put(Constants.STATUS,strStatus);
                                        hashMap.put(Constants.KEY_DATE,strDate);
                                        hashMap.put(Constants.KEY_DELIVERY_OPTION,strDeliveryOption);
                                        hashMap.put(Constants.KEY_DELIVERY_DATE,strDeliveryDate);
                                        hashMap.put(Constants.KEY_DELIVERY_DATE,strDeliveryTime);
                                        hashMap.put(Constants.KEY_QUANTITY,strQuentity);
                                        hashMap.put(Constants.KEY_DELIVERY_CHARGE,strDeliveryCharge);
                                        hashMap.put(Constants.KEY_PAYMENT_METHOD,strPaymentMethod);
                                        hashMap.put(Constants.KEY_SERVICE_CHARGE,strServiceCharge);
                                        hashMap.put(Constants.KEY_CART,strCart);
                                        hashMap.put(Constants.KEY_SHOP_ID,strShopId);
                                        hashMap.put(Constants.KEY_RESTAURANT_NAME_AR,strRestaurantName);

                                        alOrders.add(hashMap);
                                    }
                               orderHistoryAdapter = new OrderHistoryAdapter(MyAccount.this,alOrders);
                           rvOrderHistory.setAdapter(orderHistoryAdapter);
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);

                                }
//                                Snackbar snackbar = Snackbar
//                                        .make(drawer_layout, object.getString("message"), Snackbar.LENGTH_LONG)
//                                        .setAction("Ok", new View.OnClickListener() {
//                                            @Override
//                                            public void onClick(View view) {
//                                                Toast.makeText(HomeActivity.this, "Okay", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//
//                                snackbar.show();

//                                        Log.e("UserName>>",str_userName);
//                                        AppController.mySharedPreferences.saveUserDetail(str_userName);
//                                        editor= MySharedPreferences.sharedPreferences.edit();
//                                        editor.putString(Constants.key_userId,str_userId);
//                                        editor.putString(Constants.key_firstName,str_firstName);
//                                        editor.putString(Constants.key_lastName,str_last_name);
//                                        editor.putString(Constants.key_dob,str_dob);
//                                        editor.putString(Constants.key_email,strEmail);
//                                        editor.putString(Constants.key_userType,str_userType);
//                                        editor.putString(Constants.key_payPalId,str_payPalId);
//                                        editor.putString(Constants.key_profilePic,str_profilePic);
//                                        editor.putString(Constants.key_isVerified,str_isVerified);
//                                        editor.putString(Constants.key_FbProfile,str_picUrl);
//                                        editor.putString(Constants.key_deviceId,str_device_id);
//                                        editor.putString(Constants.key_userName,str_userName);
//                                        editor.commit();
//                                        intent=new Intent(RegistrationActivity.this,Search.class);
//                                        startActivity(intent);
//                                        finish();
                            }
                            else {
                                srl_myAccountRefresh.setRefreshing(false);
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }

                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
            {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_OFFSET,mOffset+"");
                params.put(Constants.KEY_USER_ID,strUserId);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }

    public void showDialog()
    {
        android.support.v7.app.AlertDialog.Builder builder= new android.support.v7.app.AlertDialog.Builder(MyAccount.this,R.style.NewDialog);
        LayoutInflater inflater= (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.dialog_gallery,null,false) ;
        builder.setView(view);
        Button btn_gallery= (Button) view.findViewById(R.id.btnOpenGallery);
        Button btn_camera= (Button) view.findViewById(R.id.btnOpenCamera);
        Button btnCancel= (Button) view.findViewById(R.id.btnCancel);
        btn_gallery.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);//
                startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
            }
        });
        btn_camera.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                startActivityForResult(intent, REQUEST_CAMERA);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            strUserId = MySharedPreferences.str_userId;
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            AlertDialog.Builder builder = new AlertDialog.Builder(MyAccount.this);
            builder.setCancelable(false);
            builder.setMessage("Do you want to save this image.");
            builder.setTitle("Alert!");
            builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog1.dismiss();
                    new AsynuploadProfile().execute();
                }
            });
            builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                  dialog1.dismiss();
                }
            });

             dialog1 = builder.create();
            dialog1.show();


        }
    }
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm=null;
        if (data != null) {
            try {
                Uri selectedImage = data.getData();
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
//                String[] filePath = {MediaStore.Images.Media.DATA};
//                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
//                c.moveToFirst();
//                int columnIndex = c.getColumnIndex(filePath[0]);
//                String picturePath = c.getString(columnIndex);
//                c.close();
//                Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
                if(bitmap!=null)
                ivProfilePic.setImageBitmap(bitmap);
                fileAttach = createFilefromBitmap(bitmap,MyAccount.this);
//                Log.e("file>>>",fileAttach.toString());
//                String[] parts= picturePath.split("/");
                //str_attachmentName= parts[parts.length - 1];
//                System.out.println(str_attachmentName);
//                Log.e("ImageName>>>",str_attachmentName);
//                if (str_attachmentName!=null&&!str_attachmentName.equals("")){
//                    edt_imageName.setText(str_attachmentName);
//                }

//                Uri uri=data.getData();
//                Log.e("Gallery>>>>>>>",uri.getPath().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * method to get the image form the camera...
     * @param data
     */
    public void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        fileAttach = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        Log.e("filePath>>",fileAttach.getPath());
//  File file = new File(destination.getPath());
        FileOutputStream fo;
        try {
            fileAttach.createNewFile();
            fo = new FileOutputStream(fileAttach);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivProfilePic.setImageBitmap(thumbnail);
    }
    private class AsynuploadProfile extends AsyncTask<Void, Void, String> {
        String resultRes = "";
        ProgressDialog pd;
        String response = "";

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            pd = new ProgressDialog(MyAccount.this);
            pd.setMessage(Constants.PLEASE_WAIT);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO Auto-generated method stub

            // Client-side HTTP transport library
            HttpClient httpClient = new DefaultHttpClient();
            FileBody fb_file = null;
            // using POST method
            HttpPost httpPostRequest = new HttpPost(Constants.URL_UPLOAD_IMAGE);
            try {
                // creating a file body consisting of the file that we want to
                // send to the server
                if (fileAttach != null) {
                    Log.e("file<<>>", fileAttach.toString());
                    if (fileAttach.exists()) {

                        fb_file = new FileBody(fileAttach);
                    } else
                        Log.e("notExist", "sd");
                }
                else
                    Log.e("file null","file null");
                /**
                 * An HTTP entity is the majority of an HTTP request or
                 * response, consisting of some of the headers and the body, if
                 * present. It seems to be the entire request or response
                 * without the request or status line (although only certain
                 * header fields are considered part of the entity).
                 *
                 * */


                MultipartEntityBuilder multiPartEntityBuilder = MultipartEntityBuilder.create();
                if (fb_file != null) {
                    multiPartEntityBuilder.addPart(Constants.KEY_PROFILE_PIC, fb_file);

                    Log.e("Exist", "exist");
                }
                multiPartEntityBuilder.addPart(Constants.KEY_USER_ID, new StringBody(strUserId));


                httpPostRequest.setEntity(multiPartEntityBuilder.build());

                Log.e("file after", fb_file.toString());
                // Execute POST request to the given URL
                HttpResponse httpResponse = null;

                httpResponse = httpClient.execute(httpPostRequest);
                Log.e("user id", strUserId);
                // receive response as inputStream
                InputStream inputStream = null;
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    resultRes = convertInputStreamToString(inputStream);
                else
                    resultRes = "Did not work!";
                return resultRes;
            } catch (Exception e) {
                resultRes = "catch";
                Log.e("catch","cat");
                e.printStackTrace();
                return null;
            }
            // return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (pd.isShowing())
                pd.dismiss();

//            AppController.constants.hideProgress();
//            Log.d("response>>>>>", resultRes);
//            Log.d("response>>>>>", resultRes.toString());
            JSONObject jsonObject = null;
            try {
                Log.e("try","try");
                jsonObject = new JSONObject(resultRes);
                String str_response = jsonObject.getString("jwt");
                Log.e("str_response>>>", str_response.trim());
                Jws<Claims> claims = Jwts.parser()
                        .setSigningKey(Constants.getBase64())
                        .parseClaimsJws(str_response);
                Log.e("Claim>>>", claims.toString());
                Log.e("JWS Claims", "Header: " + claims.getHeader() + "\nBody: " + claims.getBody() + "\nSignature: " + claims.getSignature());
                JSONObject object1 = new JSONObject(claims.getBody());
                Log.e("response<>", object1.toString());
                String str_status = object1.getString("status");
                JSONObject object = null;
                if (str_status.equals("1")) {

                    if(fileAttach.exists())fileAttach.delete();
                    Toast.makeText(MyAccount.this, object1.getString("message"), Toast.LENGTH_SHORT).show();
                    try {
                        jsonObject = object1.getJSONObject("data");
                        String strId=jsonObject.getString("id");
                        String str_address=jsonObject.getString("address");
                        String str_city=jsonObject.getString("city");
                        String str_email=jsonObject.getString("email");
                        String str_name=jsonObject.getString("name");
                        String str_password=jsonObject.getString("password");
                        String str_phoneNumber=jsonObject.getString("phone");
                        String strStatus=jsonObject.getString("status");
                        String str_postCode=jsonObject.getString("zipcode");
                        String str_picUrl=jsonObject.getString("profile_pic");

                        AppController.mySharedPreferences.saveUserDetail(strId,strId,str_address,str_city,str_email,str_name,str_password,str_phoneNumber,strStatus,str_postCode,str_picUrl);
                        Constants.setImage(MySharedPreferences.str_picUrl,ivProfile);
                        Constants.setImage(MySharedPreferences.str_picUrl,ivProfilePic);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                if (str_status.equals("0")) {
                    AppController.constants.showSnackBar("Please try again", drawer_layout);
                }

            } catch (Exception e) {
                Toast.makeText(MyAccount.this, "Please try again", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }
    private String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    /**
     * method to create file form bitmap..
     * @param bmp
     * @return
     */
    private File createFilefromBitmap(Bitmap bmp, Context context){

        try {
            //create a file to write bitmap data
            File f = new File(context.getCacheDir(), System.currentTimeMillis() + ".jpg");
            if(f.exists())
                f.delete();

            f.createNewFile();

//Convert bitmap to byte array
            Bitmap bitmap = bmp;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
            return Compressor.getDefault(context).compressToFile(f);
//   return f;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
