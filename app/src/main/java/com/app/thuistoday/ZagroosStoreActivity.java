package com.app.thuistoday;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Util.ConnectionDetector;
import Util.Constants;
import Util.MySharedPreferences;
import adapter.NavigationAdapter;
import adapter.ProductCategoriesAdapter;
import adapter.ZagroosStoreAdapter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.realm.RealmResults;
import volley.AppController;

import static android.view.View.GONE;

/**
 * Created by HOME on 1/14/2017.
 */

public class ZagroosStoreActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener  {
    TextView tv_toolBar,tv_okay,tvCartItems,tv_checkMyAccountLogin,tv_zagroosAddress;
    Button btn_signIN,btn_register;
    LinearLayout ll_home, ll_myAccountLogin, ll_needHelp;
    RelativeLayout rl_productCategories,rl_layoutProductCategories,rl_cartLayout;
    Dialog dialog;
    ImageView iv_close,ivProfile;
    RecyclerView rv_productCategories, rv_navigation,rv_zagroosStoreList;
    String str_response="",str_shopId="",str_categoryId="",str_categoryName="",str_categoryName_ar="",str_crust_on="",str_restaurantId="",str_sauceOn="",strIsEnabled="",str_selectedCategoryId="",
            str_customPizza="",str_mealDescription="",str_mealDescription_ar="",str_mealId="",strIsActive="",str_mealImage="",str_mealName="",str_mealName_ar="",str_mealPrice="",str_pizzaPrice="",str_productUnit="",
    str_type="",str_vat="",str_vatName="";
    Intent intent;
    ArrayList<String> ProductCategoryList=new ArrayList<String>();
    String str_productCategories,str_defaultValue="PRODUCT CATEGORIES";
    int selectedProductPos,mCartItems=0;
    boolean isFirstTime;
    ProgressDialog pd;
    ArrayList<HashMap<String,String>> productCategoryList = new ArrayList<>();
    ArrayList<HashMap<String,String>> ProductList = new ArrayList<>();
    HashMap<String, String> hashMap ;
    DrawerLayout drawer_layout;
    ZagroosStoreAdapter adapter;
    ProductCategoriesAdapter productCategoriesAdapter;
    ConnectionDetector cd;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zagroos_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        findIds();


//
// str_categoryId = intent.getStringExtra(Constants.CATEGORY_ID);
//      str_categoryId="";
        cd = new ConnectionDetector(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_navigation.setLayoutManager(mLayoutManager);
        rv_navigation.setItemAnimator(new DefaultItemAnimator());
        rv_navigation.setAdapter(new NavigationAdapter(new ArrayList<HashMap<String, String>>(),ZagroosStoreActivity.this));
        RecyclerView.LayoutManager mLayoutManagerZagrooStoreList = new LinearLayoutManager(getApplicationContext());
        rv_zagroosStoreList.setLayoutManager(mLayoutManagerZagrooStoreList);
        rv_zagroosStoreList.setItemAnimator(new DefaultItemAnimator());

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.top_left_icon, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        btn_signIN = (Button) header.findViewById(R.id.btn_signIN);
        btn_register = (Button) header.findViewById(R.id.btn_register);
//        btn_signIN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isSignIn=true;
//                intent=new Intent(ZagroosStoreActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
//        btn_register.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                HomeActivity.isRegister=true;
//                intent=new Intent(ZagroosStoreActivity.this,RegistrationActivity.class);
//                startActivity(intent);
//            }
//        });
        ivProfile = (ImageView) header.findViewById(R.id.ivProfile);
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getApplication().getSharedPreferences(Constants.MY_PREFS,MODE_PRIVATE);
                if (sp!=null)
                {
                    if (!sp.getString(Constants.KEY_NAME,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                    {

                        intent=new Intent(ZagroosStoreActivity.this,MyAccount.class);
                        startActivity(intent);
                    }
                }
            }
        });
//        Toast.makeText(this, ""+ MySharedPreferences.str_userId, Toast.LENGTH_SHORT).show();
        if (!MySharedPreferences.str_userId.equals("")){
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.my_account));
            ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getApplicationContext(),MyAccount.class);
                    startActivity(i);
                }
            });
            btn_register.setVisibility(GONE);
            btn_signIN.setVisibility(GONE);
            ViewGroup.MarginLayoutParams marginLayoutParams =
                    (ViewGroup.MarginLayoutParams) rv_navigation.getLayoutParams();
            marginLayoutParams.setMargins(0,(int)getResources().getDimension(R.dimen._120sp), 0, 0);
            rv_navigation.setLayoutParams(marginLayoutParams);
        }
        else
            {
            tv_checkMyAccountLogin.setText(getResources().getString(R.string.login));
            ll_myAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getApplicationContext(),RegistrationActivity.class);
                    startActivity(i);
                }
            });
            btn_signIN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isSignIn=true;
                    intent=new Intent(ZagroosStoreActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
            btn_register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeActivity.isRegister=true;
                    intent=new Intent(ZagroosStoreActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        intent=getIntent();
        Bundle bundle = this.getIntent().getExtras();
        HashMap<String,String> hashMap1 = (HashMap<String, String>) bundle.getSerializable(Constants.KEY_HASH_MAP);
        str_shopId = intent.getStringExtra(Constants.RESTAURANT_ID);
        if (cd.isConnectingToInternet()) {
            productCategoryMethod();
            getProductMethod();
        }
        else {
            AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
        }
        mCartItems=0;
        RealmController rel = new RealmController(ZagroosStoreActivity.this.getApplication());
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
        {
            for (MyRealm realm:myRealms)
            {
                mCartItems = mCartItems+1;
            }
            tvCartItems.setText(String.valueOf(mCartItems));
            rv_zagroosStoreList.setAdapter(new ZagroosStoreAdapter(ZagroosStoreActivity.this, new ArrayList<HashMap<String, String>>(),tvCartItems, str_shopId));
        }
        else
            tvCartItems.setText(String.valueOf(mCartItems));
    }

    private void findIds() {
        tv_toolBar = (TextView) findViewById(R.id.tv_toolBar);
        tvCartItems = (TextView) findViewById(R.id.tvCartItems);
        rv_navigation = (RecyclerView) findViewById(R.id.rv_navigation);
        tv_checkMyAccountLogin=(TextView)findViewById(R.id.tv_checkMyAccountLogin);
        /*
        *
        * */
        tv_zagroosAddress=(TextView)findViewById(R.id.tv_zagroosAddress);
        String str_getAddress=getIntent().getStringExtra("address_ar");
        tv_zagroosAddress.setText(str_getAddress);
        String str_get=getIntent().getStringExtra("restaurant_name");
//       tv_toolBar.setText(getResources().getString(R.string.ZagroosStoreActivityTitle));
        tv_toolBar.setText(str_get);
        /*
        *
        * */
        rl_productCategories = (RelativeLayout) findViewById(R.id.rl_productCategories);
        ll_myAccountLogin = (LinearLayout) findViewById(R.id.ll_myAccountLogin);
        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        ll_needHelp = (LinearLayout) findViewById(R.id.ll_needHelp);
        rl_layoutProductCategories=(RelativeLayout)findViewById(R.id.rl_layoutProductCategories);
        rv_zagroosStoreList = (RecyclerView) findViewById(R.id.rv_zagroosStoreList);
        rl_cartLayout=(RelativeLayout)findViewById(R.id.rl_cartLayout);
        drawer_layout=(DrawerLayout)findViewById(R.id.drawer_layout);
        ll_myAccountLogin.setOnClickListener(this);
        ll_needHelp.setOnClickListener(this);
        ll_home.setOnClickListener(this);
        rl_productCategories.setOnClickListener(this);
        rl_layoutProductCategories.setVisibility(GONE);
        rl_cartLayout.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.ll_myAccountLogin:
//                HomeActivity.isRegister=false;
//                intent = new Intent(this, RegistrationActivity.class);
//                startActivity(intent);
//                break;
            case R.id.ll_needHelp:
                break;
            case R.id.ll_home:
                intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.rl_productCategories:
                rl_layoutProductCategories.setVisibility(View.VISIBLE);
                tv_okay = (TextView)findViewById(R.id.tv_okay);

//                cd = new ConnectionDetector(this);
//                if (cd.isConnectingToInternet()) {
//                    productCategoryMethod();
//                }
//                else {
//                    AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection),drawer_layout);
//                }
                rv_productCategories = (RecyclerView)findViewById(R.id.rv_productCategories);
                rv_productCategories.setAdapter(productCategoriesAdapter);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                rv_productCategories.setLayoutManager(mLayoutManager);
                rv_productCategories.setItemAnimator(new DefaultItemAnimator());
//                rv_productCategories.setAdapter(new ProductCategoriesAdapter(new ArrayList<HashMap<String, String>>()));
                tv_okay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try{
                        if (!ProductCategoriesAdapter.selectedProductArrayList.isEmpty()) {
                            str_selectedCategoryId = String.valueOf(ProductCategoriesAdapter.selectedProductArrayList);
                            str_selectedCategoryId = str_selectedCategoryId.substring(1, str_selectedCategoryId.length() - 1);
                            Log.e("selectedCategoryId>>", str_selectedCategoryId);
                            str_categoryId = str_selectedCategoryId;
                            cd = new ConnectionDetector(ZagroosStoreActivity.this);
                            if (cd.isConnectingToInternet()) {
                                getProductMethod();
                            } else {
                                AppController.constants.showSnackBar(getResources().getString(R.string.alert_internetConnection), drawer_layout);
                            }
                        }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        rl_layoutProductCategories.setVisibility(GONE);
                    }
                });
               break;
            case R.id.rl_cartLayout:
                intent=new Intent(this,CheckoutActivity.class);
                startActivity(intent);
                break;
        }
    }
    /***
     * METHOD FOR FETCH PRODUCT CATEGORY
     */
    private void productCategoryMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ZagroosStoreActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        if (pd.isShowing()){
            pd.dismiss();
        }
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.FETCH_PRODUCT_CATEGORY_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        if (productCategoryList.size()>0)
                            productCategoryList.clear();
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                productCategoryList.clear();
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        str_categoryId= jsonObject.getString("category_id");
                                        str_categoryName=jsonObject.getString("category_name");
                                        str_categoryName_ar=jsonObject.getString("category_name_ar");
                                        str_crust_on=jsonObject.getString("crust_on");
                                        strIsEnabled= jsonObject.getString("is_enabled");
                                        str_restaurantId=jsonObject.getString("restaurant_id");
                                        str_sauceOn= jsonObject.getString("souce_on");
                                        hashMap = new HashMap<String, String>();
                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
                                        hashMap.put(Constants.CATEGORY_NAME,str_categoryName);
                                        hashMap.put(Constants.CATEGORY_NAME_AR,str_categoryName_ar);
                                        hashMap.put(Constants.CRUST_ON,str_crust_on);
                                        hashMap.put(Constants.IS_ENABLED,strIsEnabled);
                                        hashMap.put(Constants.RESTAURANT_ID,str_restaurantId);
                                        hashMap.put(Constants.SAUCE_ON,str_sauceOn);
                                        productCategoryList.add(hashMap);
                                    }
                                    productCategoriesAdapter=new ProductCategoriesAdapter(ZagroosStoreActivity.this,productCategoryList);
                                    //rv_productCategories.setAdapter(new ProductCategoriesAdapter(ZagroosStoreActivity.this,productCategoryList));
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                                }
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }
                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_SHOP_ID,str_shopId);
                Log.e("send data>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
    /***
     * METHOD FOR GET PRODUCT
     */
    private void getProductMethod() {
        RequestQueue queue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(ZagroosStoreActivity.this);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
        StringRequest strRequest = new StringRequest(com.android.volley.Request.Method.POST, Constants.FETCH_PRODUCT_URL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response)
                    {
                        if (ProductList.size()>0)
                            ProductList.clear();
                        pd.dismiss();
                        Log.e("Response>>",response);
                        try {
                            JSONObject jsonObject=new JSONObject(response);
                            str_response=jsonObject.getString("jwt");
                            Log.e("str_response>>>",str_response.trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Jws<Claims> claims = Jwts.parser()
                                    .setSigningKey(Constants.getBase64())
                                    .parseClaimsJws(str_response);
                            Log.e("Claim>>>",claims.toString());
                            Log.e("JWS Claims","Header: "+claims.getHeader()+"\nBody: " + claims.getBody()+"\nSignature: "+claims.getSignature());
                            final JSONObject object = new JSONObject(claims.getBody());
                            Log.e("Object>>",object.toString());
                            String str_status=object.getString("status");
                            if (str_status.equals("1")){
                                ProductList.clear();
                                try {
                                    JSONArray jsonArray=object.getJSONArray("data");
                                    for (int i=0;i<jsonArray.length();i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        str_categoryId = jsonObject.getString("category_id");
                                        str_crust_on=jsonObject.getString("crust_on");
                                        str_customPizza=jsonObject.getString("custompizza");
                                        strIsActive=jsonObject.getString("is_active");
                                        str_mealDescription= jsonObject.getString("meal_description");
                                        str_mealDescription_ar=jsonObject.getString("meal_description_ar");
                                        str_mealId= jsonObject.getString("meal_id");
                                        str_mealImage= jsonObject.getString("meal_image");
                                        str_mealName= jsonObject.getString("meal_name");
                                        str_mealName_ar= jsonObject.getString("meal_name_ar");
                                        str_mealPrice= jsonObject.getString("meal_price");
                                        str_pizzaPrice= jsonObject.getString("pizza_prices");
                                        str_productUnit= jsonObject.getString("product_unit");
                                        str_restaurantId= jsonObject.getString("restaurant_id");
                                        str_sauceOn= jsonObject.getString("souce_on");
                                        str_type= jsonObject.getString("type");
                                        str_vat= jsonObject.getString("vat");
                                        str_vatName= jsonObject.getString("vat_name");
                                        hashMap = new HashMap<String, String>();
                                        hashMap.put(Constants.CATEGORY_ID,str_categoryId);
                                        hashMap.put(Constants.CRUST_ON,str_crust_on);
                                        hashMap.put(Constants.CUSTOM_PIZZA,str_customPizza);
                                        hashMap.put(Constants.IS_ACTIVE,strIsActive);
                                        hashMap.put(Constants.MEAL_DESCRIPTION,str_mealDescription);
                                        hashMap.put(Constants.MEAL_DESCRIPTION_AR,str_mealDescription_ar);
                                        hashMap.put(Constants.MEAL_ID,str_mealId);
                                        hashMap.put(Constants.MEAL_IMAGE,str_mealImage);
                                        hashMap.put(Constants.MEAL_NAME,str_mealName);
                                        hashMap.put(Constants.MEAL_NAME_AR,str_mealName_ar);
                                        hashMap.put(Constants.MEAL_PRICE,str_mealPrice);
                                        hashMap.put(Constants.PIZZA_PRICE,str_pizzaPrice);
                                        hashMap.put(Constants.PRODUCT_UNIT,str_productUnit);
                                        hashMap.put(Constants.RESTAURANT_ID,str_restaurantId);
                                        hashMap.put(Constants.SAUCE_ON,str_sauceOn);
                                        hashMap.put(Constants.TYPE,str_type);
                                        hashMap.put(Constants.VAT,str_vat);
                                        hashMap.put(Constants.VAT_NAME,str_vatName);
                                        ProductList.add(hashMap);
                                    }
                                    Log.e("product_list",ProductList+"" );
                                    adapter=new ZagroosStoreAdapter(ZagroosStoreActivity.this,ProductList, tvCartItems,str_shopId);
                                    rv_zagroosStoreList.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    //rv_productCategories.setAdapter(new ProductCategoriesAdapter(ZagroosStoreActivity.this,productCategoryList));
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                                }
                            }
                            else {
                                AppController.constants.showSnackBar(object.getString("message"),drawer_layout);
                            }
                        } catch (Exception e){
                            Log.e("ERROR","<<>>"+e);
                            pd.dismiss();
                            e.printStackTrace();
                            AppController.constants.showSnackBar(getResources().getString(R.string.serverErrorMessage),drawer_layout);
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Log.e("responseError>>",error.toString());
                        pd.dismiss();
                        AppController.constants.showSnackBar(error.toString(),drawer_layout);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.KEY_SHOP_ID,str_shopId);
                params.put(Constants.KEY_CATEGORY_ID,str_categoryId);
                Log.e("senddata>>",params.toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
}