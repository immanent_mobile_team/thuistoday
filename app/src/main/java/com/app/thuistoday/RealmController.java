package com.app.thuistoday;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Admin on 2/16/2017.
 */

public class RealmController  {
    private static RealmController instance;
    private Realm myRealm;

    public RealmController(Application application) {
        myRealm = Realm.getDefaultInstance();
    }
    public RealmController()
    {
       super();
    }
    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }
    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return myRealm;
    }

    //Refresh the realm istance
    public void refresh() {

        myRealm.refresh();
    }

    //clear all objects from Book.class
    public void clearAll() {

        myRealm.beginTransaction();
        myRealm.clear(MyRealm.class);
        myRealm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<MyRealm> getAllData() {

        return myRealm.where(MyRealm.class).findAll();
    }

    //query a single item with the given id
    public MyRealm getBook(String id) {

        return myRealm.where(MyRealm.class).equalTo("id", id).findFirst();
    }

    //check if Book.class is empty
    public boolean hasBooks() {

        return !myRealm.allObjects(MyRealm.class).isEmpty();
    }

    //query example
    public RealmResults<MyRealm> queryedBooks() {

        return myRealm.where(MyRealm.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();
    }
}
