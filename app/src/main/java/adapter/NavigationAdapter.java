package adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.thuistoday.HomeActivity;
import com.app.thuistoday.R;
import com.app.thuistoday.RegistrationActivity;
import com.app.thuistoday.SettingProfile;

import java.util.HashMap;
import java.util.List;

import Util.MySharedPreferences;
import volley.AppController;

/**
 * Created by HOME on 11/19/2016.
 */

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.MyViewHolder>  {
    private List<HashMap<String,String>> shopType;
    private Context context;
    int itemCount=0;
    Resources resources;
    /*
    * ={"HOME","HELP","ABOUT","SETTING"}
    * */
    String[] name;
    Integer[] imageId = {
            R.drawable.home_icon,
            R.drawable.help_icon,
            R.drawable.about_icon,
            R.drawable.setting_icon
                        };

    Intent intent;
    View itemView;



    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_draweText;
        ImageView iv_drawerIcon;
        public String str_checkBox;
        public boolean IsCheckBoxSelected=true;
        public MyViewHolder(View view) {
            super(view);
            tv_draweText=(TextView)view.findViewById(R.id.tv_draweText);
            iv_drawerIcon=(ImageView) view.findViewById(R.id.iv_drawerIcon);

        }
    }
    public NavigationAdapter(List<HashMap<String,String>> shopType,Context context) {
        this.shopType= shopType;
        this.context = context;
    }
    @Override
    public NavigationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        name = new String[] {
                context.getString(R.string.home),
                context.getString(R.string.help),
                context.getString(R.string.about),
                context.getString(R.string.setting)
                };

        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_navigation, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NavigationAdapter.MyViewHolder holder, final int position) {

//        if ((MySharedPreferences.str_name.equals("")&&position==4)||MySharedPreferences.str_name.equals("")&&position==1)
//        {
//            holder.tv_draweText.setVisibility(View.VISIBLE);
//            holder.iv_drawerIcon.setVisibility(View.VISIBLE);
//
//        }
        holder.tv_draweText.setText(name[position]);
        //holder.iv_checkBox.setText(date[position]);
         holder.iv_drawerIcon.setImageResource(imageId[position]);
         holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("LOG>>", String.valueOf(HomeActivity.isFromHome));
                if (holder.getAdapterPosition()==0) {
                    if (HomeActivity.isDrawerFromHome==true){
                        HomeActivity.drawer.closeDrawer(GravityCompat.START);
                        HomeActivity.isDrawerFromHome=false;
                    }else {
                        intent = new Intent(context, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                   // Toast.makeText(context, "Home" , Toast.LENGTH_SHORT).show();
                }
                else if (holder.getAdapterPosition()==1) {
                    Toast.makeText(context, "help" , Toast.LENGTH_SHORT).show();
                }
                else if (holder.getAdapterPosition()==2) {
                    Toast.makeText(context, "about" , Toast.LENGTH_SHORT).show();
                }
                else if (holder.getAdapterPosition()==3) {
                    if (HomeActivity.isDrawerFromHome==true)
                    HomeActivity.drawer.closeDrawer(Gravity.START);
                    HomeActivity.isDrawerFromHome=false;
                    Intent intent = new Intent(context, SettingProfile.class);
                    context.startActivity(intent);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return imageId.length;
    }

}

