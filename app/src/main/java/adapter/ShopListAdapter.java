package adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.thuistoday.MyRealm;
import com.app.thuistoday.R;
import com.app.thuistoday.RealmController;
import com.app.thuistoday.ShopListActivity;
import com.app.thuistoday.ZagroosStoreActivity;

import java.util.HashMap;
import java.util.List;

import Util.Constants;
import io.realm.RealmResults;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by HOME on 1/14/2017.
 */

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.MyViewHolder>  {
    private List<HashMap<String,String>> shopList;
    private Context context;
    HashMap<String,String> hashMap =new HashMap<String, String>();
    String[] name = new String[] {"Vegetable","Meat and meat products","Universal shops", "Cheese and diary"};
    Integer[] imageId = {
            R.drawable.pizza_icon,
            R.drawable.bread_icon,
            R.drawable.veg_icon,
            R.drawable.pizza_icon};
    View itemView;
    Intent intent;
    private ShopListActivity shopListActivity;
    private  AlertDialog dialog ;
    private int mCartItems=0;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_restaurant_name,tv_restaurant_description;
        ImageView iv_shopListIcon,iv_topLogo;
        public MyViewHolder(View view) {
            super(view);
              tv_restaurant_name=(TextView)view.findViewById(R.id.tv_restaurant_name);
              iv_shopListIcon=(ImageView) view.findViewById(R.id.iv_shopListIcon);
              iv_topLogo=(ImageView)view.findViewById(R.id.iv_topLogo);
              tv_restaurant_description=(TextView)view.findViewById(R.id.tv_restaurant_description);
              context=view.getContext();
        }
    }
    public ShopListAdapter(ShopListActivity shopListActivity, List<HashMap<String, String>> shopList) {
        this.shopList= shopList;
        this.shopListActivity = shopListActivity;
    }
    @Override
    public ShopListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_shop_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ShopListAdapter.MyViewHolder holder, final int position) {
        hashMap = shopList.get(position);
        holder.tv_restaurant_name.setText(hashMap.get(Constants.RESTAURANT_NAME_AR));
        holder.tv_restaurant_description.setText(hashMap.get(Constants.RESTAURANT_DESCRIPTION_AR));
        if (!hashMap.get(Constants.LOGO).equals("")){
            String str=hashMap.get(Constants.LOGO);
            Constants.setImage(Constants.IMAGE_BASE_URL+str,holder.iv_topLogo);
        }
        else {
            holder.iv_shopListIcon.setImageResource(R.drawable.no_image);
        }
        if (!hashMap.get(Constants.SHOP_IMAGE).equals("")){
            String str=hashMap.get(Constants.SHOP_IMAGE);
            Constants.setImage(Constants.IMAGE_BASE_URL+str,holder.iv_shopListIcon);
        }
        else {
            holder.iv_shopListIcon.setImageResource(R.drawable.tomato_icon);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREF_PRODUCT_DETAIL,MODE_PRIVATE);
                if (sharedPreferences!=null&& !sharedPreferences.getString(Constants.KEY_SHOP_ID,Constants.NO_VALUE).equals(Constants.NO_VALUE))
                {

                    if (!sharedPreferences.getString(Constants.KEY_SHOP_ID,Constants.NO_VALUE).equals(shopList.get(position).get(Constants.RESTAURANT_ID)))
                    {
                        RealmController rel = new RealmController(shopListActivity.getApplication());
                        RealmResults<MyRealm> myRealms = rel.getAllData();
                        if (myRealms.size()!=0)
                        {
                            showDialog(position, sharedPreferences);
                        }
                        else
                        {
                            SharedPreferences.Editor edt= sharedPreferences.edit();
                            edt.putString(Constants.KEY_SHOP_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                            edt.putString(Constants.KEY_DELIVERY_CHARGE,shopList.get(position).get(Constants.KEY_DELIVERY_CHARGE));
                            edt.putString(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR,shopList.get(position).get(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR));
                            edt.commit();
                            intent=new Intent(context, ZagroosStoreActivity.class);
                            intent.putExtra(Constants.RESTAURANT_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                            intent.putExtra(Constants.CATEGORY_ID,shopList.get(position).get(Constants.CATEGORY_ID));
                            HashMap<String,String> hashMap = shopList.get(position);
                            Bundle bundle = new Bundle();
                            Log.e("GetSHopLIST",hashMap.toString());
                            bundle.putSerializable(Constants.KEY_HASH_MAP,hashMap);
                            bundle.putString("restaurant_name",hashMap.get("restaurant_name"));
                            bundle.putString("address_ar",hashMap.get("address_ar"));
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                    }
                    else
                    {
                        SharedPreferences.Editor edt= sharedPreferences.edit();
                        edt.putString(Constants.KEY_SHOP_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                        edt.putString(Constants.KEY_DELIVERY_CHARGE,shopList.get(position).get(Constants.KEY_DELIVERY_CHARGE));
                        edt.putString(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR,shopList.get(position).get(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR));
                        edt.commit();
                        intent=new Intent(context, ZagroosStoreActivity.class);
                        intent.putExtra(Constants.RESTAURANT_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                        intent.putExtra(Constants.CATEGORY_ID,shopList.get(position).get(Constants.CATEGORY_ID));
                        HashMap<String,String> hashMap = shopList.get(position);
                        Bundle bundle = new Bundle();
                        Log.e("asda",hashMap.toString());
                        Log.e("sadad",hashMap.get("restaurant_name"));
                        bundle.putSerializable(Constants.KEY_HASH_MAP,hashMap);
                        bundle.putString("restaurant_name",hashMap.get("restaurant_name"));
                        bundle.putString("address_ar",hashMap.get("address_ar"));
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                }
                else
                {
                    SharedPreferences.Editor edt= sharedPreferences.edit();
                    edt.putString(Constants.KEY_SHOP_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                    edt.putString(Constants.KEY_DELIVERY_CHARGE,shopList.get(position).get(Constants.KEY_DELIVERY_CHARGE));
                    edt.putString(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR,shopList.get(position).get(Constants.KEY_DELIVERY_CHARGE_OPENING_HOUR));
                    edt.commit();
                    intent=new Intent(context, ZagroosStoreActivity.class);
                    intent.putExtra(Constants.RESTAURANT_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                    intent.putExtra(Constants.CATEGORY_ID,shopList.get(position).get(Constants.CATEGORY_ID));
                    HashMap<String,String> hashMap = shopList.get(position);
                    Bundle bundle = new Bundle();

                    bundle.putSerializable(Constants.KEY_HASH_MAP,hashMap);

                    bundle.putString("restaurant_name",hashMap.get("restaurant_name"));
                    bundle.putString("address_ar",hashMap.get("address_ar"));
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }

            }
        });
    }

    private void showDialog(final int position, SharedPreferences sharedPreferences) {
        AlertDialog.Builder builder =new AlertDialog.Builder(context);
        builder.setMessage("By changing the shops, your cart will empty. Are sure want to change the shops?");
        builder.setCancelable(false);
        builder.setTitle(context.getResources().getString(R.string.alert));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                RealmController rela = new RealmController(shopListActivity.getApplication());
                rela.clearAll();

                    intent=new Intent(context, ZagroosStoreActivity.class);
                    intent.putExtra(Constants.RESTAURANT_ID,shopList.get(position).get(Constants.RESTAURANT_ID));
                    intent.putExtra(Constants.CATEGORY_ID,shopList.get(position).get(Constants.CATEGORY_ID));
                    HashMap<String,String> hashMap = shopList.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.KEY_HASH_MAP,hashMap);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    ShopListActivity.tvCartItems.setText(String.valueOf(mCartItems));
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
        });
         dialog = builder.create();
        dialog.show();
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }
}

