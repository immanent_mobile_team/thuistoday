package adapter;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInstaller;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.service.textservice.SpellCheckerService;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.thuistoday.ChangePassword;
import com.app.thuistoday.HomeActivity;
import com.app.thuistoday.R;
import com.app.thuistoday.RegistrationActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Util.Constants;
import Util.MySharedPreferences;
import volley.AppController;

import static Util.MySharedPreferences.str_city;
import static Util.MySharedPreferences.str_english;
import static Util.MySharedPreferences.str_status;
import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;

public class Setting_profileAdapter extends RecyclerView.Adapter<Setting_profileAdapter.ViewHolder>
{

    String[] str_SettingNames;
    int  [] for_lt_icons;
    String[] str_english;
    int[] for_toggle_icons;
    String[] str_dutch;
    int[] for_rt_icons;
    boolean isSelect=false;
    static Context context;
    View view;
    RecyclerView.ViewHolder viewHolder1;
    public static final String TAG="MY_ADAPTER";
    String str_MysharedPref_dutch="",str_MysharedPref_english="";


    public Setting_profileAdapter(Context context, int[] for_lt_icon, String[] str_setting_textNames, String[] str_english, int[] for_toggle_icons, String[] str_dutch, int[] for_rt_icons) {
        this.context=context;
        this.for_lt_icons=for_lt_icon;
        this.str_SettingNames=str_setting_textNames;
        this.str_english=str_english;
        this.for_toggle_icons=for_toggle_icons;
        this.str_dutch=str_dutch;
        this.for_rt_icons=for_rt_icons;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_iconName,tv_english,tv_dutch;
        ImageView iv_ltIcon,ivtoggleIcon,iv_rtICon;
        LinearLayout ll_layout;
        public ViewHolder(View v){

            super(v);
            tv_iconName=(TextView)v.findViewById(R.id.tv_changepwd);
            ll_layout=(LinearLayout)v.findViewById(R.id.llSetting);
            iv_ltIcon =(ImageView)v.findViewById(R.id.iv_settingScreen);
            tv_english=(TextView)v.findViewById(R.id.tv_lang_english);
            ivtoggleIcon=(ImageView)v.findViewById(R.id.iv_toggle);
            tv_dutch=(TextView)v.findViewById(R.id.tv_lang_dutch);
            iv_rtICon=(ImageView)v.findViewById(R.id.iv_arrow);

        }
    }

    @Override
    public Setting_profileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view= LayoutInflater.from(context).inflate(R.layout.setting_profile_list,parent,false);
        viewHolder1=new ViewHolder(view);
        return (ViewHolder) viewHolder1;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (position%2==0){
            holder.ll_layout.setBackgroundResource(R.drawable.gray_bg_setting);
        }else {
        }
        holder.tv_iconName.setText(str_SettingNames[position]);
        holder.iv_ltIcon.setImageResource(for_lt_icons[position]);
        holder.tv_english.setText(str_english[position]);
        holder.ivtoggleIcon.setImageResource(for_toggle_icons[position]);
        holder.tv_dutch.setText(str_dutch[position]);

        holder.iv_rtICon.setImageResource(for_rt_icons[position]);

        if (str_SettingNames[position].equals("language")
                || str_SettingNames[position].equals("taal")) {

            holder.iv_rtICon.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isSelect){
                        holder.ivtoggleIcon.setImageResource(R.drawable.push_btn_green1);
                        setLocale("nl");
                        isSelect=true;
                        AppController.mySharedPreferences
                                .saveToggleState(str_MysharedPref_english,Constants.KEY_DUTCH);
                    }
                }
            });
//            Toast.makeText(context, ""+MySharedPreferences.str_dutch, Toast.LENGTH_SHORT).show();
            if (MySharedPreferences.str_dutch.equals("dutch")){
                holder.ivtoggleIcon.setImageResource(R.drawable.push_btn_green1);
//                setLocale("en");
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.ivtoggleIcon.setImageResource(R.drawable.push_btn_green1);
                        setLocale("en");
                        AppController.mySharedPreferences.ClearDataToggleState(context);
                    }
                });
            }
        }
        else {
                if (str_SettingNames[position].equals("Change Password")
                    || str_SettingNames[position].equals("Verander wachtwoord")) {
                    holder.tv_english.setVisibility(GONE);
                    holder.ivtoggleIcon.setVisibility(GONE);
                    holder.tv_dutch.setVisibility(GONE);
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i=new Intent(context, ChangePassword.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(i);
                        }
                    });
            }
            if (str_SettingNames[position].equals("language")||
                    str_SettingNames[position].equals("taal")){
                holder.iv_rtICon.setVisibility(View.GONE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isSelect){
                            holder.ivtoggleIcon.setImageResource(R.drawable.push_btn_green1);
                            setLocale("nl");
                            isSelect=true;

                        }else {
                            holder.ivtoggleIcon.setImageResource(R.drawable.push_btn_green);
                            isSelect=false;
                            setLocale("en");
                        }
                    }

                });
            }
            if (str_SettingNames[position].equals("Log Out")||
                    str_SettingNames[position].equals("Loguit")){
                holder.tv_english.setVisibility(GONE);
                holder.ivtoggleIcon.setVisibility(GONE);
                holder.tv_dutch.setVisibility(GONE);
                holder.iv_rtICon.setVisibility(View.VISIBLE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppController.mySharedPreferences.clearAllData(context);
                        Intent i=new Intent(context,HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        Constants.disconnectFromFacebook();
//                        AppController.getInstance().getRequestQueue().getCache().remove(MySharedPreferences.str_picUrl);
                    }
                });
            }
        }

    }

    @Override
    public int getItemCount() {
        return str_SettingNames.length;

    }

    public static void setLocale(String lang)
    {

        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(context, HomeActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(refresh);
    }
}
