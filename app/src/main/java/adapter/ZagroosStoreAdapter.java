package adapter;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.thuistoday.R;
import com.app.thuistoday.ZagroosStoreActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Util.Constants;
import com.app.thuistoday.MyRealm;
import io.realm.Realm;
import io.realm.RealmResults;

import com.app.thuistoday.RealmController;

import static com.app.thuistoday.R.string.no;
import static com.app.thuistoday.R.string.number;
import static com.app.thuistoday.R.string.price;

/**
 * Created by HOME on 1/16/2017.
 */
public class ZagroosStoreAdapter extends RecyclerView.Adapter<adapter.ZagroosStoreAdapter.MyViewHolder>  {
    private List<HashMap<String,String>> storeList;
    private Context context;
    private Realm realm;
    String[] name = new String[] {"Vegetable","Meat and meat products","Universal shops", "Cheese and diary"};
    View itemView;
    String str_product_unit="",str_mealPrice="";
    HashMap<String,String> hashMap =new HashMap<String, String>();
    private Application application;
    private ArrayList<MyRealm> alMyRealm;
    private TextView tvCartItems;
    private String strShopId="";
    private double alreadyQuantity = 0;
    private float alreadyItemPrice =0;
    String price;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tvHeading,tvMealDescription,tv_price,tv_pcs;
        ImageView iv_productIcon,ivAddItem,iv_zagrooIcon;
        LinearLayout ll_layout;
        EditText edt_quantity;
        public MyViewHolder(View view) {
            super(view);
            tvHeading =(TextView)view.findViewById(R.id.tvHeading);
            tvMealDescription =(TextView)view.findViewById(R.id.tvMealDescription);
            tv_pcs=(TextView)view.findViewById(R.id.tv_pcs);
            iv_productIcon=(ImageView) view.findViewById(R.id.iv_productIcon);
            ivAddItem =(ImageView) view.findViewById(R.id.iv_addItem);
            ll_layout=(LinearLayout)view.findViewById(R.id.ll_layout);
            edt_quantity=(EditText)view.findViewById(R.id.edt_quantity);
            tv_price=(TextView)view.findViewById(R.id.tv_price);
            iv_zagrooIcon=(ImageView)view.findViewById(R.id.iv_zagrooIcon);

            context = view.getContext();

            realm = RealmController.with(application).getRealm();
            alMyRealm = new ArrayList<>();
        }
    }
    public ZagroosStoreAdapter(ZagroosStoreActivity zagroosStoreActivity, List<HashMap<String, String>> storeList, TextView tvCartItems, String str_shopId) {
        this.storeList= storeList;
        application = zagroosStoreActivity.getApplication();
        this.tvCartItems = tvCartItems;
        this.strShopId = str_shopId;
    }
    @Override
    public adapter.ZagroosStoreAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_zagroos_store, parent, false);
        return new adapter.ZagroosStoreAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final adapter.ZagroosStoreAdapter.MyViewHolder holder, final int position) {
        hashMap = storeList.get(position);
        str_mealPrice="";
//        addItem(holder,position);
        changPrice(position,holder);
        Log.e("<<HashMap>>",hashMap.toString());
        holder.tvHeading.setText(hashMap.get(Constants.MEAL_NAME_AR));
        holder.tvMealDescription.setText(hashMap.get(Constants.MEAL_DESCRIPTION_AR));



        str_product_unit=hashMap.get(Constants.PRODUCT_UNIT);
        if (str_product_unit.equals("pieces")){
            holder.tv_pcs.setText(R.string.pcs);
            holder.edt_quantity.setInputType(InputType.TYPE_CLASS_NUMBER);
            holder.edt_quantity.setKeyListener(DigitsKeyListener.getInstance("1234567890"));
        }else {
            holder.tv_pcs.setText(R.string.kg);
            holder.edt_quantity.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            holder.edt_quantity.setKeyListener(DigitsKeyListener.getInstance(true,true));
        }
        str_mealPrice = hashMap.get(Constants.MEAL_PRICE);
        str_mealPrice = str_mealPrice.replace("\\,", "\\.");

        if (hashMap.get(Constants.TYPE).equals("Incl."))
        {
            str_mealPrice=str_mealPrice.replace("\\.","\\,");
            Log.e("NEW_MEAL",str_mealPrice);
            holder.tv_price.setText(str_mealPrice);
        }
        else
        {
            String vat="", afterVatAdd="";
            vat = hashMap.get(Constants.VAT_NAME);
            /*
            *
            * */
            String vat_price = (str_mealPrice.contains(",")) ? str_mealPrice.replaceAll(",","\\.") : str_mealPrice;
            afterVatAdd = String.valueOf(((Double.parseDouble(vat_price)
                    * Double.parseDouble(vat))/100)+Double.parseDouble(vat_price));
            Log.e("Vat price ","<<>>"+afterVatAdd);
            String roundedvalue = String.valueOf(roundOff(Float.parseFloat(afterVatAdd),2));
            Log.e("Rounded value","<<>>"+roundedvalue);
             price = (roundedvalue.contains("."))
                    ? roundedvalue.replace(".",",")
                    : roundedvalue;
            Log.e("After replace","<<>>"+price);
            holder.tv_price.setText(price);
            Log.e(vat+" vat>> "+str_mealPrice+" " +hashMap.get(Constants.MEAL_NAME_AR),afterVatAdd);
            /*
            *
            *
            * */
        }


        if (!hashMap.get(Constants.MEAL_IMAGE).equals("")){
            String str_imageName=hashMap.get(Constants.MEAL_IMAGE);
            Constants.setImage(Constants.IMAGE_BASE_URL+str_imageName,holder.iv_productIcon);
        }
        else {
            holder.iv_productIcon.setImageResource(R.drawable.tomato_icon);
        }
        if (position%2==0){
            holder.ll_layout.setBackgroundResource(R.drawable.gray_bg);
        }
//            SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.PREF_PRODUCT_DETAIL,MODE_PRIVATE);
//            if (sharedPreferences!=null&& !sharedPreferences.getString(Constants.KEY_SHOP_ID,Constants.NO_VALUE).equals(Constants.NO_VALUE))
//            {
//                if (sharedPreferences.getString(Constants.KEY_SHOP_ID,Constants.NO_VALUE).equals(strShopId))
//                {
//
//                }
//            }

//            RealmController rel = new RealmController(application);
//            RealmResults<MyRealm> myRealms = rel.getAllData();
//            if (myRealms.size()!=0) {
//                for (MyRealm myRealm : myRealms) {
//                    if (hashMap.get(Constants.MEAL_NAME_AR).equals(myRealm.getStrItemName())) {
//                        holder.edt_quantity.setText(myRealm.getStrNumberOfItems());
//                    }
//                }
//            }

        holder.edt_quantity.setSelection(holder.edt_quantity.getText().length());

        holder.ivAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem(holder,position);
            }
        });

    }
    static float roundOff(float x, int position)
    {
        float a = x;
        double temp = Math.pow(10.00, position);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }


    private void changPrice(final int position, final MyViewHolder holder) {
        final HashMap<String ,String > hashMap = storeList.get(position);
        holder.edt_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            public void afterTextChanged(Editable s) {
                String str_amount = null;
                float value = 0;
//                    str_amount=price;
//                holder.tv_price.setText(str_amount);


                if (holder.edt_quantity.getText().toString().trim().length() > 0) {
                    double quantityNumber = Double.parseDouble(holder.edt_quantity.getText().toString());

//                            double price=quantityNumber*Double.parseDouble(str_mealPrice);
//                            str_amount=String.format("%.2f",str_amount);
//                            Log.e("floatValue>>>",String.format("%.2f",price));
//                            Log.e("Amount>>>",str_amount);

                    Log.e("textChanged",(hashMap.get(Constants.MEAL_PRICE)));
                            str_amount = String.valueOf(quantityNumber * Double.parseDouble(hashMap.get(Constants.MEAL_PRICE).replace(",", ".")));
//                    str_amount=price;
//                    Log.e("textChangedTwo",price);
                } else {
                    str_amount = "00";
                }
//                str_amount = str_amount.replace(".", ",");
//                str_amount= String.valueOf(roundOff(Float.parseFloat(str_amount), 2));
//                Log.e("price>>", str_amount);
//                str_amount=str_amount.replace(".",",");
//                Log.e("str_amount",str_amount);
//                holder.tv_price.setText(str_amount);
                    Log.e("print_Amount",str_amount);
                holder.tv_price.setText(String.valueOf(roundOff(Float.parseFloat(str_amount), 2)).replace(".", ","));
                Log.e("display_PRiNT",(String.valueOf(roundOff(Float.parseFloat(str_amount), 2)).replace(".", ",")));


            }
        });
    }

    double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch(Exception e) {
                return -1;
            }
        }
        else return 0;
    }
    private void addItem(final MyViewHolder holder, final int position) {


                Double c=ParseDouble(String.valueOf(holder.edt_quantity.getText()));
                String str_formatedValue= String.format("%.2f", c);
                str_product_unit=storeList.get(position).get(Constants.PRODUCT_UNIT);
                if (str_product_unit.equals("pieces")){
                        String str_ifpcs=holder.edt_quantity.getText().toString();
                    holder.edt_quantity.setText(str_ifpcs.split("\\.")[0]);

                }else {
//                    holder.tv_pcs.setText(R.string.kg);
//                    holder.edt_quantity.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//                    holder.edt_quantity.setKeyListener(DigitsKeyListener.getInstance(true,true));
                    holder.edt_quantity.setText(str_formatedValue);
                }
                Log.e("onClick:230",str_formatedValue);
                if (holder.edt_quantity.getText().toString().trim().length()==0 || Double.parseDouble(holder.edt_quantity.getText().toString())==0)
                {
                    Toast.makeText(context, "Minimum quantity should be one!", Toast.LENGTH_SHORT).show();
                    return;
                }
                int mCartItems=0;
                boolean blAlready = false;
                hashMap = storeList.get(position);
//
                RealmController rel = new RealmController(application);
//                Realm realm = rel.getRealm();
                RealmResults<MyRealm> myRealms = rel.getAllData();
                if (myRealms.size()!=0)
                {
                    for (MyRealm myRealm: myRealms) {
                        mCartItems = mCartItems+1;
                        if (hashMap.get(Constants.MEAL_NAME_AR).equals(myRealm.getStrItemName()))
                        {
                            try{
//                                Log.e("Inside on click","<<>>"+myRealm.getStrNumberOfItems());
                                alreadyQuantity = Double.parseDouble(myRealm.getStrNumberOfItems());
//                            alreadyQuantity=Double.parseDouble(myRealm.getStrNumberOfItems());
                                Log.e("Inside on click","<<>>"+myRealm.getStrPrice());
                                alreadyItemPrice = Float.parseFloat(myRealm.getStrPrice().replaceAll("\\,","."));
                                blAlready =true;
                                break;
                            }catch (Exception e){
                                Log.e("Inside catch","<<>>"+myRealm.getStrNumberOfItems());

                                alreadyQuantity = Double.parseDouble(myRealm.getStrNumberOfItems());
//                            alreadyQuantity=Double.parseDouble(myRealm.getStrNumberOfItems());

                                alreadyItemPrice = Float.parseFloat(myRealm.getStrPrice());
                                blAlready =true;
                                break;
                            }

                        }
                    }
                    if (blAlready)
                    {

                        MyRealm myrealm=  new MyRealm();
                        HashMap<String ,String > hashMap1 = storeList.get(position);
                        Log.e("Inside saving","<<>>"+hashMap1);
                        myrealm.setStrItemName(holder.tvHeading.getText().toString());
                        /*
                        *
                        * */
                        Log.e("Inside Calculations","<<>>"+holder.edt_quantity.getText().toString());
                        String no_of_items = (holder.edt_quantity.getText().toString().contains("."))
                                ? holder.edt_quantity.getText().toString().split("\\.")[0]
                                : holder.edt_quantity.getText().toString();
                        Log.d("After conversion","<<>>"+no_of_items);
                        /*
                        *
                        * */
                        myrealm.setStrNumberOfItems(String.valueOf(alreadyQuantity + Integer.parseInt(no_of_items)));

                        myrealm.setStrPrice(String.valueOf(roundOff(alreadyItemPrice + Float.parseFloat(holder.tv_price.getText().toString().replaceAll("\\,",".")),2)));
                        myrealm.setStrWeightUnit(holder.tv_pcs.getText().toString());
                        myrealm.setStrDescription(holder.tvMealDescription.getText().toString());
                        myrealm.setStrVat(hashMap1.get(Constants.VAT));
                        myrealm.setStrId(hashMap1.get(Constants.MEAL_ID));
                        myrealm.setStrType(hashMap1.get(Constants.TYPE));
//                      myrealm.setStrCategoryId(hashMap.get(Constants.CATEGORY_ID));

                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(myrealm);
                        realm.commitTransaction();
                    }
                    else
                    {
                        MyRealm myrealm=  new MyRealm();
                        HashMap<String ,String > hashMap1 = storeList.get(position);
                        Log.e("Inside saving","<<>>"+hashMap1);
                        myrealm.setStrItemName(holder.tvHeading.getText().toString());
                        myrealm.setStrNumberOfItems(holder.edt_quantity.getText().toString());
                        myrealm.setStrPrice(holder.tv_price.getText().toString());
                        myrealm.setStrWeightUnit(holder.tv_pcs.getText().toString());
                        myrealm.setStrDescription(holder.tvMealDescription.getText().toString());
//                      myrealm.setStrCategoryId(hashMap.get(Constants.CATEGORY_ID));
                        myrealm.setStrVat(hashMap1.get(Constants.VAT));
                        myrealm.setStrId(hashMap1.get(Constants.MEAL_ID));
                        myrealm.setStrType(hashMap1.get(Constants.PRODUCT_UNIT));

                        realm.beginTransaction();
                        realm.copyToRealm(myrealm);
                        realm.commitTransaction();
                    }
                }
                else
                {
                    MyRealm myrealm=  new MyRealm();
                    HashMap<String ,String > hashMap1 = storeList.get(position);
                    myrealm.setStrItemName(holder.tvHeading.getText().toString());
                    myrealm.setStrNumberOfItems(holder.edt_quantity.getText().toString());
                    myrealm.setStrPrice(holder.tv_price.getText().toString());
                    myrealm.setStrWeightUnit(holder.tv_pcs.getText().toString());
                    myrealm.setStrDescription(holder.tvMealDescription.getText().toString());
                    myrealm.setStrVat(hashMap1.get(Constants.VAT));
                    myrealm.setStrId(hashMap1.get(Constants.MEAL_ID));
                    myrealm.setStrType(hashMap1.get(Constants.PRODUCT_UNIT));


                    realm.beginTransaction();
                    realm.copyToRealm(myrealm);
                    realm.commitTransaction();
                }


                RealmController rela = new RealmController(application);
                RealmResults<MyRealm> myRealmsa = rela.getAllData();
                if (myRealmsa.size()!=0)
                {
                    mCartItems = 0;
                    for (MyRealm a : myRealmsa)
                    {
                        mCartItems = mCartItems+1;
                    }
                    tvCartItems.setText(String.valueOf(mCartItems));
                }
                Toast.makeText(context, context.getResources().getString(R.string.item_added_successfully), Toast.LENGTH_SHORT).show();

    }

    @Override
    public int getItemCount() {
        return storeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
