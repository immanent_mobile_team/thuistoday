package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.thuistoday.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Util.Constants;

/**
 * Created by HOME on 11/19/2016.
 */

public class ShopTypeAdapter extends RecyclerView.Adapter<ShopTypeAdapter.MyViewHolder>  {
    private List<HashMap<String,String>> shopType;
    private Context context;
    String[] name = new String[] {"Vegetable","Meat and meat products","Universal shops", "Cheese and diary"};
    private boolean IsCheckBoxSelected=true;
    public static ArrayList<String> arrayListCategoryId ;
    HashMap<String,String> hashMap =new HashMap<String, String>();
    View itemView;
    String str_categoryId="";
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_name;
        ImageView iv_checkBox;
        public String str_checkBox;
        public boolean IsCheckBoxSelected=true;
        public MyViewHolder(View view) {
            super(view);
            tv_name=(TextView)view.findViewById(R.id.tv_name);
            iv_checkBox=(ImageView) view.findViewById(R.id.iv_checkBox);
            context=view.getContext();
            arrayListCategoryId = new ArrayList<>();
        }
    }
    public ShopTypeAdapter(Context listener, List<HashMap<String, String>> shopType) {
        this.shopType= (ArrayList<HashMap<String, String>>) shopType;
    }
    @Override
    public ShopTypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_choose_shop_type, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ShopTypeAdapter.MyViewHolder holder, final int position) {
        hashMap = shopType.get(position);
        holder.tv_name.setText(hashMap.get(Constants.CATEGORY_NAME));
       //str_categoryId=hashMap.get(position).contains()
        //holder.iv_checkBox.setText(date[position]);
       // holder.iv_checkBox.setImageResource(imageId[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                  if (arrayList.contains(String.valueOf(""+position)))
//                  {
//                      holder.iv_checkBox.setImageResource(R.drawable.check_box);
//                      arrayList.remove(position+"");
//                  }
//                 else
//                  {
//                      holder.iv_checkBox.setImageResource(R.drawable.check_box_selected);
//                      arrayList.add(String.valueOf(position));
//                      Log.e("str_category>>","");
//                  }
                if (arrayListCategoryId.contains(shopType.get(position).get(Constants.CATEGORY_ID)))
                  {
                      holder.iv_checkBox.setImageResource(R.drawable.check_box);
                      arrayListCategoryId.remove(shopType.get(position).get(Constants.CATEGORY_ID));
                  }
                 else
                  {
                      holder.iv_checkBox.setImageResource(R.drawable.check_box_selected);
                      arrayListCategoryId.add(shopType.get(position).get(Constants.CATEGORY_ID));
                  }
                Log.e("str_category>>",arrayListCategoryId.toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return shopType.size();
    }

}

