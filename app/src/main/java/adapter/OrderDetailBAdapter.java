package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.thuistoday.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import Util.Constants;

/**
 * Created by Admin on 3/6/2017.
 */

public class OrderDetailBAdapter  extends RecyclerView.Adapter<adapter.OrderDetailBAdapter.MyViewHolder>  {
    private List<JSONObject> orderDetailList;
    private Context context;
    View itemView;
    Intent intent;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout ll_layout;
        TextView tv_price,tv_product_name,tv_quantity;
        public MyViewHolder(View view) {
            super(view);
            ll_layout=(LinearLayout)view.findViewById(R.id.ll_layout);
            tv_product_name=(TextView) view.findViewById(R.id.tv_product_name);
            tv_price=(TextView)view.findViewById(R.id.tv_price);
            tv_quantity=(TextView)view.findViewById(R.id.tv_quantity);
            context=view.getContext();
        }
    }
    public OrderDetailBAdapter(List<JSONObject> orderDetailList) {
        this.orderDetailList= orderDetailList;
    }
    @Override
    public adapter.OrderDetailBAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_order_detail_b, parent, false);
        return new adapter.OrderDetailBAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final adapter.OrderDetailBAdapter.MyViewHolder holder, final int position) {
        if (position%2==0){
            holder.ll_layout.setBackgroundResource(R.drawable.gray_bg);
        }else {
        }
        Log.e("OrderListDeta",orderDetailList.toString());
        JSONObject jsonObject = orderDetailList.get(position);
        try {
            holder.tv_price.setText(jsonObject.getString("subtotal"));
//            holder.tv_product_name.setText(jsonObject.getString("itemName"));
            holder.tv_product_name.setText(jsonObject.getString("name"));
            holder.tv_quantity.setText(jsonObject.getString("itemQuantity"));
        }
        catch (Exception e)
        {
         e.printStackTrace();
        }



    }
    @Override
    public int getItemCount() {
        return orderDetailList.size();
    }
}
