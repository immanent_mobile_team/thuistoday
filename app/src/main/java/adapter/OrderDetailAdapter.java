package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.app.thuistoday.R;
import java.util.HashMap;
import java.util.List;

/**
 * Created by HOME on 2/10/2017.
 */
public class OrderDetailAdapter extends RecyclerView.Adapter<adapter.OrderDetailAdapter.MyViewHolder>  {
    private List<HashMap<String,String>> orderDetailList;
    private Context context;
    View itemView;
    Intent intent;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout ll_layout;
        TextView tv_price,tv_product_name,tv_quantity;
        public MyViewHolder(View view) {
            super(view);
            ll_layout=(LinearLayout)view.findViewById(R.id.ll_layout);
            tv_product_name=(TextView) view.findViewById(R.id.tv_product_name);
            tv_price=(TextView)view.findViewById(R.id.tv_price);
            tv_quantity=(TextView)view.findViewById(R.id.tv_quantity);
            context=view.getContext();
        }
    }
    public OrderDetailAdapter(List<HashMap<String,String>> orderDetailList) {
        this.orderDetailList= orderDetailList;
    }
    @Override
    public adapter.OrderDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_checkout, parent, false);
        return new adapter.OrderDetailAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final adapter.OrderDetailAdapter.MyViewHolder holder, final int position) {
        if (position%2==0){
            holder.ll_layout.setBackgroundResource(R.drawable.gray_bg);
        }else {
        }
    }
    @Override
    public int getItemCount() {
            return orderDetailList.size();
    }

}
