package adapter;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.thuistoday.CheckoutActivity;
import com.app.thuistoday.MyRealm;
import com.app.thuistoday.OrderDetailB;
import com.app.thuistoday.R;
import com.app.thuistoday.RealmController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Util.Constants;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Admin on 3/2/2017.
 */

public class OrderHistoryAdapter  extends RecyclerView.Adapter<adapter.OrderHistoryAdapter.MyViewHolder>  {
    private List<HashMap<String,String>> orderList;
    private Context context;
    Application application;
    View itemView;
    Intent intent;
    TextView tvCartItems;
    String strItemName="";
    int mCartItems;
    ImageView ivClearAll;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout ll_layout;
        TextView tv_price,tvDate,tvNumber,tvStore,tvQuantity;
        ImageView ivStatus;
        public MyViewHolder(View view) {
            super(view);
            tvStore = (TextView) view.findViewById(R.id.tvStore);
            tvNumber = (TextView) view.findViewById(R.id.tvNumber);
            tv_price = (TextView)view.findViewById(R.id.tvPrice);
            tvDate = ( TextView)view.findViewById(R.id.tvDate);
            tvQuantity = ( TextView)view.findViewById(R.id.tv_quantity);
            ivStatus = (ImageView) view.findViewById(R.id.ivStatus);
            ll_layout= (LinearLayout) view.findViewById(R.id.llParent);


//            context=view.getContext();

//                edt_quantity.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//                    public void afterTextChanged(Editable s) {
//                        String str_amount = null;
//                        if (edt_quantity.getText().toString().trim().length()>0) {
//                            int quantityNumber = Integer.parseInt(edt_quantity.getText().toString());
//                            str_amount = String.valueOf(quantityNumber*54);
//                        }
//                        else {
//                            str_amount="00";
//                        }
//                        tv_price.setText(str_amount);
//                    }
//                });
        }
    }
    public OrderHistoryAdapter(Context context, ArrayList<HashMap<String, String>> checkOutList) {
        this.orderList= checkOutList;
        this.context = context;
    }
    @Override
    public adapter.OrderHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_order_history, parent, false);
        return new adapter.OrderHistoryAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final adapter.OrderHistoryAdapter.MyViewHolder holder, final int position) {

        if (position%2==0){
            holder.ll_layout.setBackgroundResource(R.drawable.gray_bg);
        }else {

        }
          HashMap<String ,String >  hashMap = orderList.get(position);
         holder.tv_price.setText("€" + hashMap.get(Constants.KEY_TOTAL_PAYMENT));
         holder.tvDate.setText(hashMap.get(Constants.KEY_DATE));
         holder.tvNumber.setText(hashMap.get(Constants.KEY_ID));
         holder.tvStore.setText(hashMap.get(Constants.KEY_RESTAURANT_NAME_AR));
         holder.ivStatus.setImageResource(R.drawable.pending_btn);

        //  holder.tv_name.setText(name[position]);
//         holder.iv_shopListIcon.setImageResource(imageId[position]);
            holder.ll_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   HashMap<String,String>  hashMap =  orderList.get(position);
                    Intent intent = new Intent(context, OrderDetailB.class);
                    Bundle bundle = new Bundle();
                    Log.e("orderid",hashMap.toString());
                    bundle.putSerializable(Constants.KEY_HASH_MAP,hashMap);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
//    public void removeAt(int position, final String strItemName) {
//
//        checkOutList.remove(position);
//        notifyItemRemoved(position);
//        notifyItemRangeChanged(position, checkOutList.size());
//        RealmController rel = new RealmController(application);
//        Realm realm = rel.getRealm();
//        RealmResults<MyRealm> myRealms = rel.getAllData();
//        if (myRealms.size()!=0)
//        {
//            realm.executeTransaction(new Realm.Transaction() {
//                @Override
//                public void execute(Realm realm) {
//
//                    MyRealm myRealm = realm.where(MyRealm.class).equalTo("strItemName", strItemName).findFirst();
//                    myRealm.removeFromRealm();
//                    realm.commitTransaction();
//                }
//            });
//        }
//
//        RealmController rela = new RealmController(application);
//        RealmResults<MyRealm> myRealmsa = rela.getAllData();
//
//        double totalPrice=0;
//        if (myRealmsa.size()!=0)
//        {
//            mCartItems = 0;
//            for (MyRealm a : myRealmsa)
//            {
//                mCartItems = mCartItems+1;
//                String str = a.getStrPrice().replace(",",".");
//
//                double dbl= Double.parseDouble(str);
//
//                totalPrice = totalPrice + dbl;
//            }
//            tvCartItems.setText(String.valueOf(mCartItems));
//        }
//        else
//            tvCartItems.setText("0");
//        CheckoutActivity.tvTotalPrice.setText("Total price: €"+totalPrice);
////            tvCartItems.setText(String.valueOf(myRealms.size()));
//
////        Toast.makeText(context, strItemName+" "+position, Toast.LENGTH_SHORT).show();
//
////        .executeTransaction(new Realm.Transaction() {
////            @Override
////            public void execute(Realm realm) {
////                RealmResults<MyRealm> result = realm.where(MyRealm.class).equalTo(MyRealm.,userId).findAll();
////                result.deleteAllFromRealm();
////            }
////        });
//    }
}
