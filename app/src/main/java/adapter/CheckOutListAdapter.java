package adapter;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.thuistoday.CheckoutActivity;
import com.app.thuistoday.MyRealm;
import com.app.thuistoday.R;
import com.app.thuistoday.RealmController;
import com.app.thuistoday.ShopListActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Util.Constants;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by HOME on 1/17/2017.
 */
 public class CheckOutListAdapter extends RecyclerView.Adapter<adapter.CheckOutListAdapter.MyViewHolder>  {
        private List<HashMap<String,String>> checkOutList;
        private Context context;
        Application application;
        View itemView;
        Intent intent;
        TextView tvCartItems;
        String strItemName="";
        int mCartItems;
        Button btn_clearAll;
        public class MyViewHolder extends RecyclerView.ViewHolder{
            LinearLayout ll_layout;
            EditText edt_quantity;
            TextView tv_price,tvItemName,tvItemDescription,tvWeightUnit;
            ImageView ivDeleteItem;
            public MyViewHolder(View view) {
                super(view);
                ll_layout=(LinearLayout)view.findViewById(R.id.ll_layout);
                edt_quantity =(EditText)view.findViewById(R.id.edt_quantity);
                tv_price =(TextView)view.findViewById(R.id.tv_price);
                tvItemName =(TextView)view.findViewById(R.id.tvHeading);
                tvWeightUnit =(TextView)view.findViewById(R.id.tv_pcs);
//                tv_price=(TextView)view.findViewById(R.id.tv_);
                ivDeleteItem = (ImageView) view.findViewById(R.id.ivDeleteItem);
                context=view.getContext();
                edt_quantity.setEnabled(false);
//                edt_quantity.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//                    public void afterTextChanged(Editable s) {
//                        String str_amount = null;
//                        if (edt_quantity.getText().toString().trim().length()>0) {
//                            int quantityNumber = Integer.parseInt(edt_quantity.getText().toString());
//                            str_amount = String.valueOf(quantityNumber*54);
//                        }
//                        else {
//                            str_amount="00";
//                        }
//                        tv_price.setText(str_amount);
//                    }
//                });
            }
        }
        public CheckOutListAdapter(ArrayList<HashMap<String, String>> checkOutList, TextView tvCartItems, CheckoutActivity checkoutActivity, Button btn_clearAll) {
               this.checkOutList= checkOutList;
               this.tvCartItems = tvCartItems;
               this.application= checkoutActivity.getApplication();
               this.btn_clearAll = btn_clearAll;
        }
        @Override
        public adapter.CheckOutListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_checkout, parent, false);
            return new adapter.CheckOutListAdapter.MyViewHolder(itemView);
        }
        @Override
        public void onBindViewHolder(final adapter.CheckOutListAdapter.MyViewHolder holder, final int position) {

            if (position%2==0){
                holder.ll_layout.setBackgroundResource(R.drawable.gray_bg);
            }else {

            }
            HashMap<String ,String >  hashMap = checkOutList.get(position);
            String price = (hashMap.get(Constants.KEY_ITEM_PRICE).contains("."))
                    ? hashMap.get(Constants.KEY_ITEM_PRICE).replaceAll("\\.","\\,")
                    : hashMap.get(Constants.KEY_ITEM_PRICE);
            Log.e("Inside Adapter","<<>>"+price);
            holder.tv_price.setText(price);

            if(hashMap.get(Constants.KEY_ITEM_WEIGHT_UNIT).equals("pcs")) {
                holder.edt_quantity.setText(hashMap.get(Constants.KEY_ITEM_QUANTITY).split("\\.")[0]);
            }
            else {
                holder.edt_quantity.setText(hashMap.get(Constants.KEY_ITEM_QUANTITY).replaceAll("\\.","\\,"));
            }
            holder.tvWeightUnit.setText(hashMap.get(Constants.KEY_ITEM_WEIGHT_UNIT));
            holder.tvItemName.setText(hashMap.get(Constants.KEY_ITEM_NAME));
            holder.ivDeleteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                          HashMap<String ,String >  hashMap = checkOutList.get(position);
                    strItemName = hashMap.get(Constants.KEY_ITEM_NAME);
                      removeAt(position,strItemName);
                }
            });

            //  holder.tv_name.setText(name[position]);
           // holder.iv_shopListIcon.setImageResource(imageId[position]);
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });

        }

        @Override
        public int getItemCount() {
            return checkOutList.size();
        }
    public void removeAt(int position, final String strItemName) {

        checkOutList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, checkOutList.size());
        RealmController rel = new RealmController(application);
        Realm realm = rel.getRealm();
        RealmResults<MyRealm> myRealms = rel.getAllData();
        if (myRealms.size()!=0)
        {
                     realm.executeTransaction(new Realm.Transaction() {
                     @Override
                     public void execute(Realm realm) {

                         MyRealm myRealm = realm.where(MyRealm.class).equalTo("strItemName", strItemName).findFirst();
                         myRealm.removeFromRealm();
                         realm.commitTransaction();
                     }
                 });
        }

        RealmController rela = new RealmController(application);
        RealmResults<MyRealm> myRealmsa = rela.getAllData();

        double totalPrice=0;
        if (myRealmsa.size()!=0)
        {
            mCartItems = 0;
            for (MyRealm a : myRealmsa)
            {
                mCartItems = mCartItems+1;
                String str = a.getStrPrice().replace(",",".");

                double dbl= Double.parseDouble(str);

                totalPrice = totalPrice + dbl;
            }
            tvCartItems.setText(String.valueOf(mCartItems));
        }
        else
            tvCartItems.setText("0");
         CheckoutActivity.tvTotalPrice.setText("Total price: €"+ String.valueOf(roundOff(Float.parseFloat(totalPrice+""),2)).replace(".",","));
//            tvCartItems.setText(String.valueOf(myRealms.size()));



//        .executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//                RealmResults<MyRealm> result = realm.where(MyRealm.class).equalTo(MyRealm.,userId).findAll();
//                result.deleteAllFromRealm();
//            }
//        });
    }
    static float roundOff(float x, int position)
    {
        float a = x;
        double temp = Math.pow(10.00, position);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }
}


