package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.thuistoday.R;
import com.app.thuistoday.ZagroosStoreActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Util.Constants;

/**
 * Created by HOME on 1/16/2017.
 */
    public class ProductCategoriesAdapter extends RecyclerView.Adapter<adapter.ProductCategoriesAdapter.MyViewHolder>  {
        private List<HashMap<String,String>> productCategories;
        private Context context;
        int itemCount=0;
        public static ArrayList<String> selectedProductArrayList ;
        HashMap<String,String> hashMap =new HashMap<String, String>();
        String[] name = new String[] {"Vegetable","Meat and meat products","Universal shops", "Cheese and diary"};

        private boolean IsCheckBoxSelected=true;
        View itemView;
        public class MyViewHolder extends RecyclerView.ViewHolder{
            public TextView tv_name;
            ImageView iv_checkBox;
            public String str_checkBox;
            public MyViewHolder(View view) {
                super(view);
                tv_name=(TextView)view.findViewById(R.id.tv_name);
                iv_checkBox=(ImageView) view.findViewById(R.id.iv_checkBox);
                context=view.getContext();
                selectedProductArrayList = new ArrayList<>();
            }
        }
        public ProductCategoriesAdapter(ZagroosStoreActivity zagroosStoreActivity, List<HashMap<String, String>> productCategories) {
            this.productCategories= productCategories;
        }
        @Override
        public adapter.ProductCategoriesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_product_categories, parent, false);
            return new adapter.ProductCategoriesAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final adapter.ProductCategoriesAdapter.MyViewHolder holder, final int position) {
            hashMap=productCategories.get(position);
            holder.tv_name.setText(hashMap.get(Constants.CATEGORY_NAME_AR));
            //holder.iv_checkBox.setText(date[position]);
            // holder.iv_checkBox.setImageResource(imageId[position]);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (selectedProductArrayList.contains(String.valueOf(""+position)))
//                    {
//                        holder.iv_checkBox.setImageResource(R.drawable.green_checkbox);
//                        selectedProductArrayList.remove(position+"");
//                    }
//                    else
//                    {
//                        holder.iv_checkBox.setImageResource(R.drawable.green_checkbox_tick);
//                        selectedProductArrayList.add(String.valueOf(position));
//                    }
                    if (selectedProductArrayList.contains(productCategories.get(position).get(Constants.CATEGORY_ID)))
                    {
                        holder.iv_checkBox.setImageResource(R.drawable.check_box);
                        selectedProductArrayList.remove(productCategories.get(position).get(Constants.CATEGORY_ID));
                    }
                    else
                    {
                        holder.iv_checkBox.setImageResource(R.drawable.check_box_selected);
                        selectedProductArrayList.add(productCategories.get(position).get(Constants.CATEGORY_ID));
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return productCategories.size();
        }

    }
