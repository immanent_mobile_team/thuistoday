//package drawer;
//
///**
// * Created by Ravi on 29/07/15.
// */
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.text.TextUtils;
//import android.view.GestureDetector;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.app.thuistoday.R;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import adapter.NavigationDrawerAdapter;
//
//
//public class FragmentDrawer extends Fragment {
//
//    private static String TAG = FragmentDrawer.class.getSimpleName();
//
//    private RecyclerView recyclerView;
//    private ActionBarDrawerToggle mDrawerToggle;
//    private DrawerLayout mDrawerLayout;
//    private NavigationDrawerAdapter adapter;
//    private View containerView;
//    private static String[] titles = null;
//    private static String[] titles_signOut = null;
//    private FragmentDrawerListener drawerListener;
//    public static List<NavDrawerItem> data;
//    ImageView iv_profile;
//    TextView tv_email;
//    RelativeLayout rl_logout;
//    String str_email;
//    AlertDialog alertDialog;
//    public FragmentDrawer() {
//
//    }
//
//    public void setDrawerListener(FragmentDrawerListener listener) {
//        this.drawerListener = listener;
//    }
//
//    public static List<NavDrawerItem> getData() {
//      data= new ArrayList<>();
//
//
//        // preparing navigation drawer items
////        if (SharedPreference.First_Name.trim().length()>0)
////        {
//            for (int i = 0; i < titles.length; i++) {
//                NavDrawerItem navItem = new NavDrawerItem();
//                navItem.setTitle(titles[i]);
//                data.add(navItem);
//            }
////        }
////        else
////        {
////            for (int i = 0; i < titles_signOut.length; i++) {
////                NavDrawerItem navItem = new NavDrawerItem();
////                navItem.setTitle(titles_signOut[i]);
////                data.add(navItem);
////            }
////        }
//        return data;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        // drawer labels
//        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
////        titles_signOut = getActivity().getResources().getStringArray(R.array.nav_drawer_labels_sign_out);
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflating view layout
//        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
////        iv_profile= (ImageView) layout.findViewById(R.id.iv_profile);
////        tv_name= (TextView) layout.findViewById(R.id.tv_name);
////        if (!SharedPreference.First_Name.equals(""))
////        {
////            tv_name.setText(SharedPreference.First_Name+" "+SharedPreference.Last_Name);
////        }
////        if (MySharedPreferences.sharedPreferences != null) {
////            if (!MySharedPreferences.sharedPreferences.getString(Constants.KEY_USER_ID, Constants.NO_VALUE).equals(Constants.NO_VALUE)) {
////                str_email = MySharedPreferences.sharedPreferences.getString(Constants.key_email, Constants.NO_VALUE);
////            }
////        }
////        tv_email=(TextView)layout.findViewById(R.id.tv_email);
////        tv_email.setText(str_email);
////        tv_email.setSelected(true);
////        rl_logout=(RelativeLayout)layout.findViewById(R.id.rl_logout);
////        rl_logout.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
////                alertDialogBuilder.setTitle("Logout");
////                alertDialogBuilder.setCancelable(false);
////                alertDialogBuilder.setMessage("Are you sure you want to logout?");
////                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface arg0, int arg1) {
////                        if (MySharedPreferences.sharedPreferences != null) {
////                            if (!MySharedPreferences.sharedPreferences.getString(Constants.KEY_USER_ID, Constants.NO_VALUE).equals(Constants.NO_VALUE)) {
////                                SharedPreferences.Editor editor = MySharedPreferences.sharedPreferences.edit();
////                                editor.remove(Constants.KEY_USER_ID);
////                                editor.remove(Constants.key_userId);
////                                editor.remove(Constants.key_profilePic);
////                                editor.remove(Constants.key_FbProfile);
////                                editor.clear();
////                                editor.apply();
////                                AccessToken accessToken = AccessToken.getCurrentAccessToken();
////                                if(accessToken != null){
////                                    LoginManager.getInstance().logOut();
////                                }
////                                Intent intent=new Intent(getActivity(),MainActivity.class);
////                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                                startActivity(intent);
////                                getActivity().finish();
////                            }
////                        }
////                        /***
////                         * TO CLEAR SHAREDPREFERENCE
////                         *
////                         */
//////                        SharedPreferences settings = getActivity().getSharedPreferences(MainActivity.MYPREFENCES, Context.MODE_PRIVATE);
//////                        SharedPreferences.Editor editor = settings.edit();
//////                        if (!MainActivity.sharedPreferences.getString(Constants.KEY_CHECKED_UNCHECKED, Constants.KEY_NO_VALUE).equals(Constants.KEY_NO_VALUE)) {
//////                            editor.clear();
//////                            editor.remove(Constants.KEY_CHECKED_UNCHECKED);
//////                            editor.apply();
//////                            MainActivity.isFromLogin=true;
////                           /* Intent intent=new Intent(getActivity(),MainActivity.class);
////                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                            startActivity(intent);
////                            getActivity().finish();
////                            */
////
////                        }
//////                        else{
//////                            intent=new Intent(getActivity(),MainActivity.class);
//////                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//////                            startActivity(intent);
//////
//////                            getActivity().finish();
//////                        }
////                    //}
////                });
////                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        alertDialog.dismiss();
////                    }
////                });
////                alertDialog = alertDialogBuilder.create();
////                alertDialog.show();
////            }
////        });
////        tv_email.setSelected(true);
////        tv_email.setEllipsize(TextUtils.TruncateAt.MARQUEE);
////        tv_email.setSingleLine(true);
//        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
//        adapter = new NavigationDrawerAdapter(getActivity(), getData());
//        recyclerView.setAdapter(adapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                drawerListener.onDrawerItemSelected(view, position);
//                mDrawerLayout.closeDrawer(containerView);
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));
//
////        iv_profile.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if (!SharedPreference.First_Name.equals(""))
////                {
////                    if (ProfileUpdate.isOnProfileActivity==false)
////                    {
////                        mDrawerLayout.closeDrawer(containerView);
////                        Intent intent=new Intent(getActivity(), ProfileUpdate.class);
////                        startActivity(intent);
////                    }
////                  }
////                else
////                {
////                    Toast.makeText(getActivity(), "Login first", Toast.LENGTH_SHORT).show();
////                }
////               }
////        });
//        return layout;
//
//    }
//
//
//    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
//        containerView = getActivity().findViewById(fragmentId);
//        mDrawerLayout = drawerLayout;
//        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
//            @Override
//            public void onDrawerOpened(View drawerView) {
//                super.onDrawerOpened(drawerView);
//                getActivity().invalidateOptionsMenu();
//            }
//
//            @Override
//            public void onDrawerClosed(View drawerView) {
//                super.onDrawerClosed(drawerView);
//                getActivity().invalidateOptionsMenu();
//            }
//
//            @Override
//            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                toolbar.setAlpha(1 - slideOffset / 2);
//            }
//        };
//
//        mDrawerLayout.setDrawerListener(mDrawerToggle);
//        mDrawerLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                mDrawerToggle.syncState();
//            }
//        });
//    }
//
//    public static interface ClickListener {
//        public void onClick(View view, int position);
//
//        public void onLongClick(View view, int position);
//    }
//
//    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {
//
//        private GestureDetector gestureDetector;
//        private ClickListener clickListener;
//
//        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
//            this.clickListener = clickListener;
//            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
//                @Override
//                public boolean onSingleTapUp(MotionEvent e) {
//                    return true;
//                }
//
//                @Override
//                public void onLongPress(MotionEvent e) {
//                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
//                    if (child != null && clickListener != null) {
//                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
//                    }
//                }
//            });
//        }
//
//        @Override
//        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
//
//            View child = rv.findChildViewUnder(e.getX(), e.getY());
//            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
//                clickListener.onClick(child, rv.getChildPosition(child));
//            }
//            return false;
//        }
//
//        @Override
//        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//        }
//
//        @Override
//        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//        }
//
//
//    }
//
//    public interface FragmentDrawerListener {
//        public void onDrawerItemSelected(View view, int position);
//    }
//}
